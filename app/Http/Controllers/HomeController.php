<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('home.index');
    }

    public function home_fullpage_slider()
    {
        return view('home.fullpage-slider');
    }

    public function home_menu_center()
    {
        return view('home.menu-center');
    }

    public function home_portfolio_style()
    {
        return view('home.portfolio-style');
    }

    public function home_presentation_page()
    {
        return view('home.presentation-page');
    }

    public function home_video_tour()
    {
        return view('home.video-tour');
    }
}
