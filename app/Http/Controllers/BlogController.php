<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    // Classic

    public function style_1_columns()
    {
        return view('blog.classic.style-1-columns');
    }

    public function style_2_columns()
    {
        return view('blog.classic.style-2-columns');
    }

    public function style_3_columns()
    {
        return view('blog.classic.style-3-columns');
    }

    public function style_4_columns()
    {
        return view('blog.classic.style-4-columns');
    }

    public function style_2_columns_sidebar()
    {
        return view('blog.classic.style-2-columns-sidebar');
    }

    public function style_3_columns_sidebar()
    {
        return view('blog.classic.style-3-columns-sidebar');
    }

    public function style_large()
    {
        return view('blog.classic.style-large');
    }

    public function style_small()
    {
        return view('blog.classic.style-small');
    }


    
    //Masonry

    public function m_style_2_columns()
    {
        return view('blog.masonry.style-2-columns');
    }

    public function m_style_3_columns()
    {
        return view('blog.masonry.style-3-columns');
    }

    public function m_style_4_columns()
    {
        return view('blog.masonry.style-4-columns');
    }

    public function m_style_2_columns_sidebar()
    {
        return view('blog.masonry.style-2-columns-sidebar');
    }

    public function m_style_3_columns_sidebar()
    {
        return view('blog.masonry.style-3-columns-sidebar');
    }

    // Post

    public function p_standard_sidebar()
    {
        return view('blog.post.standard-post-sidebar');
    }

    public function p_standard()
    {
        return view('blog.post.standard-post');
    }
}
