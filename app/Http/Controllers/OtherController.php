<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OtherController extends Controller
{
    public function navigation_side_menu()
    {
        return view('others.navigation-side-menu');
    }

    public function subpages_blog()
    {
        return view('others.subpages-blog');
    }

    public function subpages_price_table()
    {
        return view('others.subpages-price-table');
    }
}
