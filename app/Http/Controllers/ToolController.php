<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ToolController extends Controller
{
    public function documentation()
    {
        return view('tools.doc');
    }

    public function idx()
    {
        return view('tools.idx');
    }

    public function shortcode()
    {
        return view('tools.shortcodes');
    }

    public function support()
    {
        return view('tools.support');
    }

    public function typography()
    {
        return view('tools.typography');
    }
}
