<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GalleryController extends Controller
{
    // Classic Style sidebar
    public function style_1_column_sidebar()
    {
        return view('gallery.classic.style-1-column-sidebar');
    }

    public function style_2_columns_sidebar()
    {
        return view('gallery.classic.style-2-columns-sidebar');
    }
    
    public function style_3_columns_sidebar()
    {
        return view('gallery.classic.style-3-columns-sidebar');
    }

    
    // Classic style
    public function style_1_column()
    {
        return view('gallery.classic.style-1-column');
    }

    public function style_2_columns()
    {
        return view('gallery.classic.style-2-columns');
    }
    
    public function style_3_columns()
    {
        return view('gallery.classic.style-3-columns');
    }

    public function style_4_columns()
    {
        return view('gallery.classic.style-4-columns');
    }

    // Grid style Fullscreen
    public function style_2_columns_fullscreen()
    {
        return view('gallery.grid.style-2-columns-fullscreen');
    }
    
    public function style_3_columns_fullscreen()
    {
        return view('gallery.grid.style-3-columns-fullscreen');
    }

    public function style_4_columns_fullscreen()
    {
        return view('gallery.grid.style-4-columns-fullscreen');
    }

    // Grid style
    public function g_style_2_columns()
    {
        return view('gallery.grid.style-2-columns');
    }
    
    public function g_style_3_columns()
    {
        return view('gallery.grid.style-3-columns');
    }

    public function g_style_4_columns()
    {
        return view('gallery.grid.style-4-columns');
    }

    public function g_style_alternatives()
    {
        return view('gallery.grid.alternative');
    }

    // Masonry style
    public function m_style_2_columns()
    {
        return view('gallery.masonry.style-2-columns');
    }
    
    public function m_style_3_columns()
    {
        return view('gallery.masonry.style-3-columns');
    }

    public function m_style_4_columns()
    {
        return view('gallery.masonry.style-4-columns');
    }

    // Masonry style Sidebar
    public function m_style_2_columns_sidebar()
    {
        return view('gallery.masonry.style-2-columns-sidebar');
    }
    
    public function m_style_3_columns_sidebar()
    {
        return view('gallery.masonry.style-3-columns-sidebar');
    }

    // Portfolio
    public function portfolio_fullscreen()
    {
        return view('gallery.portfolio.post-fullscreen');
    }
    
    public function post_standard()
    {
        return view('gallery.portfolio.post-standard');
    }
}
