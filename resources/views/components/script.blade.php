<script type="text/javascript" src="{{ asset('assets/js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/vendor/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/vendor/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/vendor/jquery-migrate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/vendor/revslider/js/jquery.themepunch.tools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/vendor/revslider/js/jquery.themepunch.revolution.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom/_main.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom/_packed.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom/custom_menu.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/vendor/core.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom/shortcodes_init.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom/_utils.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom/_front.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/vendor/picker/picker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/vendor/picker/picker.date.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/vendor/picker/picker.time.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/vendor/picker/picker.start.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/vendor/draggable.min.js') }}"></script>
<script type='text/javascript' src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom/_googlemap_init.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/vendor/accordion.min.js') }}"></script>

<!-- Customizer -->
<!-- <script type="text/javascript" src="custom_menu/js/_customizer.js"></script>-->
<!-- <script type="text/javascript" src="custom_menu/js/custom_tools.js"></script>-->



