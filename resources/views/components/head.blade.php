<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/x-icon" href="{{ asset('assets/images/icon/favicon.ico') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--[if lt IE 9]>
    <script src="js/vendor/html5.js" type="text/javascript"></script>
    <![endif]-->
    <title>{{ $title ?? 'Asset Ph | single property' }}</title>
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" id="calendar-css" href="{{ asset('assets/css/calendar.css') }}" type="text/css" media="all">
    <link rel="stylesheet" id="rs-plugin-settings-css" href="{{ asset('assets/js/vendor/revslider/css/settings.css') }}" type="text/css" media="all">
    <link rel="stylesheet" id="fontello-css" href="{{ asset('assets/css/fontello.css') }}" type="text/css" media="all">
    <link rel="stylesheet" id="packed-style-css" href="{{ asset('assets/css/_packed.css') }}" type="text/css" media="all">
    <link rel="stylesheet" id="main-style-css" href="{{ asset('assets/css/main_style.css') }}" type="text/css" media="all">
    <link rel="stylesheet" id="theme-skin-css" href="{{ asset('assets/css/general.css') }}" type="text/css" media="all">
    <style id="theme-skin-inline-css" type="text/css"></style>
    <link rel="stylesheet" id="responsive-css" href="{{ asset('assets/css/responsive.css') }}" type="text/css" media="all">

</head>