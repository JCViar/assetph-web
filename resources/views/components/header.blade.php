<header class="noFixMenu menu_left with_user_menu">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="topWrapFixed"></div>
                <div class="topWrap styleShaded">
                    <div class="usermenu_area">
                        <div class="container">
                            <div class="menuUsItem menuItemRight">
                                <ul id="usermenu" class="usermenu_list sf-arrows">
                                    <li class="usermenu_info_time">
                                        <span class="contact_info_time">Open House on the 24th, - 12 mid day to 5 pm.</span>
                                    </li>
                                    <li class="usermenu_socials copy_socials socPage">
                                        <a class="social_icons icon-twitter" target="_blank" href="https://twitter.com/Theme_REX"></a>
                                        <a class="social_icons icon-facebook" target="_blank" href="https://www.facebook.com/themerex"></a>
                                        <a class="social_icons icon-gplus" target="_blank" href="https://plus.google.com/102189073109602153696"></a>
                                        <a class="social_icons icon-vimeo" target="_blank" href="#"></a>
                                        <a class="social_icons icon-pinterest" target="_blank" href="#"></a>
                                    </li>
                                </ul>
                            </div>
                            @if (Route::has('login'))
                                <div class="menuUsItem menuItemLeft">
                                    <ul class="usermenu_list sf-arrows">
                                        @auth
                                            <li class="usermenu_login">
                                                <div>{{ Auth::user()->name }}</div>
                                            </li>
                                            <li class="usermenu_login">
                                               
                                                <!-- Authentication -->
                                                <form method="POST" action="{{ route('logout') }}">
                                                    @csrf
                
                                                    <x-dropdown-link :href="route('logout')"
                                                            onclick="event.preventDefault();
                                                                        this.closest('form').submit();">
                                                        {{ __('Log out') }}
                                                    </x-dropdown-link>
                                                </form>
                                            </li>
                                        @else
                                            <li class="usermenu_login">
                                                <a href="{{ route('login') }}">Login | Register</a>
                                            </li>
                                        @endauth
                                        <li class="usermenu_call_back">
                                            <a class="user-popup-link" href="#form_popup_top">
                                                <span>request call back</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="mainmenu_area">
                        <div class="wrap_logo">
                            <div class="logo with_text">
                                <a href="{{ route('home') }}">
                                    <img src="{{ asset('assets/images/logo/46x60.png') }}" class="logo_main" alt="">
                                    <img src="{{ asset('assets/images/logo/46x60.png') }}" class="logo_fixed" alt="">
                                    <div class="logo_info">
                                        <span class="logo_text">
                                            <b>Asset Ph</b>
                                        </span>
                                        <span class="logo_slogan">single property</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="wrap_menu">
                            <div class="infoTopWrap">
                                <div class="location">
                                    <span class="info_icon icon-house"></span>
                                    <span class="contact_info_location">
                                        <b>123 QuickSale Street</b>
                                        <br>Chicago, IL 60606
                                    </span>
                                </div>
                                <div class="phone">
                                    <span class="info_icon icon-phone-handle"></span>
                                    <span class="contact_info_phone">
                                        <b>123-456-7890</b>
                                        <br>info@yoursite.com
                                    </span>
                                </div>
                            </div>
                            <a href="#" class="openResponsiveMenu"></a>
                            <nav role="navigation" class="menuTopWrap topMenuStyleLine">
                                <ul id="mainmenu" class="sf-arrows">
                                    <li class="menu-item {{ Request::is('home*') ? 'current-menu-ancestor' : '' }} menu-item-has-children">
                                        <a href="{{ route('home') }}" class="sf-with-ul">Home</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item {{ Request::is('*home-fullpage-slider*') ? 'current-menu-item' : '' }}">
                                                <a href="{{ route('homeFullpageSlider') }}">Fullpage slider</a>
                                            </li>
                                            <li class="menu-item {{ Request::is('*home-video-tour*') ? 'current-menu-item' : '' }}">
                                                <a href="{{ route('homeVideoTour') }}">Video tour</a>
                                            </li>
                                            <li class="menu-item {{ Request::is('*home-presentation-page*') ? 'current-menu-item' : '' }}">
                                                <a href="{{ route('homePresentation') }}">Presentation page</a>
                                            </li>
                                            <li class="menu-item {{ Request::is('*home-portfolio-style*') ? 'current-menu-item' : '' }}">
                                                <a href="{{ route('homePortfolio') }}">Portfolio Style</a>
                                            </li>
                                            <li class="menu-item {{ Request::is('*home-menu-center*') ? 'current-menu-item' : '' }}">
                                                <a href="{{ route('homeMenuCenter') }}">Menu Center</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="menu-item columns custom_view_item {{ Request::is(['about-us*', 'services*', 'agents-team*', 'contacts*', 'faqs*', 'tools*', 'others*'])  ? 'current-menu-ancestor' : '' }} menu-item-has-children">
                                        <a href="#" class="sf-with-ul">Pages</a>
                                        <ul class="menu-panel columns">
                                            <li>
                                                <ul class="custom-menu-style columns sub-menu">
                                                    <li class="menu-item menu-item-has-children">
                                                        <a href="#" class="sf-with-ul">Main</a>
                                                        <ul class="sub-menu">
                                                            <li class="menu-item {{ Request::is('*about-us*') ? 'current-menu-item' : '' }}">
                                                                <a href="{{ route('aboutUs') }}">About Us</a>
                                                            </li>
                                                            <li class="menu-item {{ Request::is('*services*') ? 'current-menu-item' : '' }}">
                                                                <a href="{{ route('services') }}">Services</a>
                                                            </li>
                                                            <li class="menu-item {{ Request::is('*agents-team*') ? 'current-menu-item' : '' }}">
                                                                <a href="{{ route('team') }}">Agents Team</a>
                                                            </li>
                                                            <li class="menu-item {{ Request::is('*contacts*') ? 'current-menu-item' : '' }}">
                                                                <a href="{{ route('contacts') }}">Contacts</a>
                                                            </li>
                                                            <li class="menu-item {{ Request::is('*faqs*') ? 'current-menu-item' : '' }}">
                                                                <a href="{{ route('faqs') }}">FAQ's</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="menu-item menu-item-has-children">
                                                        <a href="#" class="sf-with-ul">Tools</a>
                                                        <ul class="sub-menu">
                                                            <li class="menu-item {{ Request::is('*idx*') ? 'current-menu-item' : '' }}">
                                                                <a href="{{ route('idx') }}">IDX page</a>
                                                            </li>
                                                            <li class="menu-item {{ Request::is('*shortcodes*') ? 'current-menu-item' : '' }}">
                                                                <a href="{{ route('shortcodes') }}">Shortcodes</a>
                                                            </li>
                                                            <li class="menu-item {{ Request::is('*typography*') ? 'current-menu-item' : '' }}">
                                                                <a href="{{ route('typography') }}">Typography</a>
                                                            </li>
                                                            <li class="menu-item {{ Request::is('*documentation*') ? 'current-menu-item' : '' }}">
                                                                <a href="{{ route('documentation') }}">Documentation</a>
                                                            </li>
                                                            <li class="menu-item {{ Request::is('*support*') ? 'current-menu-item' : '' }}">
                                                                <a href="{{ route('support') }}">Support</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="menu-item menu-item-has-children">
                                                        <a href="#" class="sf-with-ul">Others</a>
                                                        <ul class="sub-menu">
                                                            <li class="menu-item {{ Request::is('*others/subpages-price-table*') ? 'current-menu-item' : '' }}">
                                                                <a href="{{ route('pricetable') }}">Price Table</a>
                                                            </li>
                                                            <li class="menu-item {{ Request::is('*others/subpages-blog*') ? 'current-menu-item' : '' }}">
                                                                <a href="{{ route('postblock') }}">Post Blocks</a>
                                                            </li>
                                                            <li class="menu-item  {{ Request::is('*others/navigation-side-menu*') ? 'current-menu-item' : '' }}">
                                                                <a href="{{ route('sidemenu') }}">Side Menu</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="menu-item menu-item-has-children {{ Request::is('blog*') ? 'current-menu-ancestor' : '' }}">
                                        <a href="#" class="sf-with-ul">Blog</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item menu-item-has-children {{ Request::is('*blog/classic*') ? 'current-menu-ancestor' : '' }} current-menu-parent">
                                                <a href="#" class="sf-with-ul">Classic Style</a>
                                                <ul class="sub-menu">
                                                    <li class="menu-item {{ Request::is('*blog/classic/style-large') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('blogClassicLarge') }}">Classic Style Large</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*blog/classic/style-small') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('blogClassicSmall') }}">Classic Style Small</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*blog/classic/style-1-columns') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('blogClassic1') }}">1 Column</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*blog/classic/style-2-columns') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('blogClassic2') }}">2 Columns</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*blog/classic/style-3-columns') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('blogClassic3') }}">3 Columns</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*blog/classic/style-4-columns*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('blogClassic4') }}">4 Columns</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*blog/classic/style-2-columns-sidebar') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('blogClassic2S') }}">2 Columns + sidebar</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*blog/classic/style-3-columns-sidebar') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('blogClassic3S') }}">3 Columns + sidebar</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="menu-item menu-item-has-children {{ Request::is('*blog/masonry*') ? 'current-menu-ancestor' : '' }}">
                                                <a href="#" class="sf-with-ul">Masonry Style</a>
                                                <ul class="sub-menu">
                                                    <li class="menu-item {{ Request::is('*style-2-columns') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('blogMasonry2') }}">2 Columns</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*style-3-columns') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('blogMasonry3') }}">3 Columns</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*style-4-columns') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('blogMasonry4') }}">4 Columns</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*style-2-columns-sidebar') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('blogMasonry2S') }}">2 Columns + sidebar</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*style-3-columns-sidebar') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('blogMasonry3S') }}">3 Columns + sidebar</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="menu-item menu-item-has-children {{ Request::is('*blog/post*') ? 'current-menu-ancestor' : '' }}">
                                                <a href="#" class="sf-with-ul">Blog Post</a>
                                                <ul class="sub-menu">
                                                    <li class="menu-item {{ Request::is('*standard-sidebar') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('postStandardS') }}">Standard Post with Sidebar</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*standard') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('postStandard') }}">Standard Post</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="menu-item menu-item-has-children  {{ Request::is('gallery*') ? 'current-menu-ancestor' : '' }}">
                                        <a href="#" class="sf-with-ul">Gallery</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item menu-item-has-children {{ Request::is('*gallery/grid/style*') ? 'current-menu-ancestor' : '' }} current-menu-parent">
                                                <a href="#" class="sf-with-ul">Grid Style</a>
                                                <ul class="sub-menu">
                                                    <li class="menu-item {{ Request::is('*gallery/grid/style-2-columns*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('grid2') }}">2 Columns</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*gallery/grid/style-3-columns*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('grid3') }}">3 Columns</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*gallery/grid/style-4-columns*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('grid4') }}">4 Columns</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*gallery/grid/style-2-columns-fullscreen*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('grid2fs') }}">2 Columns fullscreen</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*gallery/grid/style-3-columns-fullscreen*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('grid3fs') }}">3 Columns fullscreen</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*gallery/grid/style-4-columns-fullscreen*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('grid4fs') }}">4 Columns fullscreen</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="menu-item menu-item-has-children {{ Request::is('*gallery/masonry*') ? 'current-menu-ancestor' : '' }}">
                                                <a href="#" class="sf-with-ul">Masonry Style</a>
                                                <ul class="sub-menu">
                                                    <li class="menu-item {{ Request::is('*gallery/masonry/style-2-columns*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('masonry2') }}">2 Columns</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*gallery/masonry/style-3-columns*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('masonry3') }}">3 Columns</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*gallery/masonry/style-4-columns*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('masonry4') }}">4 Columns</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*gallery/masonry/style-2-column-sidebar*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('masonry2Sidebar') }}">2 Columns + sidebar</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*gallery/masonry/style-3-column-sidebar*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('masonry3Sidebar') }}">3 Columns + sidebar</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="menu-item menu-item-has-children {{ Request::is('*gallery/classic*') ? 'current-menu-ancestor' : '' }}">
                                                <a href="#" class="sf-with-ul">Classic Style</a>
                                                <ul class="sub-menu">
                                                    <li class="menu-item {{ Request::is('*gallery/classic/style-1-column*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('classic1') }}">1 Column</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*gallery/classic/style-2-column*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('classic2') }}">2 Columns</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*gallery/classic/style-3-column*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('classic3') }}">3 Columns</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*gallery/classic/style-4-column*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('classic4') }}">4 Columns</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*gallery/classic/style-1-column-sidebar*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('classic1Sidebar') }}">1 Column + sidebar</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*gallery/classic/style-2-column-sidebar*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('classic2Sidebar') }}">2 Columns + sidebar</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*gallery/classic/style-3-column-sidebar*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('classic3Sidebar') }}">3 Columns + sidebar</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="menu-item {{ Request::is('*grid/alternative') ? 'current-menu-ancestor' : '' }}">
                                                <a href="{{ route('gridAlternative') }}">Grid Alternative</a>
                                            </li>
                                            <li class="menu-item menu-item-has-children {{ Request::is('*portfolio*') ? 'current-menu-ancestor' : '' }}">
                                                <a href="#" class="sf-with-ul">Portfolio Post</a>
                                                <ul class="sub-menu">
                                                    <li class="menu-item {{ Request::is('*gallery/portfolio/post-standard*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('portfolioStandard') }}">Standard</a>
                                                    </li>
                                                    <li class="menu-item {{ Request::is('*gallery/portfolio/post-fullscreen*') ? 'current-menu-item' : '' }}">
                                                        <a href="{{ route('portfolioFullscreen') }}">Fullscreen</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="menu-item {{ Request::is('appointments*') ? 'current-menu-ancestor' : '' }}">
                                        <a href="{{ route('appointments') }}" class="sf-with-ul">Appointments</a>
                                    </li>
                                    <li class="menu-item {{ Request::is('tour*') ? 'current-menu-ancestor' : '' }} menu-item-has-children thumb custom_view_item">
                                        <a href="{{ route('projects') }}" class="sf-with-ul">Tour</a>
                                        <ul class="menu-panel thumb">
                                            <li>
                                                <ul class="custom-menu-style thumb sub-menu">
                                                    <li>
                                                        <a href="{{ route('tour') }}" data-author="Admin" data-pubdate="December 22, 2014" data-thumb="{{ asset('assets/images/main_menu/1170x780.png') }}" data-title="Who Should Buy ‘Rent To Own’" data-link="{{ route('tour') }}">
                                                            <img alt="Who Should Buy ‘Rent To Own’" src="{{ asset('assets/images/main_menu/75x75.png') }}">
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('tour') }}" data-author="Admin" data-pubdate="December 22, 2014" data-thumb="{{ asset('assets/images/main_menu/1170x779.png') }}" data-title="Commercial Real Estate Loans" data-link="{{ route('tour') }}">
                                                            <img alt="Commercial Real Estate Loans" src="{{ asset('assets/images/main_menu/75x75.png') }}">
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('tour') }}" data-author="Admin" data-pubdate="December 22, 2014" data-thumb="{{ asset('assets/images/main_menu/1170x777.png') }}" data-title="Real Estate Traps To Avoid" data-link="{{ route('tour') }}">
                                                            <img alt="Real Estate Traps To Avoid" src="{{ asset('assets/images/main_menu/75x75.png') }}">
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('tour') }}" data-author="Admin" data-pubdate="December 22, 2014" data-thumb="{{ asset('assets/images/main_menu/1170x780.png') }}" data-title="Selling Your House?" data-link="{{ route('tour') }}">
                                                            <img alt="Selling Your House?" src="{{ asset('assets/images/main_menu/75x75.png') }}">
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('tour') }}" data-author="Admin" data-pubdate="December 22, 2014" data-thumb="{{ asset('assets/images/main_menu/1170x778.png') }}" data-title="To Buy or Not To Buy" data-link="{{ route('tour') }}">
                                                            <img alt="To Buy or Not To Buy" src="{{ asset('assets/images/main_menu/75x75.png') }}">
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('tour') }}" data-author="Admin" data-pubdate="December 22, 2014" data-thumb="{{ asset('assets/images/main_menu/1170x778.png') }}" data-title="Freehold Vs. Condo" data-link="{{ route('tour') }}">
                                                            <img alt="Freehold Vs. Condo" src="{{ asset('assets/images/main_menu/75x75.png') }}">
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('tour') }}" data-author="Admin" data-pubdate="December 22, 2014" data-thumb="{{ asset('assets/images/main_menu/1170x780.png') }}" data-title="The Art of House Hunting" data-link="{{ route('tour') }}">
                                                            <img alt="The Art of House Hunting" src="{{ asset('assets/images/main_menu/75x75.png') }}">
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('tour') }}" data-author="Admin" data-pubdate="December 22, 2014" data-thumb="{{ asset('assets/images/main_menu/1170x761.png') }}" data-title="The Perfect Apartment" data-link="{{ route('tour') }}">
                                                            <img alt="The Perfect Apartment" src="{{ asset('assets/images/main_menu/75x75.png') }}">
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div class="item_placeholder">
                                                    <div class="thumb_wrap">
                                                        <img alt="Who Should Buy ‘Rent To Own’" src="{{ asset('assets/images/main_menu/1170x780.png') }}">
                                                    </div>
                                                    <h4 class="item_title">
                                                        <a href="{{ route('tour') }}">Who Should Buy ‘Rent To Own’</a>
                                                    </h4>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>