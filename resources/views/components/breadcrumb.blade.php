<section id="topOfPage" class="topTabsWrap no_padding_container">
    <div class="container">
        <div class="col-sm-12">
            <div class="speedBar">
                @foreach(Request::segments() as $segment)
                    <span class="all">{{ ucwords($segment) }}</span>
                    <span class="breadcrumbs_delimiter"> / </span>
                @endforeach
            </div>
            <h1 class="pageTitle">{{ ucwords(str_replace("-", " ", $segment)) }}</h1>
        </div>
    </div>
</section>