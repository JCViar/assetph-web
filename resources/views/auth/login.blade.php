<x-guest-layout>
    <x-app-layout>
                <!-- Session Status -->
            <x-auth-session-status class="mb-4" :status="session('status')" />

            <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <div id="user-popUp" class="user-popUp mfp-with-anim">
                <div class="sc_tabs">
                    <ul class="loginHeadTab" role="tablist">
                        <li role="tab" tabindex="-1" aria-controls="loginForm" aria-labelledby="user_popup_1" aria-selected="true" aria-expanded="true">
                            <a href="#loginForm" class="loginFormTab icon" role="presentation" id="user_popup_1">Log In</a>
                        </li>
                        <li role="tab" tabindex="0" aria-controls="registerForm" aria-labelledby="user_popup_2" aria-selected="false" aria-expanded="false">
                            <a href="#registerForm" class="registerFormTab icon" role="presentation" id="user_popup_2">Create an Account</a>
                        </li>
                    </ul>
                    <div id="loginForm" class="formItems loginFormBody" aria-labelledby="user_popup_1" role="tabpanel" aria-hidden="false">
                        <div class="itemformLeft">
                            <form action="{{ route('login') }}" method="POST" name="login_form" class="formValid">
                                
                                @csrf

                                <ul class="formList">
                                    <li class="icon formLogin">
                                        <div>
                                            <input id="email" class="block mt-1 w-full" type="email" name="email" placeholder="Email" required autofocus />
                                        </div>
                                        {{-- <input type="text" id="login" name="log" value="" placeholder="Login"> --}}
                                    </li>
                                    <li class="icon formPass">
                                        <div class="mt-4">
                                            <input type="password" id="password" name="password" value="" placeholder="Password">
                                        </div>
                                        
                                    </li>
                                    <li class="remember">
                                        <!-- Remember Me -->
                                        <label for="remember_me" class="inline-flex items-center">
                                            <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                                            <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                                            <div class="inline-flex ml-4">
                                                @if (Route::has('password.request'))
                                                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                                                        {{ __('Forgot your password?') }}
                                                    </a>
                                                @endif
                                            </div>
                                        </label>
                                    </li>
                                    <li class="sc_button sc_button_style_global sc_button_size_big squareButton global big">
                                        <x-button class="ml-3">
                                            {{ __('Log in') }}
                                        </x-button>
                                        
                                    </li>
                                </ul>
                            </form>
                        </div>
                        <div class="itemformRight">
                            <ul class="formList">
                                <li>You can login using your social profile</li>
                                <li class="loginSoc">
                                    <a href="#" class="iconLogin fb"></a>
                                    <a href="#" class="iconLogin tw"></a>
                                    <a href="#" class="iconLogin gg"></a>
                                </li>
                                <li>
                                    <a href="#" class="loginProblem">Problem with login?</a>
                                </li>
                            </ul>
                        </div>
                        <div class="result messageBlock"></div>
                    </div>
                    <div id="registerForm" class="formItems registerFormBody" aria-labelledby="user_popup_2" role="tabpanel" aria-hidden="true">
                        
                        <form name="register_form" method="POST" action="{{ route('register') }}" class="formValid" >
                            
                            @csrf
                            
                            <input type="hidden" name="redirect_to" value="#">
                            <div class="itemformLeft">
                                <ul class="formList">
                                    <li class="icon formUser">
                                        <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" placeholder="Name" required autofocus />
                                    </li>
                                    <li class="icon formLogin">
                                        <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" placeholder="E-mail" required />
                                    </li>
                                    <li class="i-agree">
                                        <input type="checkbox" value="forever" id="i-agree" name="i-agree">
                                        <label for="i-agree">I agree with</label> <a href="#">Terms &amp; Conditions</a>
                                    </li>
                                    <li class="sc_button sc_button_style_global sc_button_size_big squareButton global big">
                                        <x-button class="ml-4 ">
                                            {{ __('Register') }}
                                        </x-button>
                                    </li>
                                </ul>
                            </div>
                            <div class="itemformRight">
                                <ul class="formList">
                                    <li class="icon formPass">
                                        <x-input id="password" class="block mt-1 w-full"
                                            type="password"
                                            name="password"
                                            placeholder="Password"
                                            required autocomplete="new-password" />
                                        
                                    </li>
                                    <li class="icon formPass">
                                        <x-input id="password_confirmation" class="block mt-1 w-full"
                                            type="password"
                                            placeholder="Confirm Password"
                                            name="password_confirmation" required />
                                    </li>
                                    <li class="i-agree">
                                        <a href="{{ route('login') }}">
                                            {{ __('Already registered?') }}
                                        </a>
                                    </li>
                                 
                                </ul>
                            </div>
                        </form>
                        <div class="result messageBlock"></div>
                    </div>
                </div>
            </div>
    </x-app-layout>

















</x-guest-layout>
