<x-app-layout>
    <x-slot name="title">
        FAQ's
    </x-slot>
    <section class="light_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">			
                    <div class="sc_tabs sc_tabs_style_1 no_margin_bottom" data-active="0">
                        <ul class="sc_tabs_titles">
                            <li class="tab_names">
                                <a href="#sc_tabs_faq_1" class="theme_button" id="sc_tabs_faq_1_tab">General</a>
                            </li>
                            <li class="tab_names">
                                <a href="#sc_tabs_faq_2" class="theme_button" id="sc_tabs_faq_2_tab">Pricing</a>
                            </li>
                            <li class="tab_names">
                                <a href="#sc_tabs_faq_3" class="theme_button" id="sc_tabs_faq_3_tab">Account Management</a>
                            </li>
                            <li class="tab_names">
                                <a href="#sc_tabs_faq_4" class="theme_button" id="sc_tabs_faq_4_tab">Troubleshooting</a>
                            </li>
                        </ul>

                        <div id="sc_tabs_faq_1" class="sc_tabs_content">
                            <div class="col-sm-6">
                                <div class="sc_accordion sc_accordion_style_2 no_margin_bottom" data-active="1">
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">What about one timets?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">How do I teach my audience to participate? What are the best practices from other customers?</h5>
                                        <div class="sc_accordion_content">
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">Can I respond multiple times with a single text message?</h5>
                                        <div class="sc_accordion_content">
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">How can I cancel my subscription?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="sc_accordion sc_accordion_style_2 no_margin_bottom" data-active="2">
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">How can I cancel my subscription?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">Can I combine multiple questions (polls) into a survey?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">Can I use my own short code?</h5>
                                        <div class="sc_accordion_content">
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">Do I need to certify my phone number? If so, how do I certify my phone number?</h5>
                                        <div class="sc_accordion_content">
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">Can I combine multiple questions (polls) into a survey?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="sc_tabs_faq_2" class="sc_tabs_content">
                            <div class="col-sm-6">
                                <div class="sc_accordion sc_accordion_style_2 no_margin_bottom" data-active="2">
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">How can I cancel my subscription?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">Can I combine multiple questions (polls) into a survey?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">Can I use my own short code?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">Do I need to certify my phone number? If so, how do I certify my phone number?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p> 
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">Can I combine multiple questions (polls) into a survey?</h5>
                                        <div class="sc_accordion_content"> 
                                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="sc_accordion sc_accordion_style_2 no_margin_bottom" data-active="1">
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">What about one timets?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. </p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">How do I teach my audience to participate? What are the best practices from other customers?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. </p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">Can I respond multiple times with a single text message?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. </p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">How can I cancel my subscription?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="sc_tabs_faq_3" class="sc_tabs_content">
                            <div class="columnsWrap sc_columns sc_columns_count_2">
                                <div class="col-sm-6">
                                    <div class="sc_accordion sc_accordion_style_2 no_margin_bottom" data-active="2">
                                        <div class="sc_accordion_item">
                                            <h5 class="sc_accordion_title">How can I cancel my subscription?</h5>
                                            <div class="sc_accordion_content"> 
                                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            </div>
                                        </div>
                                        <div class="sc_accordion_item">
                                            <h5 class="sc_accordion_title">Can I combine multiple questions (polls) into a survey?</h5>
                                            <div class="sc_accordion_content"> 
                                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            </div>
                                        </div>
                                        <div class="sc_accordion_item">
                                            <h5 class="sc_accordion_title">Can I use my own short code?</h5>
                                            <div class="sc_accordion_content"> 
                                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            </div>
                                        </div>
                                        <div class="sc_accordion_item">
                                            <h5 class="sc_accordion_title">Do I need to certify my phone number? If so, how do I certify my phone number?</h5>
                                            <div class="sc_accordion_content"> 
                                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            </div>
                                        </div>
                                        <div class="sc_accordion_item">
                                            <h5 class="sc_accordion_title">Can I combine multiple questions (polls) into a survey?</h5>
                                            <div class="sc_accordion_content"> 
                                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="sc_accordion sc_accordion_style_2 no_margin_bottom" data-active="3">
                                        <div class="sc_accordion_item">
                                            <h5 class="sc_accordion_title">What about one timets?</h5>
                                            <div class="sc_accordion_content"> 
                                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. </p>
                                            </div>
                                        </div>
                                        <div class="sc_accordion_item">
                                            <h5 class="sc_accordion_title">How do I teach my audience to participate? What are the best practices from other customers?</h5>
                                            <div class="sc_accordion_content"> 
                                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p> 
                                            </div>
                                        </div>
                                        <div class="sc_accordion_item">
                                            <h5 class="sc_accordion_title">Can I respond multiple times with a single text message?</h5>
                                            <div class="sc_accordion_content"> 
                                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. </p>
                                            </div>
                                        </div>
                                        <div class="sc_accordion_item">
                                            <h5 class="sc_accordion_title">How can I cancel my subscription?</h5>
                                            <div class="sc_accordion_content"> 
                                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="sc_tabs_faq_4" class="sc_tabs_content">
                            <div class="col-sm-6">
                                <div class="sc_accordion sc_accordion_style_2 no_margin_bottom" data-active="0">
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">How can I cancel my subscription?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">Can I combine multiple questions (polls) into a survey?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">Can I use my own short code?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">Do I need to certify my phone number? If so, how do I certify my phone number?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">Can I combine multiple questions (polls) into a survey?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="sc_accordion sc_accordion_style_2 no_margin_bottom" data-active="2">
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">What about one timets?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. </p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">How do I teach my audience to participate? What are the best practices from other customers?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. </p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">Can I respond multiple times with a single text message?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. </p>
                                        </div>
                                    </div>
                                    <div class="sc_accordion_item">
                                        <h5 class="sc_accordion_title">How can I cancel my subscription?</h5>
                                        <div class="sc_accordion_content"> 
                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>					
                </div>
            </div>
        </div>
    </section> 
</x-app-layout>