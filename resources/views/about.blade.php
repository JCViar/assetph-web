<x-app-layout>
    <x-slot name="title">
        About Us
    </x-slot>
    <section class="light_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="sc_title_align_center sc_title sc_title_underline color_1">About Asset Ph</h2>
                    <div class="sc_content sc_aligncenter text_styling">
                        Purchasing products from ThemeRex means entrusting<br />
                        your reputation to one of the best web-studios. 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <a href="#" class="sc_icon icon-login sc_icon_round sc_icon_round_big no_bg_icon sc_aligncenter theme_accent theme_accent_border margin_top_small margin_bottom_mini"></a>
                    <h5 class="sc_title_align_center sc_title sc_title_regular  color_1">Fits For Any Purpose</h5>
                    ThemeRex company does its work with a customer in mind which is we ensured that Asset Ph theme will be any kind of website you wish. 
                </div>
                <div class="col-md-4 col-sm-4">
                    <a href="#" class="sc_icon icon-monitor sc_icon_round sc_icon_round_big no_bg_icon sc_aligncenter theme_accent theme_accent_border margin_top_small margin_bottom_mini"></a>
                    <h5 class="sc_title_align_center sc_title sc_title_regular  color_1">100% Responsive</h5>
                    Asset Ph will automatically be adjusted to any resolution without you having to worry about creating versions for each of the displays. 
                </div>
                <div class="col-md-4 col-sm-4">
                    <a href="#" class="sc_icon icon-paper-plane-empty sc_icon_round sc_icon_round_big no_bg_icon sc_aligncenter theme_accent theme_accent_border margin_top_small margin_bottom_mini">	</a>
                    <h5 class="sc_title_align_center sc_title sc_title_regular  color_1">Free Updates &#038; Support</h5>
                    We create our themes with a thought of our customers, therefore our team work hard to provide you best technical support in the world. 
                </div>
            </div>
        </div>
    </section>

    <section class="grey_section no_padding_container">
        <div class="container-fluid">
            <div class="row">
                <div class="columnsFloat autoHeight columnsWrap ">
                    <div class="col-sm-6">
                        <div class="sc_section text-center margin_right_large margin_left_large">
                            <div class="sc_content margin_top_big text_styling">Who We Are &amp; What We Do</div>
                            <h2 class="sc_title sc_title_underline color_1">Who We Are</h2> 
                            Purchasing web products from ThemeRex company means entrusting your reputation to one of the best web-studios in this area. Our team creates our themes with a thought of our customers, therefore our creative minds works hard to provide you best technical support in the whole world.
                            <br />
                            <span class="sc_button sc_button_style_global sc_button_size_big  margin_top_small margin_bottom_big squareButton global big">
                                <a href="#" class="">learn more</a>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-6 image_bg_5"></div>
                </div>
            </div>
        </div>
    </section>

    <section class="light_section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="twitBlockWrap">
                        <span class="twitterTitle">Latest Tweet Update</span>
                        <div class="sc_twitter twitBlock">
                            <div class="sc_slider sc_slider_swiper sc_slider_controls sc_slider_nopagination sc_slider_noresize sc_slider_autoheight swiper-slider-container">
                                <ul class="slides swiper-wrapper">
                                    <li class="sc_twitter_item swiper-slide">
                                        <p>
                                            <span class="twitterIco"></span>
                                            Check out #WordPress Good Energy - Ecology &amp; Renewable Energy Company on #EnvatoMarket #themeforest
                                            <a href="http://themeforest.net/item/good-energy-ecology-renewable-energy-company/12909437?utm_source=sharetw" target="_blank">themeforest.net/item/good-ener…</a>
                                        </p>
                                    </li>
                                    <li class="sc_twitter_item swiper-slide">
                                        <p>
                                            <span class="twitterIco"></span>
                                            Like Our Themes? Get Them at Creative Market!
                                            <a href="http://themerex.net/like-our-themes-get-them-at-creative-market/" target="_blank">themerex.net/like-our-theme…</a>
                                            #ThemeRex #WordPress #creativemarket
                                        </p>
                                    </li>
                                    <li class="sc_twitter_item swiper-slide last">
                                        <p>
                                            <span class="twitterIco"></span>
                                            Looking Ahead: Trendy #blogger and #portfolio Site #Templates in Production!
                                            <a href="http://themerex.net/looking-ahead-trendy-blogger-and-portfolio-site-templates-in-production/" target="_blank">themerex.net/looking-ahead-…</a>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                            <ul class="flex-direction-nav">
                                <li>
                                    <a class="flex-prev" href="#"></a>
                                </li>
                                <li>
                                    <a class="flex-next" href="#"></a>
                                </li>
                            </ul>
                        </div>
                        <span class="twitterAuthor">
                            Follow us on 
                            <a href="https://twitter.com/Theme_REX" target="_blank">Twitter</a> for our latest updates
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-app-layout>