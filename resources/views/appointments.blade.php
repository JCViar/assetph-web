<x-app-layout>
    <x-slot name="title">
        Appointments
    </x-slot>
    <section class="dark_section image_bg_4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="container">
                        <h2 class="sc_title_align_center sc_title margin_top_mini color_1">Make an Appointment</h2>
                        <div class="sc_content margin_bottom_mini sc_aligncenter text_styling">
                            That is why we are ready to become your guide through Asset Ph!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="light_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="booking_main_container" id="booking_container_all">
                        <div class="booking_header_container">
                            <div class="booking_month_container_all">
                                <div class="booking_month_container booking_month_container_custom">
                                    <div class="booking_font_custom booking_month_name booking_month_name_custom" id="month_name">October</div>
                                    <div class="booking_font_custom booking_month_year booking_year_name_custom" id="booking_month_year">2015</div>
                                </div>
                                <div class="booking_month_nav_container" id="booking_month_nav">
                                    <div class="booking_mont_nav_button_container" id="booking_month_nav_prev">
                                        <a href="#" class="booking_month_nav_button booking_month_navigation_button_custom">
                                            <img src="{{ asset('assets/images/icon/prev.png') }}">
                                        </a>
                                    </div>
                                    <div class="booking_mont_nav_button_container" id="booking_month_nav_next">
                                        <a href="#" class="booking_month_nav_button booking_month_navigation_button_custom">
                                            <img src="{{ asset('assets/images/icon/next.png') }}">
                                        </a>
                                    </div>
                                </div>
                                <div class="booking_cleardiv">
                                </div>
                            </div>
                            <div class="booking_select_calendar_container">
                                <div class="booking_float_right booking_font_13" id="booking_calendar_select_label">Select the calendar:</div>
                                <div class="booking_cleardiv"></div>
                                <div class="booking_float_right" id="booking_calendar_select">
                                    <select name="calendar" id="calendar_select_input" onchange="javascript:updateCalendar(this.options[this.selectedIndex].value);">
                                        <option value="1" selected>Demo calendar </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="booking_cleardiv">
                        </div>
                        <div class="booking_calendar_container_all">
                            <div class="booking_name_days_container" id="booking_name_days_container">
                                <div class="booking_font_custom booking_day_name booking_weekdays_custom">Monday</div>
                                <div class="booking_font_custom booking_day_name booking_weekdays_custom">Tuesday</div>
                                <div class="booking_font_custom booking_day_name booking_weekdays_custom">Wednesday</div>
                                <div class="booking_font_custom booking_day_name booking_weekdays_custom">Thursday</div>
                                <div class="booking_font_custom booking_day_name booking_weekdays_custom">Friday</div>
                                <div class="booking_font_custom booking_day_name booking_weekdays_custom">Saturday</div>
                                <div class="booking_font_custom booking_day_name booking_weekdays_custom">Sunday</div>
                            </div>
                            <div class="days_container_all" id="booking_calendar_container">
                                <div class="booking_day_container booking_day_grey booking_font_14">
                                    <a class="booking_font_14"></a>
                                </div>
                                <div class="booking_day_container booking_day_grey booking_font_14">
                                    <a class="booking_font_14"></a>
                                </div>
                                <div class="booking_day_container booking_day_grey booking_font_14">
                                    <a class="booking_font_14"></a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="1" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">1</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_black booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="2" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">2</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="3" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">3</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="4" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">4</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="5" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">5</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="6" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">6</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="7" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">7</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="8" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">8</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="9" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">9</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="10" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">10</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="11" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">11</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="12" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">12</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="13" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">13</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="14" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">14</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="15" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">15</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="16" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">16</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="17" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">17</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="18" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">18</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="19" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">19</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="20" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">20</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="21" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">21</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="22" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">22</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="23" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">23</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="24" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">24</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="25" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">25</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="26" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">26</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="27" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">27</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="28" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">28</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="29" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">29</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="30" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">30</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_white booking_font_14">
                                    <a class="booking_font_14" year="2015" month="10" day="31" popup="1" over="0">
                                        <div class="booking_day_number booking_font_14">31</div>
                                        <div class="booking_day_book booking_font_14"></div>
                                        <div class="booking_cleardiv"></div>
                                        <div class="booking_day_slots booking_font_14">
                                        Not available</div>
                                    </a>
                                </div>
                                <div class="booking_day_container booking_day_grey booking_font_14">
                                    <a class="booking_font_14"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-app-layout>