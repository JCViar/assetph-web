<!DOCTYPE html>
<html lang="en-US">
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="icon" type="image/x-icon" href="{{ asset('assets/images/icon/favicon.ico') }}">
	<title>Asset Ph | single property</title>
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
	<link rel="stylesheet" id="rs-plugin-settings-css" href="{{ asset('assets/js/vendor/revslider/css/settings.css') }}" type="text/css" media="all">
	<link rel="stylesheet" id="fontello-css" href="{{ asset('assets/css/fontello.css') }}" type="text/css" media="all">
	<link rel="stylesheet" id="packed-style-css" href="{{ asset('assets/css/_packed.css') }}" type="text/css" media="all">
	<link rel="stylesheet" id="main-style-css" href="{{ asset('assets/css/main_style.css') }}" type="text/css" media="all">
	<link rel="stylesheet" id="theme-skin-css" href="{{ asset('assets/css/general.css') }}" type="text/css" media="all">
	<style id="theme-skin-inline-css" type="text/css"></style>
	<link rel="stylesheet" id="responsive-css" href="{{ asset('assets/css/responsive.css') }}" type="text/css" media="all">

</head>
<body class="page themerex_body fullscreen top_panel_above theme_skin_general usermenu_hide responsive_menu_show compact_responsive_menu">

<div class="main_content">
	<div class="boxedWrap">
		<div class="fullScreenSlider">
			<header class="noFixMenu menu_left without_user_menu">
				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-12">
							<div class="topWrapFixed"></div>
							<div class="topWrap styleShaded">
								<div class="mainmenu_area">
									<div class="wrap_logo">
										<div class="logo with_text">
											<a href="{{ route('home') }}">
												<img src="{{ asset('assets/images/logo/46x60.png') }}" class="logo_main" alt="">
												<img src="{{ asset('assets/images/logo/46x60.png') }}" class="logo_fixed" alt="">
												<div class="logo_info">
													<span class="logo_text">
														<b>Asset Ph</b>
													</span>
													<span class="logo_slogan">single property</span>
												</div>
											</a>
										</div>
									</div>
									<div class="wrap_menu">
										<a href="#" class="openResponsiveMenu"></a>
									</div>
									<nav role="navigation" class="menuTopWrap topMenuStyleLine">
										<ul id="mainmenu" class="">
											<li class="menu-item current-menu-item current-menu-ancestor menu-item-has-children">
												<a href="{{ route('home') }}" class="sf-with-ul">Home</a>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

			<div id="mainslider_4" class="sliderHomeBullets  slider_engine_revo slider_alias_main">
				<div id="rev_slider_wrapper" class="rev_slider_wrapper fullscreen-container" style="background-color:#E9E9E9;padding:0px;">
					<!-- START REVOLUTION SLIDER 4.6.5 fullscreen mode -->
					<div id="rev_slider" class="rev_slider fullscreenbanner" style="display:none;">
						<ul>	<!-- SLIDE  -->
							<li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-thumb="{{ asset('assets/images/slider/320x200.png') }}"  data-saveperformance="off" >
								<!-- MAIN IMAGE -->
								<img src="{{ asset('assets/images/slider/1900x964.png') }}"  alt="presentation-1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
								<!-- LAYERS -->
							</li>
							<!-- SLIDE  -->
							<li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-thumb="{{ asset('assets/images/slider/320x200.png') }}"  data-saveperformance="off" >
								<!-- MAIN IMAGE -->
								<img src="{{ asset('assets/images/slider/1900x963.png') }}"  alt="presentation-2"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
								<!-- LAYERS -->
							</li>
							<!-- SLIDE  -->
							<li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-thumb="{{ asset('assets/images/slider/320x200.png') }}"  data-saveperformance="off" >
								<!-- MAIN IMAGE -->
								<img src="{{ asset('assets/images/slider/1900x961.png') }}"  alt="presentation-3"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
								<!-- LAYERS -->
							</li>
							<!-- SLIDE  -->
							<li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-thumb="{{ asset('assets/images/slider/320x200.png') }}"  data-saveperformance="off" >
								<!-- MAIN IMAGE -->
								<img src="{{ asset('assets/images/slider/1900x961.png') }}"  alt="presentation-4"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
								<!-- LAYERS -->
							</li>
						</ul>
					</div>
				</div>
			</div>

		</div>

		<section class="footerContentWrap no_padding_container light_section">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<footer class="footerWrap footerStyleDark">
							<div class="container footerWidget widget_area sidebarStyleDark">
								<aside class="col-md-3 col-sm-6 widgetWrap widget widget_socials">
									<div class="widget_inner">
										<div class="logo">
											<a href="{{ route('home') }}">
												<img src="{{ asset('assets/images/logo/46x60.png') }}" alt="">
												<div class="logo_info">
													<span class="logo_text">
														<b>Asset Ph</b>
													</span>
													<span class="logo_slogan">single property</span>
												</div>
											</a>
										</div>
										<div class="logo_descr">
											This is the perfect four bedroom family home on an idyllic block, in one of Chicago's nicest suburbs. With three bedrooms and a master suite, there is plenty of space for all.
										</div>
										<div class="logo_socials socPage">
											<ul>
												<li>
													<a class="social_icons icon-twitter" target="_blank" href="https://twitter.com/Theme_REX"></a>
												</li>
												<li>
													<a class="social_icons icon-facebook" target="_blank" href="https://www.facebook.com/themerex"></a>
												</li>
												<li>
													<a class="social_icons icon-gplus" target="_blank" href="https://plus.google.com/102189073109602153696"></a>
												</li>
												<li>
													<a class="social_icons icon-vimeo" target="_blank" href="#"></a>
												</li>
												<li>
													<a class="social_icons icon-pinterest" target="_blank" href="#"></a>
												</li>
											</ul>
										</div>
									</div>
								</aside>
								<aside class="col-md-3 col-sm-6 widgetWrap widget widget_recent_comments">
									<h5 class="title">Recent Comments</h5>
									<ul id="recentcomments">
										<li class="recentcomments">
											<span class="comment-author-link">Admin</span>
											on 
											<a href="#">Is Condo Life For You?</a>
										</li>
										<li class="recentcomments">
											<span class="comment-author-link">Admin</span> 
											on 
											<a href="#">New Post With Image</a>
										</li>
										<li class="recentcomments">
											<span class="comment-author-link">Admin</span> 
											on 
											<a href="#">House Market Indicators</a>
										</li>
										<li class="recentcomments">
											<span class="comment-author-link">
												<a href="http://themeforest.net/user/ThemeREX/portfolio" rel="external nofollow" class="url">John Brown</a>
											</span> 
											on 
											<a href="#">Is Condo Life For You?</a>
										</li>
									</ul>
								</aside>
								<aside class="col-md-3 col-sm-6 widgetWrap widget widget_twitter">
									<h5 class="title">Twitter</h5>
									<ul>
										<li class="theme_text">
											<a href="https://twitter.com/Theme_REX" class="username" target="_blank">@Theme_REX</a> Like Our Themes? Get Them at Creative Market! <a href="http://themerex.net/like-our-themes-get-them-at-creative-market/" target="_blank">themerex.net/like-our-theme…</a> #ThemeRex #WordPress #creativemarket</li>
										<li class="theme_text last">
											<a href="https://twitter.com/Theme_REX" class="username" target="_blank">@Theme_REX</a> Looking Ahead: Trendy #blogger and #portfolio Site #Templates in Production!
											<a href="http://themerex.net/looking-ahead-trendy-blogger-and-portfolio-site-templates-in-production/" target="_blank">themerex.net/looking-ahead-…</a>
										</li>
									</ul>
								</aside>
								<aside class="col-md-3 col-sm-6 widgetWrap widget widget_recent_posts">
									<h5 class="title">Recent Posts</h5>
									<article class="post_item with_thumb first">
										<div class="post_thumb">
											<img alt="New Post With Image" src="{{ asset('assets/images/90x70.png') }}">
										</div>
										<h5 class="post_title">
											<a href="#">New Post With Image</a>
										</h5>
										<div class="post_info">
										<span class="post_date">January 6, 2015</span>
										<span class="post_comments">
											<a href="#">
												<span class="comments_icon icon-eye"></span>
												<span class="post_comments_number">709</span>
											</a>
										</span>
										</div>
									</article>
									<article class="post_item with_thumb">
										<div class="post_thumb">
											<img alt="House Market Indicators" src="{{ asset('assets/images/90x70.png') }}">
										</div>
										<h5 class="post_title">
											<a href="#">House Market Indicators</a>
										</h5>
										<div class="post_info">
											<span class="post_date">December 10, 2014</span>
											<span class="post_comments">
												<a href="#">
													<span class="comments_icon icon-eye"></span>
													<span class="post_comments_number">531</span>
												</a>
											</span>
										</div>
									</article>
									<article class="post_item with_thumb">
										<div class="post_thumb">
											<img alt="Post With Image" src="{{ asset('assets/images/90x70.png') }}">
										</div>
										<h5 class="post_title">
											<a href="#">Post With Image</a>
										</h5>
										<div class="post_info">
											<span class="post_date">December 8, 2014</span>
											<span class="post_comments">
												<a href="#">
													<span class="comments_icon icon-eye"></span>
													<span class="post_comments_number">106</span>
												</a>
											</span>
										</div>
									</article>
								</aside>
							</div>
						</footer>
						<div class="copyWrap">
							<div class="copy container">
								<div class="copyright">
									ThemeREX © 2014 All Rights Reserved
									<a href="#">Terms of Use</a>
									and
									<a href="#">Privacy Policy</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>
</div>

<div class="preloader">
    <div class="preloader_image"></div>
</div>

	<script type="text/javascript" src="{{ asset('assets/js/vendor/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/vendor/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/vendor/jquery-migrate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/vendor/revslider/js/jquery.themepunch.tools.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/vendor/revslider/js/jquery.themepunch.revolution.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/custom/_main.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/custom/_packed.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/custom/custom_menu.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/vendor/core.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/custom/shortcodes_init.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/custom/_utils.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/custom/_front.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/vendor/draggable.min.js') }}"></script>

</body>
</html>