<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    
    <x-head title="{{ $title ?? '' }} | Asset Ph" />

    <body class="home page themerex_body fullscreen top_panel_above theme_skin_general usermenu_show">
        <div id="app">
            <div class="main_content">
                <div class="boxedWrap">
                    
                    <x-header />
                    @if (Request::is(['*gallery*', '*tour*', '*faqs*', '*blog*', '*tools*']))
                        <x-breadcrumb />
                    @endif

                    {{ $slot }}

                    <x-footer />

                </div>
            </div>
            <div class="preloader">
                <div class="preloader_image"></div>
            </div>
        </div>
        
    <x-script />
    
    </body>
</html>
