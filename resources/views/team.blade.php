<x-app-layout>
    <x-slot name="title">
        Agents Team
    </x-slot>
    <section class="dark_section image_bg_4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="container">
                        <h2 class="sc_title_align_center sc_title sc_title_underline  margin_top_small color_1">We are a passionate team of real estate brokers</h2>
                        <div class="sc_content margin_bottom_small sc_aligncenter text_styling">
                            Our agency is a full-service real estate brokerage representing<br>
                            clients in a broad spectrum of property classes. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="light_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">			
                    <h2 class="sc_title_align_center sc_title sc_title_underline color_1">Our services</h2>
                    <div class="sc_content sc_subtitle  margin_bottom_small sc_aligncenter text_styling">
                        Asset Ph agency extends far beyond what a regular real estate firm offers.<br />
                        And we stand behind our promise to deliver timely service. 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="sc_skills sc_skills_pie" data-type="pie" data-subtitle="Skills">
                    <div class="columnsWrap sc_skills_columns">
                        <div class="sc_skills_column col-md-3 col-sm-6">
                            <div class="sc_skills_item sc_skills_style_1">
                                <canvas></canvas>
                                <div class="sc_skills_total" data-start="0" data-stop="80" data-step="1" data-steps="100" data-max="100" data-speed="33" data-duration="2640" data-color="#1f9ad6" data-easing="easeOutCirc" data-ed="%">0%</div>
                            </div>
                            <div class="sc_skills_info">Family residential</div>
                        </div>
                        <div class="sc_skills_column col-md-3 col-sm-6">
                            <div class="sc_skills_item sc_skills_style_1">
                                <canvas></canvas>
                                <div class="sc_skills_total" data-start="0" data-stop="75" data-step="1" data-steps="100" data-max="100" data-speed="22" data-duration="1650" data-color="#1f9ad6" data-easing="easeOutCirc" data-ed="%">0%</div>
                            </div>
                            <div class="sc_skills_info">Vacation rentals</div>
                        </div>
                        <div class="sc_skills_column col-md-3 col-sm-6">
                            <div class="sc_skills_item sc_skills_style_1">
                                <canvas></canvas>
                                <div class="sc_skills_total" data-start="0" data-stop="95" data-step="1" data-steps="100" data-max="100" data-speed="23" data-duration="2185" data-color="#1f9ad6" data-easing="easeOutCirc" data-ed="%">0%</div>
                            </div>
                            <div class="sc_skills_info">Residential leasing</div>
                        </div>
                        <div class="sc_skills_column col-md-3 col-sm-6">
                            <div class="sc_skills_item sc_skills_style_1">
                                <canvas></canvas>
                                <div class="sc_skills_total" data-start="0" data-stop="95" data-step="1" data-steps="100" data-max="100" data-speed="32" data-duration="3040" data-color="#1f9ad6" data-easing="easeOutCirc" data-ed="%">0%</div>
                            </div>
                            <div class="sc_skills_info">New development</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> 

    <section class="grey_section" >
        <div class="container">
            <div class="row">
                <div class="col-sm-12">		
                    <h2 class="sc_title_align_center sc_title sc_title_underline color_1">Our Team</h2>
                    <div class="sc_content sc_aligncenter text_styling margin_bottom_small sc_subtitle">
                        Asset Ph agency assembled the best team of professional real estate brokers<br />
                        who offer only top quality services. 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="sc_team">
                    <div class="col-md-3 col-sm-6">
                        <div class="sc_team_item sc_team_item_1 first">
                            <div class="sc_team_item_avatar">
                                <img class="wp-post-image" width="370" height="370" alt="t2.jpg" src="images/team/370x370.png">
                                <a class="hoverLink" href="#"></a>
                            </div>
                            <div class="sc_team_item_info">
                                <h4 class="sc_team_item_title">Amanda Doe</h4>
                                <div class="sc_team_item_position">Sales Manager</div>
                                <ul class="sc_team_item_socials">
                                    <li>
                                        <a href="https://twitter.com/Theme_REX" class="social_icons social_twitter icon-twitter" target="_blank"></a>
                                    </li>
                                    <li>
                                        <a href="https://plus.google.com/102189073109602153696" class="social_icons social_gplus icon-gplus" target="_blank"></a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/themerex" class="social_icons social_facebook icon-facebook" target="_blank"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="sc_team_item sc_team_item_2">
                            <div class="sc_team_item_avatar">
                                <img class="wp-post-image" width="370" height="370" alt="t4.jpg" src="images/team/370x370.png">
                                <a class="hoverLink" href="#"></a>
                            </div>
                            <div class="sc_team_item_info">
                                <h4 class="sc_team_item_title">Natalia Sitam</h4>
                                <div class="sc_team_item_position">Client Relations</div>
                                <ul class="sc_team_item_socials">
                                    <li>
                                        <a href="https://twitter.com/Theme_REX" class="social_icons social_twitter icon-twitter" target="_blank"></a>
                                    </li>
                                    <li>
                                        <a href="https://plus.google.com/102189073109602153696" class="social_icons social_gplus icon-gplus" target="_blank"></a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/themerex" class="social_icons social_facebook icon-facebook" target="_blank"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="sc_team_item sc_team_item_3">
                            <div class="sc_team_item_avatar">
                                <img class="wp-post-image" width="370" height="370" alt="t1.jpg" src="images/team/370x370.png">
                                <a class="hoverLink" href="#"></a>
                            </div>
                            <div class="sc_team_item_info">
                                <h4 class="sc_team_item_title">John Smith</h4>
                                <div class="sc_team_item_position">Senior Agent</div>
                                <ul class="sc_team_item_socials">
                                    <li>
                                        <a href="https://twitter.com/Theme_REX" class="social_icons social_twitter icon-twitter" target="_blank"></a>
                                    </li>
                                    <li>
                                        <a href="https://plus.google.com/102189073109602153696" class="social_icons social_gplus icon-gplus" target="_blank"></a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/themerex" class="social_icons social_facebook icon-facebook" target="_blank"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="sc_team_item sc_team_item_4">
                            <div class="sc_team_item_avatar">
                                <img class="wp-post-image" width="370" height="370" alt="t3.jpg" src="images/team/370x370.png">
                                <a class="hoverLink" href="#"></a>
                            </div>
                            <div class="sc_team_item_info">
                                <h4 class="sc_team_item_title">Martin Jackson</h4>
                                <div class="sc_team_item_position">Junior Broker</div>
                                <ul class="sc_team_item_socials">
                                    <li>
                                        <a href="https://twitter.com/Theme_REX" class="social_icons social_twitter icon-twitter" target="_blank"></a>
                                    </li>
                                    <li>
                                        <a href="https://plus.google.com/102189073109602153696" class="social_icons social_gplus icon-gplus" target="_blank"></a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/themerex" class="social_icons social_facebook icon-facebook" target="_blank"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> 

    <section class="light_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="sc_content sc_aligncenter text_styling sc_subtitle margin_bottom_small">
                        We create templates for your websites that will increase your reputation<br />
                        and highlight your professionalism and leadership. 
                    </div>
                    <div class="sc_content aligncenter">
                        <span class="sc_button sc_button_style_global sc_button_size_big squareButton global big">
                            <a href="#" class="">start a project</a>
                        </span>
                        <span class="sc_button sc_button_style_global sc_button_size_big squareButton global big">
                            <a href="#" class="">see our works</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-app-layout>