<x-app-layout>
    <x-slot name="title">
        Others
    </x-slot>
	<section class="light_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">			
					<h2 class="sc_title_align_center sc_title sc_title_underline  color_1">Price Table</h2>
					<div class="sc_content sc_subtitle sc_aligncenter text_styling">
						Asset Ph represents buyers and sellers with many<br />
						of  most distinguished properties. 
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="sc_pricing_light sc_pricing_table columns_4 alignCenter">
						<div class="sc_pricing_columns sc_pricing_column_1">
							<ul class="columnsAnimate">
								<li class="sc_pricing_data sc_pricing_title">Free</li>
								<li class="sc_pricing_data sc_pricing_price">
									<div class="sc_price_item">
										<span class="sc_price_currency">$</span>
										<div class="sc_price_money">29.99</div>
										<div class="sc_price_info">
											<div class="sc_price_period">/month</div>
										</div>
									</div>
								</li>
								<li class="sc_pricing_data">
									<b>No</b> support
								</li>
								<li class="sc_pricing_data">
									<b>No</b> update
								</li>
								<li class="sc_pricing_data">
									<b>1 user</b> acces
								</li>
								<li class="sc_pricing_data">
									<b>32 MB</b> bandwidth
								</li>
								<li class="sc_pricing_data">
									<b>1 user</b> only
								</li>
								<li class="sc_pricing_data">
									<b>No</b> Security
								</li>
								<li class="sc_pricing_data">
									<span class="sc_button sc_button_style_global sc_button_size_big squareButton global big">
										<a href="#" class="">make an app.</a>
									</span>
								</li>
							</ul>
						</div>
						<div class="active sc_pricing_columns sc_pricing_column_2">
							<ul class="columnsAnimate">
								<li class="sc_pricing_data sc_pricing_title">Personal</li>
								<li class="sc_pricing_data sc_pricing_price">
									<div class="sc_price_item">
										<span class="sc_price_currency">$</span>
										<div class="sc_price_money">59.99</div>
										<div class="sc_price_info">
											<div class="sc_price_period">/month</div>
										</div>
									</div>
								</li>
								<li class="sc_pricing_data">
									<b>Free</b> support
								</li>
								<li class="sc_pricing_data">
									<b>Free</b> update
								</li>
								<li class="sc_pricing_data">
									<b>up to 10 user</b> acces
								</li>
								<li class="sc_pricing_data">
									<b>10 GB</b> bandwidth
								</li>
								<li class="sc_pricing_data">
									<b>up to 10 user</b> only
								</li>
								<li class="sc_pricing_data">
									<b>Security Suite</b>
								</li>
								<li class="sc_pricing_data">
									<span class="sc_button sc_button_style_global sc_button_size_big squareButton global big">
										<a href="#" class="">make an app.</a>
									</span>
								</li>
							</ul>
						</div>
						<div class="sc_pricing_columns sc_pricing_column_3">
							<ul class="columnsAnimate">
								<li class="sc_pricing_data sc_pricing_title">Business</li>
								<li class="sc_pricing_data sc_pricing_price">
									<div class="sc_price_item">
										<span class="sc_price_currency">$</span>
										<div class="sc_price_money">79.99</div>
										<div class="sc_price_info">
											<div class="sc_price_period">/month</div>
										</div>
									</div>
								</li>
								<li class="sc_pricing_data">
									<b>Free</b> support
								</li>
								<li class="sc_pricing_data">
									<b>Free</b> update
								</li>
								<li class="sc_pricing_data">
									<b>up to 40 user</b> acces
								</li>
								<li class="sc_pricing_data">
									<b>100 GB</b> bandwidth
								</li>
								<li class="sc_pricing_data">
									<b>up to 100 user</b> only
								</li>
								<li class="sc_pricing_data">
									<b>Security Suite</b>
								</li>
								<li class="sc_pricing_data">
									<span class="sc_button sc_button_style_global sc_button_size_big squareButton global big">
										<a href="#" class="">make an app.</a>
									</span>
								</li>
							</ul>
						</div>
						<div class="sc_pricing_columns sc_pricing_column_4">
							<ul class="columnsAnimate">
								<li class="sc_pricing_data sc_pricing_title">Premium</li>
								<li class="sc_pricing_data sc_pricing_price">
									<div class="sc_price_item">
										<span class="sc_price_currency">$</span>
										<div class="sc_price_money">99.99</div>
										<div class="sc_price_info">
											<div class="sc_price_period">/month</div>
										</div>
									</div>
								</li>
								<li class="sc_pricing_data">
									<b>Free</b> support
								</li>
								<li class="sc_pricing_data">
									<b>Free</b> update
								</li>
								<li class="sc_pricing_data">
									<b>up to 50 user</b> acces
								</li>
								<li class="sc_pricing_data">
									<b>150 GB</b> bandwidth
								</li>
								<li class="sc_pricing_data">
									<b>up to 150 user</b> only
								</li>
								<li class="sc_pricing_data">
									<b>Security Suite</b>
								</li>
								<li class="sc_pricing_data">
									<span class="sc_button sc_button_style_global sc_button_size_big squareButton global big">
										<a href="#" class="">make an app.</a>
									</span>
								</li>
							</ul>
						</div>
					</div>
					<div class="sc_content margin_top_middle sc_aligncenter">
						Three ways to pay. All plans include the same features.<br />
						&#8216;Pay Me&#8217; payments are always free on all plans: 0% per credit card transaction. 
					</div>
				</div>
			</div>
		</div>
	</section> 

	<section class="grey_section" >
		<div class="container">
			<div class="row">
				<div class="col-sm-12">		
					<h2 class="sc_title_align_center sc_title sc_title_regular color_1">Have a Questions?</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<h5 class="sc_title_align_left sc_title sc_title_regular  color_1">Where do I get started?</h5>
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
					<h5 class="sc_title_align_left sc_title sc_title_regular  margin_top_small color_1">What happens at the end of the paid period?</h5> 
					Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
					<h5 class="sc_title_align_left sc_title sc_title_regular  margin_top_small color_1">Do you accept purchase orders?</h5> 
					Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidat proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
				</div>
				<div class="col-sm-6">
					<h5 class="sc_title_align_left sc_title sc_title_regular  color_1">How can I cancel my subscription?</h5> 
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
					<h5 class="sc_title_align_left sc_title sc_title_regular  margin_top_small color_1">What about one timets?</h5> 
					Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquncidunnam aliquam quaerat voluptatem. 
					<h5 class="sc_title_align_left sc_title sc_title_regular  margin_top_small color_1">Do I have to pay for each mag I want to publish?</h5> 
					Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. 
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 no_margin_top">
					<span class="sc_button sc_button_style_accent_2 sc_button_size_banner alignleft squareButton fullSize accent_2 banner  ico">
						<a href="#" class="icon-life-buoy">Have More Questions?</a>
					</span>
				</div>
				<div class="col-sm-6 no_margin_top">
					<span class="sc_button sc_button_style_accent_2 sc_button_size_banner alignleft squareButton fullSize accent_2 banner  ico">
						<a href="#" class="icon-comments">Open a ticket</a>
					</span>
				</div>
			</div>
		</div>
	</section> 
</x-app-layout>