<x-app-layout>
    <x-slot name="title">
        Blog
    </x-slot>
	<section class="light_section without_sidebar">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="masonryWrap no_padding_mansory">
						<div class="masonry masonry-colums-2 isotopeNOanim" data-columns="2">
							<article class="isotopeElement post_format_quote">
								<div class="isotopePadding bg_post">
									<div class="post_wrap_part">
										<div class="post_format_wrap postQuote">
											<blockquote cite="#" class="sc_quote sc_quote_style_1">
												<p>Look deep into nature, and then you will understand everything better.</p>
												<p class="sc_quote_title">
													<a href="#">Albert Einstein</a>
												</p>
											</blockquote>
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">January 1, 2015</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_audio">
								<div class="isotopePadding bg_post">
									<div class="audio_container with_info">
										<span class="audio_info">Lily hunter</span>
										<h4 class="audio_info">Insert Audio Title Here</h4>
										<div>
											<audio title="Insert Audio Title Here" src="sounds/laura.mp3"></audio>
										</div>
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Are You A Good Tenant?</a>
										</h4>
										<div class="post_format_wrap postAudio">
										The factors that make a good tenant can vary, based on the landlord and the individual renter. Few universal characteristics hold true...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 11, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_standard">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"></a>
											</span>
										</div>
										<img alt="Real Estate Traps To Avoid" src="{{ asset('assets/images/blog/714x474.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Real Estate Traps To Avoid</a>
										</h4>
										<div class="post_format_wrap postStandard">
											Since a home buyer apportions the bulk of his savings and income to a single investment, he wants to save as much as possible. Property developers...
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 11, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 1" href="#">1</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_standard">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Common Refinance Mistakes" src="{{ asset('assets/images/isotope/714x916.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Common Refinance Mistakes</a>
										</h4>
										<div class="post_format_wrap postStandard">
										If you&#8217;re like many homeowners out there, you may be thinking about taking advantage of the low mortgage rates out there and refinance your...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 11, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_status">
								<div class="isotopePadding bg_post">
									<div class="post_wrap_part">
										<div class="post_format_wrap postStatus">
											<p>Nothing in life is to be feared, it is only to be understood. Now is the time to understand more, so that we may fear less&#8230;</p>
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 8, 2014</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_gallery">
								<div class="isotopePadding bg_post">
									<div class="sc_section post_thumb thumb">
										<div id="sc_slider_303341080" class="sc_slider sc_slider_swiper swiper-slider-container sc_slider_controls" data-old-width="714" data-old-height="402" data-interval="6842">
											<ul class="slides swiper-wrapper">
												<li class="swiper-slide">
													<img src="{{ asset('assets/images/blog/714x402.png') }}" alt="">
												</li>
												<li class="swiper-slide">
													<img src="{{ asset('assets/images/blog/714x402.png') }}" alt="">
												</li>
												<li class="swiper-slide">
													<img src="{{ asset('assets/images/blog/714x402.png') }}" alt="">
												</li>
											</ul>
											<ul class="flex-direction-nav">
												<li>
													<a class="flex-prev" href="#">
													</a>
												</li>
												<li>
													<a class="flex-next" href="#">
													</a>
												</li>
											</ul>
										</div>
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Gallery Post Format</a>
										</h4>
										<div class="post_format_wrap postGallery">
										Don’t be afraid of multiple offers. You still have some control. There are a few things that can happen if you get into competition. The seller...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 8, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 2" href="#">2</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_link">
								<div class="isotopePadding bg_post">
									<div class="post_wrap_part">
										<div class="post_format_wrap postLink">
											<p>Lily Hunter wrote about 
												<a href="http://themeforest.net/user/ThemeREX/portfolio">ThemeRex</a>
											</p>
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="http://themeforest.net/user/ThemeREX/portfolio" class="post_date">December 8, 2014</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_chat">
								<div class="isotopePadding bg_post">
									<div class="post_wrap_part">
										<div class="post_format_wrap postChat">
											<div class="sc_chat">
												<p class="sc_quote_title">
													<a href="#">Mat Jefferson:</a>
												</p>
												<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
											</div>
											<div class="sc_chat">
												<p class="sc_quote_title">
													<a href="#">Maria Anderson:</a>
												</p>
												<p>Labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi commodo consequat.</p>
											</div>
											<div class="sc_chat">
												<p class="sc_quote_title">
													<a href="#">Mat Jefferson:</a>
												</p>
												<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
											</div>
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 8, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_aside">
								<div class="isotopePadding bg_post">
									<div class="post_wrap_part">
										<div class="post_format_wrap postAside">
											<p>Choose a job you love, and you will never have to work a day in your life.</p>
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 8, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_standard">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="What Is Housing Сooperative" src="{{ asset('assets/images/blog/714x285.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">What Is Housing Сooperative</a>
										</h4>
										<div class="post_format_wrap postStandard">
										A housing cooperative, or co-op, is a legal entity, usually a corporation, which owns real estate, consisting of one or more residential...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 8, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_standard">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Reverse Mortgages" src="{{ asset('assets/images/blog/714x476.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Reverse Mortgages</a>
										</h4>
										<div class="post_format_wrap postStandard">
										A reverse mortgage is a home loan that provides cash payments based on home equity. Homeowners normally &#8220;defer payment of the loan until...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">November 11, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_standard">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Buying Luxury Real Estate" src="{{ asset('assets/images/blog/714x474.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Buying Luxury Real Estate</a>
										</h4>
										<div class="post_format_wrap postStandard">
										There is not one design, style or size that embodies a luxury home. It could be a sprawling 15,000-square-foot French manor set on several rolling...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">November 11, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_standard last">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Questions For Your Realtor" src="{{ asset('assets/images/isotope/714x896.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Questions For Your Realtor</a>
										</h4>
										<div class="post_format_wrap postStandard">
										If you are like most home buyers and sellers, chances are you&#8217;ll want a qualified real estate agent to help you through the process. Agents...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">September 11, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
						</div>
					</div>
					<div id="pagination" class="pagination">
						<ul>
							<li class="pager_current active squareButton light">
								<span>1</span>
							</li>
							<li class="squareButton light">
								<a href="#">2</a>
							</li>
							<li class="pager_next squareButton light">
								<a href="#">&raquo;</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
</x-app-layout>