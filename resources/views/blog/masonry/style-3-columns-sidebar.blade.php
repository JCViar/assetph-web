<x-app-layout>
    <x-slot name="title">
        Blog
    </x-slot>
	<section class="light_section with_sidebar">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-12">
					<div class="masonryWrap no_padding_mansory">
						<div class="masonry masonry-colums-3 isotopeNOanim" data-columns="3">
							<article class="isotopeElement post_format_standard">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"></a>
											</span>
										</div>
										<img alt="Real Estate Traps To Avoid" src="{{ asset('assets/images/blog/714x474.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Real Estate Traps To Avoid</a>
										</h4>
										<div class="post_format_wrap postStandard">
											Since a home buyer apportions the bulk of his savings and income to a single investment, he wants to save as much as possible. Property developers...
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 11, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 1" href="#">1</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_standard">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Common Refinance Mistakes" src="{{ asset('assets/images/blog/714x916.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Common Refinance Mistakes</a>
										</h4>
										<div class="post_format_wrap postStandard">
										If you&#8217;re like many homeowners out there, you may be thinking about taking advantage of the low mortgage rates out there and refinance your...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 11, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_gallery">
								<div class="isotopePadding bg_post">
									<div class="sc_section post_thumb thumb">
										<div id="sc_slider_303341080" class="sc_slider sc_slider_swiper swiper-slider-container sc_slider_controls" data-old-width="714" data-old-height="402" data-interval="6842">
											<ul class="slides swiper-wrapper">
												<li class="swiper-slide">
													<img src="{{ asset('assets/images/blog/714x402.png') }}" alt="">
												</li>
												<li class="swiper-slide">
													<img src="{{ asset('assets/images/blog/714x402.png') }}" alt="">
												</li>
												<li class="swiper-slide">
													<img src="{{ asset('assets/images/blog/714x402.png') }}" alt="">
												</li>
											</ul>
											<ul class="flex-direction-nav">
												<li>
													<a class="flex-prev" href="#">
													</a>
												</li>
												<li>
													<a class="flex-next" href="#">
													</a>
												</li>
											</ul>
										</div>
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Gallery Post Format</a>
										</h4>
										<div class="post_format_wrap postGallery">
										Don’t be afraid of multiple offers. You still have some control. There are a few things that can happen if you get into competition. The seller...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 8, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 2" href="#">2</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_link">
								<div class="isotopePadding bg_post">
									<div class="post_wrap_part">
										<div class="post_format_wrap postLink">
											<p>Lily Hunter wrote about 
												<a href="http://themeforest.net/user/ThemeREX/portfolio">ThemeRex</a>
											</p>
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="http://themeforest.net/user/ThemeREX/portfolio" class="post_date">December 8, 2014</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_chat">
								<div class="isotopePadding bg_post">
									<div class="post_wrap_part">
										<div class="post_format_wrap postChat">
											<div class="sc_chat">
												<p class="sc_quote_title">
													<a href="#">Mat Jefferson:</a>
												</p>
												<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
											</div>
											<div class="sc_chat">
												<p class="sc_quote_title">
													<a href="#">Maria Anderson:</a>
												</p>
												<p>Labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi commodo consequat.</p>
											</div>
											<div class="sc_chat">
												<p class="sc_quote_title">
													<a href="#">Mat Jefferson:</a>
												</p>
												<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
											</div>
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 8, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_aside">
								<div class="isotopePadding bg_post">
									<div class="post_wrap_part">
										<div class="post_format_wrap postAside">
											<p>Choose a job you love, and you will never have to work a day in your life.</p>
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 8, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_standard">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="What Is Housing Сooperative" src="{{ asset('assets/images/blog/714x285.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">What Is Housing Сooperative</a>
										</h4>
										<div class="post_format_wrap postStandard">
										A housing cooperative, or co-op, is a legal entity, usually a corporation, which owns real estate, consisting of one or more residential...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 8, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_standard">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Reverse Mortgages" src="{{ asset('assets/images/blog/714x476.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Reverse Mortgages</a>
										</h4>
										<div class="post_format_wrap postStandard">
										A reverse mortgage is a home loan that provides cash payments based on home equity. Homeowners normally &#8220;defer payment of the loan until...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">November 11, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_standard">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Buying Luxury Real Estate" src="{{ asset('assets/images/blog/714x474.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Buying Luxury Real Estate</a>
										</h4>
										<div class="post_format_wrap postStandard">
										There is not one design, style or size that embodies a luxury home. It could be a sprawling 15,000-square-foot French manor set on several rolling...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">November 11, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_standard last">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Questions For Your Realtor" src="{{ asset('assets/images/blog/714x896.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Questions For Your Realtor</a>
										</h4>
										<div class="post_format_wrap postStandard">
										If you are like most home buyers and sellers, chances are you&#8217;ll want a qualified real estate agent to help you through the process. Agents...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">September 11, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
						</div>
						<div id="pagination" class="pagination">
							<ul>
								<li class="pager_current active squareButton light">
									<span>1</span>
								</li>
								<li class="squareButton light">
									<a href="#">2</a>
								</li>
								<li class="pager_next squareButton light">
									<a href="#">&raquo;</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div id="sidebar_main" class="widget_area sidebar_main sidebar sidebarStyleDark" role="complementary">
						<aside class=" widgetWrap widget widget_categories">
							<h5 class="title">Categories</h5>
							<ul>
								<li class="cat-item current-cat-parent dropMenu">
									<a href="blog-classic-style-1-columns.html">Classic Style</a> (14)
									<ul class="children">
										<li class="cat-item">
											<a href="blog-classic-style-1-columns.html">1 Column</a> (9)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-2-columns.html">2 Columns</a> (11)
										</li>
										<li class="cat-item current-cat">
											<a href="blog-classic-style-2-columns-sidebar.html">2 Columns + sidebar</a> (11)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-3-columns.html">3 Columns</a> (10)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-3-columns-sidebar.html">3 Columns + sidebar</a> (11)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-4-columns.html">4 Columns</a> (10)
										</li>
									</ul>
								</li>
								<li class="cat-item">
									<a href="blog-classic-style-large.html">Classic Style Large</a> (13)
								</li>
								<li class="cat-item">
									<a href="blog-classic-style-small.html">Classic Style Small</a> (12)
								</li>
								<li class="cat-item">
									<a href="#">Contemporary</a> (15)
								</li>
								<li class="cat-item">
									<a href="#">Cottage</a> (5)
								</li>
								<li class="cat-item">
									<a href="gallery-grid-alternative.html">Grid Alternative</a> (22)
								</li>
								<li class="cat-item dropMenu">
									<a href="blog-masonry-style-2-columns.html">Masonry demo</a> (18)
									<ul class="children">
										<li class="cat-item">
											<a href="blog-masonry-style-2-columns.html">2 Columns</a> (17)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-2-columns-sidebar.html">2 Columns + sidebar</a> (17)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-3-columns.html">3 Columns</a> (17)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-3-columns-sidebar.html">3 Columns + sidebar</a> (13)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-4-columns.html">4 Columns</a> (16)
										</li>
									</ul>
								</li>
								<li class="cat-item dropMenu">
									<a href="gallery-classic-style-1-column.html">Portfolio Classic</a> (22)
									<ul class="children">
										<li class="cat-item">
											<a href="gallery-classic-style-1-column.html">1 Column</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-1-column-sidebar.html">1 Column + sidebar</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-2-columns.html">2 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-2-columns-sidebar.html">2 Columns + sidebar</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-3-columns.html">3 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-3-columns-sidebar.html">3 Columns + sidebar</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-4-columns.html">4 Columns</a> (22)
										</li>
									</ul>
								</li>
								<li class="cat-item dropMenu">
									<a href="gallery-grid-style-2-columns.html">Portfolio Grid</a> (22)
									<ul class="children">
										<li class="cat-item">
											<a href="gallery-grid-style-2-columns.html">2 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-2-columns-fullscreen.html">2 Columns fullscreen</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-3-columns.html">3 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-3-columns-fullscreen.html">3 Columns fullscreen</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-4-columns.html">4 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-4-columns-fullscreen.html">4 Columns fullscreen</a> (22)
										</li>
									</ul>
								</li>
								<li class="cat-item dropMenu">
									<a href="gallery-masonry-style-2-columns.html">Portfolio Masonry</a> (12)
									<ul class="children">
										<li class="cat-item">
											<a href="gallery-masonry-style-2-columns.html">2 Columns</a> (11)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-2-columns-sidebar.html">2 Columns + sidebar</a> (12)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-3-columns.html">3 Columns</a> (12)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-3-columns-sidebar.html">3 Columns + sidebar</a> (12)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-4-columns.html">4 Columns</a> (12)
										</li>
									</ul>
								</li>
								<li class="cat-item">
									<a href="gallery-grid-alternative.html">Projects</a> (9)
								</li>
							</ul>
						</aside>
						<aside class="widgetWrap widget widget_recent_comments">
							<h5 class="title">Recent Comments</h5>
							<ul>
								<li class="recentcomments">
									<span class="comment-author-link">Admin</span> 
									on 
									<a href="#">Is Condo Life For You?</a>
								</li>
								<li class="recentcomments">
									<span class="comment-author-link">Admin</span> 
									on 
									<a href="#">New Post With Image</a>
								</li>
								<li class="recentcomments">
									<span class="comment-author-link">Admin</span> 
									on 
									<a href="#">House Market Indicators</a>
								</li>
							</ul>
						</aside>
						<aside class="widgetWrap widget widget_archive">
							<h5 class="title">Archives</h5>
							<ul>
								<li>
									<a href='#'>January 2015</a>&nbsp;(3)
								</li>
								<li>
									<a href='#'>December 2014</a>&nbsp;(59)
								</li>
								<li>
									<a href='#'>November 2014</a>&nbsp;(3)
								</li>
								<li>
									<a href='#'>September 2014</a>&nbsp;(1)
								</li>
								<li>
									<a href='#'>August 2014</a>&nbsp;(1)
								</li>
								<li>
									<a href='#'>July 2014</a>&nbsp;(2)
								</li>
								<li>
									<a href='#'>May 2014</a>&nbsp;(5)
								</li>
								<li>
									<a href='#'>February 2014</a>&nbsp;(1)
								</li>
							</ul>
						</aside>
						<aside class="widgetWrap widget widget_search">
							<form role="search" method="get" class="search-form" action="index.html">
								<input type="text" class="search-field" placeholder="Search" value="" name="s" title="Search for:" />
								<span class="search-button light ico">
									<a class="search-field icon-search" href="#"></a>
								</span>
							</form>
						</aside>
						<aside class="widgetWrap widget widget_calendar">
							<div id="calendar_wrap">
								<table class="calendar">
									<thead>
										<tr>
											<th class="curMonth" colspan="7">
												<a href="#" title="View posts for August 2015">August</a>
											</th>
										</tr>
										<tr>
											<th scope="col" title="Monday">Mon</th>
											<th scope="col" title="Tuesday">Tue</th>
											<th scope="col" title="Wednesday">Wed</th>
											<th scope="col" title="Thursday">Thu</th>
											<th scope="col" title="Friday">Fri</th>
											<th scope="col" title="Saturday">Sat</th>
											<th scope="col" title="Sunday">Sun</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td colspan="1" class="pad">&nbsp;</td>
											<td>
												<span>1</span>
											</td>
											<td>
												<span>2</span>
											</td>
											<td>
												<a href="#" title="Post Formats – Chat">3</a>
											</td>
											<td>
												<span>4</span>
											</td>
											<td>
												<a href="#" title="Vimeo Video Post">5</a>
											</td>
											<td>
												<span>6</span>
											</td>
										</tr>
										<tr>
											<td>
												<span>7</span>
											</td>
											<td>
												<a href="#" title="What Is Housing Сooperative">8</a>
											</td>
											<td>
												<span>9</span>
											</td>
											<td>
												<a href="#" title="Post format – Aside">10</a>
											</td>
											<td>
												<span>11</span>
											</td>
											<td>
												<span>12</span>
											</td>
											<td>
												<span>13</span>
											</td>
										</tr>
										<tr>
											<td>
												<a href="#" title="Post formats – Link">14</a>
											</td>
											<td>
												<a href="#" title="Audio Post">15</a>
											</td>
											<td>
												<span>16</span>
											</td>
											<td>
												<span>17</span>
											</td>
											<td>
												<a href="#" title="Gallery Post Format">18</a>
											</td>
											<td>
												<a href="#" title="Post With Image">19</a>
											</td>
											<td>
												<span>20</span>
											</td>
										</tr>
										<tr>
											<td>
												<span>21</span>
											</td>
											<td>
												<span>22</span>
											</td>
											<td>
												<a href="#" title="Post Without Image">23</a>
											</td>
											<td>
												<span>24</span>
											</td>
											<td>
												<span>25</span>
											</td>
											<td>
												<a href="#" title="Status">26</a>
											</td>
											<td>
												<span>27</span>
											</td>
										</tr>
										<tr>
											<td>
												<span>28</span>
											</td>
											<td class="today">
												<span>29</span>
											</td>
											<td>
												<span>30</span>
											</td>
											<td class="pad" colspan="4">&nbsp;</td>
										</tr>
									</tbody>

									<tfoot>
										<tr>
											<th colspan="4" class="prevMonth">
												<div class="left">
													<a href="#" data-type="post" data-year="2014" data-month="7" title="View posts for July 2015">Jul</a>
												</div>
											</th>
											<th colspan="3" class="nextMonth">
												<div class="right">
													<a href="#" data-type="post" data-year="2015" data-month="9" title="View posts for September 2015">Sep</a>
												</div>
											</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</aside>
						<aside class="widgetWrap widget widget_tag_cloud">
							<h5 class="title">Tags</h5>
							<div class="tagcloud">
								<a href='#' class='tag-link-12' title='12 topics'>Attic</a>
								<a href='#' class='tag-link-46' title='15 topics'>Basement</a>
								<a href='#' class='tag-link-11' title='11 topics'>Bedroom</a>
								<a href='#' class='tag-link-65' title='8 topics'>Driveway</a>
								<a href='#' class='tag-link-8' title='6 topics'>Garage</a>
								<a href='#' class='tag-link-10' title='9 topics'>Kitchen</a>
								<a href='#' class='tag-link-64' title='9 topics'>Living room</a>
								<a href='#' class='tag-link-9' title='23 topics'>Popular</a>
							</div>
						</aside>
						<aside class="widgetWrap widget null-instagram-feed">
							<h5 class="title">Instagram</h5>
							<ul class="instagram-pics">
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
							</ul>
							<p class="clear">
								<a href="#" rel="me" target="_blank">Follow Us</a>
							</p>
						</aside>
					</div>						
				</div>
			</div>
		</div>
	</section>
</x-app-layout>