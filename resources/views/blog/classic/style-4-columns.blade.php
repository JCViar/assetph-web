<x-app-layout>
    <x-slot name="title">
        Blog
    </x-slot>
	<section class="light_section without_sidebar">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="masonryWrap no_padding_mansory">
						<div class="masonry masonry-colums-4 isotopeNOanim" data-columns="4">
							<article class="isotopeElement">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"> </a>
											</span>
										</div>
										<img alt="New Post With Image" src="{{ asset('assets/images/blog/714x402.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">New Post With Image</a>
										</h4>
										<div class="post_format_wrap postImage">
											ThemeRex is a studio that aims to make their users’ experience easier and much more...
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">January 6, 2015</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 1" href="#comments">1</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 5" href="#">5</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"></a>
											</span>
										</div>
										<img alt="House Market Indicators" src="{{ asset('assets/images/blog/714x402.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">House Market Indicators</a>
										</h4>
										<div class="post_format_wrap postStandard">
											In attempting to identify bubbles before they burst, economists have developed a...
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 10, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 1" href="#comments">1</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 2" href="#">2</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"></a>
											</span>
										</div>
										<img alt="Post With Image" src="{{ asset('assets/images/blog/714x402.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Selling Your House?</a>
										</h4>
										<div class="post_format_wrap postStandard">
											It’s not hard to find a 50-something asking the question, “Am I too old to buy a...
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 8, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 2" href="#">2</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_gallery">
								<div class="isotopePadding bg_post">
									<div class="sc_section post_thumb thumb">
										<div id="sc_slider_3" class="sc_slider sc_slider_swiper swiper-slider-container sc_slider_controls" data-old-width="714" data-old-height="402" data-interval="9446">
											<ul class="slides swiper-wrapper">
												<li class="swiper-slide">
													<img src="{{ asset('assets/images/blog/714x402.png') }}" alt="">
												</li>
												<li class="swiper-slide">
													<img src="{{ asset('assets/images/blog/714x402.png') }}" alt="">
												</li>
												<li class="swiper-slide">
													<img src="{{ asset('assets/images/blog/714x402.png') }}" alt="">
												</li>
											</ul>
											<ul class="flex-direction-nav">
												<li>
													<a class="flex-prev" href="#">
													</a>
												</li>
												<li>
													<a class="flex-next" href="#">
													</a>
												</li>
											</ul>
										</div>
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Gallery Post Format</a>
										</h4>
										<div class="post_format_wrap postGallery">
											First of all, have your credit checked. Homebuyers to have their credit checked six...
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 8, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 2" href="#">2</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"></a>
											</span>
										</div>
										<img alt="A Mortgage In Your 50s" src="{{ asset('assets/images/blog/714x402.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">A Mortgage In Your 50s</a>
										</h4>
										<div class="post_format_wrap postStandard">
											Although you may feel ready to get out on your own, make sure your finances are in...
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 5, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 1" href="#">1</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"></a>
											</span>
										</div>
										<img alt="Steps To Buy A Home" src="{{ asset('assets/images/blog/714x402.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Steps To Buy A Home</a>
										</h4>
										<div class="post_format_wrap postStandard">
											There are a many benefits to owning your own condo – chief among them the fact...
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 4, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"></a>
											</span>
										</div>
										<img alt="Are You Ready to Rent?" src="{{ asset('assets/images/blog/714x402.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Are You Ready to Rent?</a>
										</h4>
										<div class="post_format_wrap postStandard">
											The peak homebuying season may be over, but there are still steps you can take to...
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 4, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement last">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"></a>
											</span>
										</div>
										<img alt="Is Condo Life For You?" src="{{ asset('assets/images/blog/714x402.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Is Condo Life For You?</a>
										</h4>
										<div class="post_format_wrap postStandard">
											During the 2007-2009 financial crisis, the United States economy crumbled because of...
										</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 3, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 2" href="#comments">2</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 1" href="#">1</a>
											</span>
										</div>
									</div>
								</div>
							</article>
						</div>
					</div>
					<div id="viewmore" class="squareButton pagination_viewmore">
						<a href="#" id="viewmore_link" class="theme_button view_more_button">
							<span class="icon-spin3 viewmore_loading">
							</span>
							<span class="viewmore_text_1">View more</span>
							<span class="viewmore_text_2">Loading ...</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
</x-app-layout>