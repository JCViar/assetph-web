<x-app-layout>
    <x-slot name="title">
        Blog
    </x-slot>
	<section class="light_section with_sidebar">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-12">
					<div class="masonryWrap no_padding_mansory">
						<div class="masonry masonry-colums-3 isotopeNOanim" data-columns="3">
							<article class="isotopeElement">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"> </a>
											</span>
										</div>
										<img alt="New Post With Image" src="{{ asset('assets/images/blog/714x402.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">New Post With Image</a>
										</h4>
										<div class="post_format_wrap postImage">
										ThemeRex is a studio that aims to make their users’ experience easier and much more pleasant. You probalby won’t have a better opportunity to make...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">January 6, 2015</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 1" href="#comments">1</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 5" href="#">5</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"></a>
											</span>
										</div>
										<img alt="House Market Indicators" src="{{ asset('assets/images/blog/714x402.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">House Market Indicators</a>
										</h4>
										<div class="post_format_wrap postStandard">
										In attempting to identify bubbles before they burst, economists have developed a number of financial ratios and economic indicators that can be...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 10, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 1" href="#comments">1</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 2" href="#">2</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"></a>
											</span>
										</div>
										<img alt="Post With Image" src="{{ asset('assets/images/blog/714x402.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Post With Image</a>
										</h4>
										<div class="post_format_wrap postStandard">
										ThemeRex is a studio that aims to make their users’ experience easier and much more pleasant. You probalby won’t have a better opportunity to make...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 8, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 2" href="#">2</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement post_format_gallery">
								<div class="isotopePadding bg_post">
									<div class="sc_section post_thumb thumb">
										<div id="sc_slider_3" class="sc_slider sc_slider_swiper swiper-slider-container sc_slider_controls" data-old-width="714" data-old-height="402" data-interval="9446">
											<ul class="slides swiper-wrapper">
												<li class="swiper-slide">
													<img src="{{ asset('assets/images/blog/714x402.png') }}" alt="">
												</li>
												<li class="swiper-slide">
													<img src="{{ asset('assets/images/blog/714x402.png') }}" alt="">
												</li>
												<li class="swiper-slide">
													<img src="{{ asset('assets/images/blog/714x402.png') }}" alt="">
												</li>
											</ul>
											<ul class="flex-direction-nav">
												<li>
													<a class="flex-prev" href="#">
													</a>
												</li>
												<li>
													<a class="flex-next" href="#">
													</a>
												</li>
											</ul>
										</div>
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Gallery Post Format</a>
										</h4>
										<div class="post_format_wrap postGallery">
										Don’t be afraid of multiple offers. You still have some control. There are a few things that can happen if you get into competition. The seller...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 8, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 2" href="#">2</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"></a>
											</span>
										</div>
										<img alt="A Mortgage In Your 50s" src="{{ asset('assets/images/blog/714x402.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">A Mortgage In Your 50s</a>
										</h4>
										<div class="post_format_wrap postStandard">
										It’s not hard to find a 50-something asking the question, “Am I too old to buy a home?” The answer is, absolutely not. A couple of decades ago...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 5, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 1" href="#">1</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"></a>
											</span>
										</div>
										<img alt="Steps To Buy A Home" src="{{ asset('assets/images/blog/714x402.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Steps To Buy A Home</a>
										</h4>
										<div class="post_format_wrap postStandard">
										First of all, have your credit checked. Homebuyers to have their credit checked six months before buying a place to make sure their FICO score is...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 4, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"></a>
											</span>
										</div>
										<img alt="Are You Ready to Rent?" src="{{ asset('assets/images/blog/714x402.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Are You Ready to Rent?</a>
										</h4>
										<div class="post_format_wrap postStandard">
										Although you may feel ready to get out on your own, make sure your finances are in order before you take the leap. Take a look at some rental...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 4, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 0" href="#">0</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"></a>
											</span>
										</div>
										<img alt="Post With Image" src="{{ asset('assets/images/blog/714x402.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Post With Image</a>
										</h4>
										<div class="post_format_wrap postStandard">
										Although you may feel ready to get out on your own, make sure your finances are in order before you take the leap. Take a look at some rental...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 8, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 2" href="#">2</a>
											</span>
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement last">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#"></a>
											</span>
										</div>
										<img alt="Is Condo Life For You?" src="{{ asset('assets/images/blog/714x402.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Is Condo Life For You?</a>
										</h4>
										<div class="post_format_wrap postStandard">
										There are a many benefits to owning your own condo &#8211; chief among them the fact that you actually own your own condo! However, along with the...</div>
										<div class="bog_post_info infoPost">
											<span class="datePost">
												<a href="#" class="post_date">December 3, 2014</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 2" href="#comments">2</a>
											</span>
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 1" href="#">1</a>
											</span>
										</div>
									</div>
								</div>
							</article>
						</div>
					</div>
					<div id="viewmore" class="squareButton pagination_viewmore">
						<a href="#" id="viewmore_link" class="theme_button view_more_button">
							<span class="icon-spin3 viewmore_loading">
							</span>
							<span class="viewmore_text_1">View more</span>
							<span class="viewmore_text_2">Loading ...</span>
						</a>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div id="sidebar_main" class="widget_area sidebar_main sidebar sidebarStyleDark" role="complementary">
						<aside class=" widgetWrap widget widget_categories">
							<h5 class="title">Categories</h5>
							<ul>
								<li class="cat-item current-cat-parent dropMenu">
									<a href="blog-classic-style-1-columns.html">Classic Style</a> (14)
									<ul class="children">
										<li class="cat-item">
											<a href="blog-classic-style-1-columns.html">1 Column</a> (9)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-2-columns.html">2 Columns</a> (11)
										</li>
										<li class="cat-item current-cat">
											<a href="blog-classic-style-2-columns-sidebar.html">2 Columns + sidebar</a> (11)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-3-columns.html">3 Columns</a> (10)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-3-columns-sidebar.html">3 Columns + sidebar</a> (11)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-4-columns.html">4 Columns</a> (10)
										</li>
									</ul>
								</li>
								<li class="cat-item">
									<a href="blog-classic-style-large.html">Classic Style Large</a> (13)
								</li>
								<li class="cat-item">
									<a href="blog-classic-style-small.html">Classic Style Small</a> (12)
								</li>
								<li class="cat-item">
									<a href="#">Contemporary</a> (15)
								</li>
								<li class="cat-item">
									<a href="#">Cottage</a> (5)
								</li>
								<li class="cat-item">
									<a href="gallery-grid-alternative.html">Grid Alternative</a> (22)
								</li>
								<li class="cat-item dropMenu">
									<a href="blog-masonry-style-2-columns.html">Masonry demo</a> (18)
									<ul class="children">
										<li class="cat-item">
											<a href="blog-masonry-style-2-columns.html">2 Columns</a> (17)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-2-columns-sidebar.html">2 Columns + sidebar</a> (17)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-3-columns.html">3 Columns</a> (17)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-3-columns-sidebar.html">3 Columns + sidebar</a> (13)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-4-columns.html">4 Columns</a> (16)
										</li>
									</ul>
								</li>
								<li class="cat-item dropMenu">
									<a href="gallery-classic-style-1-column.html">Portfolio Classic</a> (22)
									<ul class="children">
										<li class="cat-item">
											<a href="gallery-classic-style-1-column.html">1 Column</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-1-column-sidebar.html">1 Column + sidebar</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-2-columns.html">2 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-2-columns-sidebar.html">2 Columns + sidebar</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-3-columns.html">3 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-3-columns-sidebar.html">3 Columns + sidebar</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-4-columns.html">4 Columns</a> (22)
										</li>
									</ul>
								</li>
								<li class="cat-item dropMenu">
									<a href="gallery-grid-style-2-columns.html">Portfolio Grid</a> (22)
									<ul class="children">
										<li class="cat-item">
											<a href="gallery-grid-style-2-columns.html">2 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-2-columns-fullscreen.html">2 Columns fullscreen</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-3-columns.html">3 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-3-columns-fullscreen.html">3 Columns fullscreen</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-4-columns.html">4 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-4-columns-fullscreen.html">4 Columns fullscreen</a> (22)
										</li>
									</ul>
								</li>
								<li class="cat-item dropMenu">
									<a href="gallery-masonry-style-2-columns.html">Portfolio Masonry</a> (12)
									<ul class="children">
										<li class="cat-item">
											<a href="gallery-masonry-style-2-columns.html">2 Columns</a> (11)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-2-columns-sidebar.html">2 Columns + sidebar</a> (12)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-3-columns.html">3 Columns</a> (12)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-3-columns-sidebar.html">3 Columns + sidebar</a> (12)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-4-columns.html">4 Columns</a> (12)
										</li>
									</ul>
								</li>
								<li class="cat-item">
									<a href="gallery-grid-alternative.html">Projects</a> (9)
								</li>
							</ul>
						</aside>
						<aside class="widgetWrap widget widget_recent_comments">
							<h5 class="title">Recent Comments</h5>
							<ul>
								<li class="recentcomments">
									<span class="comment-author-link">Admin</span> 
									on 
									<a href="#">Is Condo Life For You?</a>
								</li>
								<li class="recentcomments">
									<span class="comment-author-link">Admin</span> 
									on 
									<a href="#">New Post With Image</a>
								</li>
								<li class="recentcomments">
									<span class="comment-author-link">Admin</span> 
									on 
									<a href="#">House Market Indicators</a>
								</li>
							</ul>
						</aside>
						<aside class="widgetWrap widget widget_archive">
							<h5 class="title">Archives</h5>
							<ul>
								<li>
									<a href='#'>January 2015</a>&nbsp;(3)
								</li>
								<li>
									<a href='#'>December 2014</a>&nbsp;(59)
								</li>
								<li>
									<a href='#'>November 2014</a>&nbsp;(3)
								</li>
								<li>
									<a href='#'>September 2014</a>&nbsp;(1)
								</li>
								<li>
									<a href='#'>August 2014</a>&nbsp;(1)
								</li>
								<li>
									<a href='#'>July 2014</a>&nbsp;(2)
								</li>
								<li>
									<a href='#'>May 2014</a>&nbsp;(5)
								</li>
								<li>
									<a href='#'>February 2014</a>&nbsp;(1)
								</li>
							</ul>
						</aside>
						<aside class="widgetWrap widget widget_search">
							<form role="search" method="get" class="search-form" action="index.html">
								<input type="text" class="search-field" placeholder="Search" value="" name="s" title="Search for:" />
								<span class="search-button light ico">
									<a class="search-field icon-search" href="#"></a>
								</span>
							</form>
						</aside>
						<aside class="widgetWrap widget widget_calendar">
							<div id="calendar_wrap">
								<table class="calendar">
									<thead>
										<tr>
											<th class="curMonth" colspan="7">
												<a href="#" title="View posts for August 2015">August</a>
											</th>
										</tr>
										<tr>
											<th scope="col" title="Monday">Mon</th>
											<th scope="col" title="Tuesday">Tue</th>
											<th scope="col" title="Wednesday">Wed</th>
											<th scope="col" title="Thursday">Thu</th>
											<th scope="col" title="Friday">Fri</th>
											<th scope="col" title="Saturday">Sat</th>
											<th scope="col" title="Sunday">Sun</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td colspan="1" class="pad">&nbsp;</td>
											<td>
												<span>1</span>
											</td>
											<td>
												<span>2</span>
											</td>
											<td>
												<a href="#" title="Post Formats – Chat">3</a>
											</td>
											<td>
												<span>4</span>
											</td>
											<td>
												<a href="#" title="Vimeo Video Post">5</a>
											</td>
											<td>
												<span>6</span>
											</td>
										</tr>
										<tr>
											<td>
												<span>7</span>
											</td>
											<td>
												<a href="#" title="What Is Housing Сooperative">8</a>
											</td>
											<td>
												<span>9</span>
											</td>
											<td>
												<a href="#" title="Post format – Aside">10</a>
											</td>
											<td>
												<span>11</span>
											</td>
											<td>
												<span>12</span>
											</td>
											<td>
												<span>13</span>
											</td>
										</tr>
										<tr>
											<td>
												<a href="#" title="Post formats – Link">14</a>
											</td>
											<td>
												<a href="#" title="Audio Post">15</a>
											</td>
											<td>
												<span>16</span>
											</td>
											<td>
												<span>17</span>
											</td>
											<td>
												<a href="#" title="Gallery Post Format">18</a>
											</td>
											<td>
												<a href="#" title="Post With Image">19</a>
											</td>
											<td>
												<span>20</span>
											</td>
										</tr>
										<tr>
											<td>
												<span>21</span>
											</td>
											<td>
												<span>22</span>
											</td>
											<td>
												<a href="#" title="Post Without Image">23</a>
											</td>
											<td>
												<span>24</span>
											</td>
											<td>
												<span>25</span>
											</td>
											<td>
												<a href="#" title="Status">26</a>
											</td>
											<td>
												<span>27</span>
											</td>
										</tr>
										<tr>
											<td>
												<span>28</span>
											</td>
											<td class="today">
												<span>29</span>
											</td>
											<td>
												<span>30</span>
											</td>
											<td class="pad" colspan="4">&nbsp;</td>
										</tr>
									</tbody>

									<tfoot>
										<tr>
											<th colspan="4" class="prevMonth">
												<div class="left">
													<a href="#" data-type="post" data-year="2014" data-month="7" title="View posts for July 2015">Jul</a>
												</div>
											</th>
											<th colspan="3" class="nextMonth">
												<div class="right">
													<a href="#" data-type="post" data-year="2015" data-month="9" title="View posts for September 2015">Sep</a>
												</div>
											</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</aside>
						<aside class="widgetWrap widget widget_tag_cloud">
							<h5 class="title">Tags</h5>
							<div class="tagcloud">
								<a href='#' class='tag-link-12' title='12 topics'>Attic</a>
								<a href='#' class='tag-link-46' title='15 topics'>Basement</a>
								<a href='#' class='tag-link-11' title='11 topics'>Bedroom</a>
								<a href='#' class='tag-link-65' title='8 topics'>Driveway</a>
								<a href='#' class='tag-link-8' title='6 topics'>Garage</a>
								<a href='#' class='tag-link-10' title='9 topics'>Kitchen</a>
								<a href='#' class='tag-link-64' title='9 topics'>Living room</a>
								<a href='#' class='tag-link-9' title='23 topics'>Popular</a>
							</div>
						</aside>
						<aside class="widgetWrap widget null-instagram-feed">
							<h5 class="title">Instagram</h5>
							<ul class="instagram-pics">
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.') }}png"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.') }}png"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.') }}png"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.') }}png"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.') }}png"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.') }}png"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
							</ul>
							<p class="clear">
								<a href="#" rel="me" target="_blank">Follow Us</a>
							</p>
						</aside>
					</div>						
				</div>
			</div>
		</div>
	</section>
</x-app-layout>