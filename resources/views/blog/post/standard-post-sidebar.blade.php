<x-app-layout>
    <x-slot name="title">
        Blog
    </x-slot>
	<section class="light_section with_sidebar">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-12">
					<div class="postLeft hrShadow">
						<article class="post_content">
							<div class="sc_section post_thumb thumb">
								<img alt="House Market Indicators" src="{{ asset('assets/images/blog/1170x778.png') }}">
							</div>
							<h1 class="post_title entry-title">House Market Indicators</h1>
							<div class="post_info infoPost">
								<span class="authorPost">
									<a href="#" class="post_author fn" rel="author">Admin</a>
								</span>
								<span class="datePost">
									<a href="#" class="post_date">December 10, 2014</a>
								</span>
								<span class="likePost icon-heart-4">2</span>
								<span class="commentPost">
									<a class="icon-comment-3" title="Comments - 1" href="#comments">1</a>
								</span>
							</div>
							<div class="post_text_area">
								<p>In attempting to identify bubbles before they burst, economists have developed a number of financial ratios and economic indicators that can be used to evaluate whether homes in a given area are fairly valued. By comparing current levels to previous levels that have proven unsustainable in the past (i.e. led to or at least accompanied crashes), one can make an educated guess as to whether a given real estate market is experiencing a bubble. Indicators describe two interwoven aspects of housing bubble: a valuation component and a debt (or leverage) component. The valuation component measures how expensive houses are relative to what most people can afford, and the debt component measures how indebted households become in buying them for home or profit (and also how much exposure the banks accumulate by lending for them).</p>
								<div class="tagsWrap">
									<span class="post_cats">
										Categories: 
										<a class="cat_link" href="#">1 Column,</a>
										<a class="cat_link" href="#">2 Columns,</a>
										<a class="cat_link" href="#">2 Columns + sidebar,</a>
										<a class="cat_link" href="#">3 Columns,</a>
										<a class="cat_link" href="#">3 Columns + sidebar,</a>
										<a class="cat_link" href="#">4 Columns,</a>
										<a class="cat_link" href="#">Classic Style</a>
									</span>
									<div class="postSharing">
										<ul>
											<li class="likeButton like" data-postid="642" data-likes="2" data-title-like="Like" data-title-dislike="Dislike">
												<a class="icon-heart" title="Like - 2" href="#">
													<span class="likePost">Like</span>
												</a>
											</li>
											<li class="share">
												<a class="icon-share shareDrop" title="Share" href="#">Share</a>
												<ul class="share-social shareDrop">
													<li>
														<a href="#" class="icon-twitter social_icons share-item" onclick="window.open('https://twitter.com/', '_blank', 'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=480, height=400, toolbar=0, status=0'); return false;">Twitter</a>
													</li>
													<li>
														<a href="#" class="icon-facebook social_icons share-item" onclick="window.open('http://www.facebook.com', '_blank', 'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=480, height=400, toolbar=0, status=0'); return false;">Facebook</a>
													</li>
													<li>
														<a href="#" class="icon-gplus social_icons share-item" onclick="window.open('https://plus.google.com', '_blank', 'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=480, height=400, toolbar=0, status=0'); return false;">Gplus</a>
													</li>
													<li>
														<a href="#" class="icon-pinterest social_icons share-item" onclick="window.open('http://pinterest.com', '_blank', 'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=480, height=400, toolbar=0, status=0'); return false;">Pinterest</a>
													</li>
												</ul>
											</li>
										</ul>
									</div>
									<div class="post_tags">
										Tags: <a class="tag_link" href="#">Living room</a>
									</div>
								</div>
							</div>
						</article>
					</div>
					<div class="author vcard hrShadow">
						<div class="wrap">
							<div class="avatar">
								<a href="#">
									<img alt='' src='images/team/70x70.png' srcset='images/team/70x70.png' class='avatar avatar-70 photo' />
								</a>
							</div>
							<div class="authorInfo">
								<h5 class="post_author_title">
								<span>
									<a href="#" class="fn">Admin</a>
								</span>
								</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
							</div>
							<div class="socPage">
								<ul>
									<li>
										<a href="https://www.facebook.com/themerex" class="social_icons social_facebook icon-facebook" target="_blank">
										</a>
									</li>
									<li>
										<a href="https://twitter.com/Theme_REX" class="social_icons social_twitter icon-twitter" target="_blank">
										</a>
									</li>
									<li>
										<a href="https://plus.google.com/102189073109602153696" class="social_icons social_gplus icon-gplus" target="_blank">
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="relatedWrap hrShadow">
						<h3>Recommended Posts</h3>
						<div class="relatedPostWrap margin_bottom_big">
							<div class="columnsWrap  no_indent_style columnsFloat">
								<article class="col-sm-4 no_margin_top">
									<div class="wrap thumb">
										<div class="thumb">
											<img alt="New Post With Image" src="{{ asset('assets/images/blog/400x280.png') }}">
										</div>
										<div class="relatedInfo">
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 5" href="#">5</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 1" href="#comments">1</a>
											</span>
											<h5>
											<a href="#">New Post With Image</a>
											</h5>
										</div>
									</div>
								</article>
								<article class="col-sm-4 no_margin_top">
									<div class="wrap thumb">
										<div class="thumb">
											<img alt="Post With Image" src="{{ asset('assets/images/blog/400x280.png') }}">
										</div>
										<div class="relatedInfo">
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 2" href="#">2</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<h5>
											<a href="#">Post With Image</a>
											</h5>
										</div>
									</div>
								</article>
								<article class="col-sm-4 no_margin_top">
									<div class="wrap thumb">
										<div class="thumb">
											<img alt="Post With Image" src="{{ asset('assets/images/blog/400x280.png') }}">
										</div>
										<div class="relatedInfo">
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 2" href="#">2</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<h5>
											<a href="#">Gallery Post Format</a>
											</h5>
										</div>
									</div>
								</article>
							</div>
						</div>
					</div>
					<div id="comments" class="comments hrShadow">
						<h3 class="comments_title">1 Comment</h3>
						<ul class="comments_list commBody">
							<li id="comment-26" class="comment byuser comment-author-admin bypostauthor even thread-even depth-1 commItem">
								<div class="comment_author_avatar avatar">
									<img alt='' src='images/team/70x70.png' srcset='images/team/70x70.png' class='avatar avatar-70 photo' />
								</div>
								<div class="wrap_comment">
									<h5 class="comment_title">
									<a href="#">Admin</a>
									</h5>
									<div class="posted">
										<span class="comment_date">March 31, 2015</span>
									</div>
									<div class="authorInfo">
										<div class="comment_content">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
										</div>
									</div>
									<div class="replyWrap">
										<a rel="nofollow" class="comment-reply-login" href="#">Log in to Reply</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
					<div class="formValid">
						<h3>Leave a Reply</h3>
						<div class="commForm commentsForm">
							<div id="respond" class="comment-respond">
								<h3 id="reply-title" class="comment-reply-title">
								<small>
								<a rel="nofollow" id="cancel-comment-reply-link" href="#respond" style="display:none;">Cancel reply</a>
								</small>
								</h3>
								<p class="must-log-in">You must be <a href="#">logged in</a> to post a comment.</p>
							</div>
							<div class="nav_comments"></div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div id="sidebar_main" class="widget_area sidebar_main sidebar sidebarStyleDark" role="complementary">
						<aside class=" widgetWrap widget widget_categories">
							<h5 class="title">Categories</h5>
							<ul>
								<li class="cat-item current-cat-parent dropMenu">
									<a href="blog-classic-style-1-columns.html">Classic Style</a> (14)
									<ul class="children">
										<li class="cat-item">
											<a href="blog-classic-style-1-columns.html">1 Column</a> (9)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-2-columns.html">2 Columns</a> (11)
										</li>
										<li class="cat-item current-cat">
											<a href="blog-classic-style-2-columns-sidebar.html">2 Columns + sidebar</a> (11)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-3-columns.html">3 Columns</a> (10)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-3-columns-sidebar.html">3 Columns + sidebar</a> (11)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-4-columns.html">4 Columns</a> (10)
										</li>
									</ul>
								</li>
								<li class="cat-item">
									<a href="blog-classic-style-large.html">Classic Style Large</a> (13)
								</li>
								<li class="cat-item">
									<a href="blog-classic-style-small.html">Classic Style Small</a> (12)
								</li>
								<li class="cat-item">
									<a href="#">Contemporary</a> (15)
								</li>
								<li class="cat-item">
									<a href="#">Cottage</a> (5)
								</li>
								<li class="cat-item">
									<a href="gallery-grid-alternative.html">Grid Alternative</a> (22)
								</li>
								<li class="cat-item dropMenu">
									<a href="blog-masonry-style-2-columns.html">Masonry demo</a> (18)
									<ul class="children">
										<li class="cat-item">
											<a href="blog-masonry-style-2-columns.html">2 Columns</a> (17)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-2-columns-sidebar.html">2 Columns + sidebar</a> (17)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-3-columns.html">3 Columns</a> (17)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-3-columns-sidebar.html">3 Columns + sidebar</a> (13)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-4-columns.html">4 Columns</a> (16)
										</li>
									</ul>
								</li>
								<li class="cat-item dropMenu">
									<a href="gallery-classic-style-1-column.html">Portfolio Classic</a> (22)
									<ul class="children">
										<li class="cat-item">
											<a href="gallery-classic-style-1-column.html">1 Column</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-1-column-sidebar.html">1 Column + sidebar</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-2-columns.html">2 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-2-columns-sidebar.html">2 Columns + sidebar</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-3-columns.html">3 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-3-columns-sidebar.html">3 Columns + sidebar</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-4-columns.html">4 Columns</a> (22)
										</li>
									</ul>
								</li>
								<li class="cat-item dropMenu">
									<a href="gallery-grid-style-2-columns.html">Portfolio Grid</a> (22)
									<ul class="children">
										<li class="cat-item">
											<a href="gallery-grid-style-2-columns.html">2 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-2-columns-fullscreen.html">2 Columns fullscreen</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-3-columns.html">3 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-3-columns-fullscreen.html">3 Columns fullscreen</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-4-columns.html">4 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-4-columns-fullscreen.html">4 Columns fullscreen</a> (22)
										</li>
									</ul>
								</li>
								<li class="cat-item dropMenu">
									<a href="gallery-masonry-style-2-columns.html">Portfolio Masonry</a> (12)
									<ul class="children">
										<li class="cat-item">
											<a href="gallery-masonry-style-2-columns.html">2 Columns</a> (11)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-2-columns-sidebar.html">2 Columns + sidebar</a> (12)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-3-columns.html">3 Columns</a> (12)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-3-columns-sidebar.html">3 Columns + sidebar</a> (12)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-4-columns.html">4 Columns</a> (12)
										</li>
									</ul>
								</li>
								<li class="cat-item">
									<a href="gallery-grid-alternative.html">Projects</a> (9)
								</li>
							</ul>
						</aside>
						<aside class="widgetWrap widget widget_recent_comments">
							<h5 class="title">Recent Comments</h5>
							<ul>
								<li class="recentcomments">
									<span class="comment-author-link">Admin</span> 
									on 
									<a href="#">Is Condo Life For You?</a>
								</li>
								<li class="recentcomments">
									<span class="comment-author-link">Admin</span> 
									on 
									<a href="#">New Post With Image</a>
								</li>
								<li class="recentcomments">
									<span class="comment-author-link">Admin</span> 
									on 
									<a href="#">House Market Indicators</a>
								</li>
							</ul>
						</aside>
						<aside class="widgetWrap widget widget_archive">
							<h5 class="title">Archives</h5>
							<ul>
								<li>
									<a href='#'>January 2015</a>&nbsp;(3)
								</li>
								<li>
									<a href='#'>December 2014</a>&nbsp;(59)
								</li>
								<li>
									<a href='#'>November 2014</a>&nbsp;(3)
								</li>
								<li>
									<a href='#'>September 2014</a>&nbsp;(1)
								</li>
								<li>
									<a href='#'>August 2014</a>&nbsp;(1)
								</li>
								<li>
									<a href='#'>July 2014</a>&nbsp;(2)
								</li>
								<li>
									<a href='#'>May 2014</a>&nbsp;(5)
								</li>
								<li>
									<a href='#'>February 2014</a>&nbsp;(1)
								</li>
							</ul>
						</aside>
						<aside class="widgetWrap widget widget_search">
							<form role="search" method="get" class="search-form" action="index.html">
								<input type="text" class="search-field" placeholder="Search" value="" name="s" title="Search for:" />
								<span class="search-button light ico">
									<a class="search-field icon-search" href="#"></a>
								</span>
							</form>
						</aside>
						<aside class="widgetWrap widget widget_calendar">
							<div id="calendar_wrap">
								<table class="calendar">
									<thead>
										<tr>
											<th class="curMonth" colspan="7">
												<a href="#" title="View posts for August 2015">August</a>
											</th>
										</tr>
										<tr>
											<th scope="col" title="Monday">Mon</th>
											<th scope="col" title="Tuesday">Tue</th>
											<th scope="col" title="Wednesday">Wed</th>
											<th scope="col" title="Thursday">Thu</th>
											<th scope="col" title="Friday">Fri</th>
											<th scope="col" title="Saturday">Sat</th>
											<th scope="col" title="Sunday">Sun</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td colspan="1" class="pad">&nbsp;</td>
											<td>
												<span>1</span>
											</td>
											<td>
												<span>2</span>
											</td>
											<td>
												<a href="#" title="Post Formats – Chat">3</a>
											</td>
											<td>
												<span>4</span>
											</td>
											<td>
												<a href="#" title="Vimeo Video Post">5</a>
											</td>
											<td>
												<span>6</span>
											</td>
										</tr>
										<tr>
											<td>
												<span>7</span>
											</td>
											<td>
												<a href="#" title="What Is Housing Сooperative">8</a>
											</td>
											<td>
												<span>9</span>
											</td>
											<td>
												<a href="#" title="Post format – Aside">10</a>
											</td>
											<td>
												<span>11</span>
											</td>
											<td>
												<span>12</span>
											</td>
											<td>
												<span>13</span>
											</td>
										</tr>
										<tr>
											<td>
												<a href="#" title="Post formats – Link">14</a>
											</td>
											<td>
												<a href="#" title="Audio Post">15</a>
											</td>
											<td>
												<span>16</span>
											</td>
											<td>
												<span>17</span>
											</td>
											<td>
												<a href="#" title="Gallery Post Format">18</a>
											</td>
											<td>
												<a href="#" title="Post With Image">19</a>
											</td>
											<td>
												<span>20</span>
											</td>
										</tr>
										<tr>
											<td>
												<span>21</span>
											</td>
											<td>
												<span>22</span>
											</td>
											<td>
												<a href="#" title="Post Without Image">23</a>
											</td>
											<td>
												<span>24</span>
											</td>
											<td>
												<span>25</span>
											</td>
											<td>
												<a href="#" title="Status">26</a>
											</td>
											<td>
												<span>27</span>
											</td>
										</tr>
										<tr>
											<td>
												<span>28</span>
											</td>
											<td class="today">
												<span>29</span>
											</td>
											<td>
												<span>30</span>
											</td>
											<td class="pad" colspan="4">&nbsp;</td>
										</tr>
									</tbody>

									<tfoot>
										<tr>
											<th colspan="4" class="prevMonth">
												<div class="left">
													<a href="#" data-type="post" data-year="2014" data-month="7" title="View posts for July 2015">Jul</a>
												</div>
											</th>
											<th colspan="3" class="nextMonth">
												<div class="right">
													<a href="#" data-type="post" data-year="2015" data-month="9" title="View posts for September 2015">Sep</a>
												</div>
											</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</aside>
						<aside class="widgetWrap widget widget_tag_cloud">
							<h5 class="title">Tags</h5>
							<div class="tagcloud">
								<a href='#' class='tag-link-12' title='12 topics'>Attic</a>
								<a href='#' class='tag-link-46' title='15 topics'>Basement</a>
								<a href='#' class='tag-link-11' title='11 topics'>Bedroom</a>
								<a href='#' class='tag-link-65' title='8 topics'>Driveway</a>
								<a href='#' class='tag-link-8' title='6 topics'>Garage</a>
								<a href='#' class='tag-link-10' title='9 topics'>Kitchen</a>
								<a href='#' class='tag-link-64' title='9 topics'>Living room</a>
								<a href='#' class='tag-link-9' title='23 topics'>Popular</a>
							</div>
						</aside>
						<aside class="widgetWrap widget null-instagram-feed">
							<h5 class="title">Instagram</h5>
							<ul class="instagram-pics">
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
							</ul>
							<p class="clear">
								<a href="#" rel="me" target="_blank">Follow Us</a>
							</p>
						</aside>
					</div>						
				</div>
			</div>
		</div>
	</section>
</x-app-layout>