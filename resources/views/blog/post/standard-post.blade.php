<x-app-layout>
    <x-slot name="title">
        Blog
    </x-slot>
	<section class="light_section without_sidebar">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="post postLeft hrShadow">
						<article class="post_content">
							<div class="sc_section post_thumb thumb">
								<img alt="House Market Indicators" src="{{ asset('assets/images/blog/1170x778.png') }}">
							</div>
							<h1 class="post_title entry-title">House Market Indicators</h1>
							<div class="post_info infoPost">
								<span class="authorPost">
									<a href="#" class="post_author fn" rel="author">Admin</a>
								</span>
								<span class="datePost">
									<a href="#" class="post_date">December 10, 2014</a>
								</span>
								<span class="likePost icon-heart-4">2</span>
								<span class="commentPost">
									<a class="icon-comment-3" title="Comments - 1" href="#comments">1</a>
								</span>
							</div>
							<div class="post_text_area">
								<p>ThemeRex is a studio that aims to make their users’ experience easier and much more pleasant. You probalby won’t have a better opportunity to make sure of their competence, as well as friendliness towards their customers. Vast experience and understanding of each product’s peculiarities let ThemeRex create outstanding business solutions. </p>
								<blockquote class="sc_quote  margin_bottom_mini sc_quote_style_1">
									<p>Always bear in mind that your own resolution to succeed is more important than any other.</p>
								</blockquote> 
								<p>ThemeRex is doing their best to grant each owner of their themes with maximum opportunities to present their individuality, show their achievements, and establish the best contact with their audience. Using their rich experience, world’s innovations and their own web solutions the ThemeRex team creates templates that will solve your business’s tasks at maximum degree.</p>
								<h4>High Professionalism</h4>
								<p>Many year experience and hundreds of successful projects have made ThemeRex a professional on the web area. Within this period of time, the company has gone through a serious evolution from amateurs to leaders. We create templates for your websites that will increase your reputation and highlight your professionalism and leadership. </p>
								<div class="sc_section sc_alignleft col-sm-6">
									<figure class="sc_image  sc_image_shape_square">
										<img src="{{ asset('assets/images/blog/1900x706.png') }}" alt="" />
										<figcaption>
										<span>Title of Image</span>
										</figcaption>
									</figure>
								</div> 
								<p>Purchasing products from ThemeRex means entrusting your reputation to one of the best web-studios. ThemeRex company does its work with its customer in mind. That is why you are getting the best product that will perfectly suit your needs.</p>
								<p>Not only does ThemeRex create bright, eye-catching designs and do premium quality code, they provide great support to their customers. ThemeRex technical support is always there to assist their users with all their technical questions regarding ThemeRex products. We speak clear language understood by everyone. You need our assistance &#8211; we happily help you. Use comments to ask pre-sale questions, and Ticket System to get any help with our products. Our support team is always happy to help our customers. Whether it is assistance with installation or just configuration, we are here to help.</p>
								<div class="tagsWrap">
									<span class="post_cats">
										Categories: 
										<a class="cat_link" href="#">1 Column,</a>
										<a class="cat_link" href="#">2 Columns,</a>
										<a class="cat_link" href="#">2 Columns + sidebar,</a>
										<a class="cat_link" href="#">3 Columns,</a>
										<a class="cat_link" href="#">3 Columns + sidebar,</a>
										<a class="cat_link" href="#">4 Columns,</a>
										<a class="cat_link" href="#">Classic Style,</a>
										<a class="cat_link" href="#">Classic Style Large,</a>
										<a class="cat_link" href="#">Classic Style Small,</a>
										<a class="cat_link" href="#">Contemporary,</a>
										<a class="cat_link" href="#">Cottage</a>
									</span>
									<div class="postSharing">
										<ul>
											<li class="likeButton like" data-postid="525" data-likes="5" data-title-like="Like" data-title-dislike="Dislike">
												<a class="icon-heart" title="Like - 5" href="#">
													<span class="likePost">Like</span>
												</a>
											</li>
											<li class="share">
												<a class="icon-share shareDrop" title="Share" href="#">Share</a>
												<ul class="share-social shareDrop">
													<li>
														<a href="#" class="icon-twitter social_icons share-item" onclick="window.open('https://twitter.com/', '_blank', 'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=480, height=400, toolbar=0, status=0'); return false;">Twitter</a>
													</li>
													<li>
														<a href="#" class="icon-facebook social_icons share-item" onclick="window.open('http://www.facebook.com', '_blank', 'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=480, height=400, toolbar=0, status=0'); return false;">Facebook</a>
													</li>
													<li>
														<a href="#" class="icon-gplus social_icons share-item" onclick="window.open('https://plus.google.com/', '_blank', 'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=480, height=400, toolbar=0, status=0'); return false;">Gplus</a>
													</li>
													<li>
														<a href="#" class="icon-pinterest social_icons share-item" onclick="window.open('http://pinterest.com/', '_blank', 'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=480, height=400, toolbar=0, status=0'); return false;">Pinterest</a>
													</li>
												</ul>
											</li>
										</ul>
									</div>
									<div class="post_tags">
										Tags: <a class="tag_link" href="#">Bedroom</a>
									</div>
								</div>
							</div>
						</article>
					</div>
					<div class="author vcard hrShadow">
						<div class="wrap">
							<div class="avatar">
								<a href="#">
									<img alt='' src='images/team/70x70.png' srcset='images/team/70x70.png' class='avatar avatar-70 photo' />
								</a>
							</div>
							<div class="authorInfo">
								<h5 class="post_author_title">
								<span>
									<a href="#" class="fn">Admin</a>
								</span>
								</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
							</div>
							<div class="socPage">
								<ul>
									<li>
										<a href="https://www.facebook.com/themerex" class="social_icons social_facebook icon-facebook" target="_blank">
										</a>
									</li>
									<li>
										<a href="https://twitter.com/Theme_REX" class="social_icons social_twitter icon-twitter" target="_blank">
										</a>
									</li>
									<li>
										<a href="https://plus.google.com/102189073109602153696" class="social_icons social_gplus icon-gplus" target="_blank">
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="relatedWrap hrShadow">
						<h3>Recommended Posts</h3>
						<div class="relatedPostWrap margin_bottom_big">
							<div class="columnsWrap  no_indent_style columnsFloat">
								<article class="col-sm-4 column_item_1">
									<div class="wrap thumb">
										<div class="thumb">
											<img alt="New Post With Image" src="{{ asset('assets/images/blog/400x280.png') }}">
										</div>
										<div class="relatedInfo">
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 5" href="#">5</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 1" href="#comments">1</a>
											</span>
											<h5>
											<a href="#">New Post With Image</a>
											</h5>
										</div>
									</div>
								</article>
								<article class="col-sm-4 column_item_2">
									<div class="wrap thumb">
										<div class="thumb">
											<img alt="Post With Image" src="{{ asset('assets/images/blog/400x280.png') }}">
										</div>
										<div class="relatedInfo">
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 2" href="#">2</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<h5>
											<a href="#">Post With Image</a>
											</h5>
										</div>
									</div>
								</article>
								<article class="col-sm-4 column_item_2">
									<div class="wrap thumb">
										<div class="thumb">
											<img alt="Post With Image" src="{{ asset('assets/images/blog/400x280.png') }}">
										</div>
										<div class="relatedInfo">
											<span class="likePost">
												<a class="icon-heart-4" title="Likes - 2" href="#">2</a>
											</span>
											<span class="commentPost">
												<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
											</span>
											<h5>
											<a href="#">Gallery Post Format</a>
											</h5>
										</div>
									</div>
								</article>
							</div>
						</div>
					</div>
					<div id="comments" class="comments hrShadow">
						<h3 class="comments_title">1 Comment</h3>
						<ul class="comments_list commBody">
							<li id="comment-26" class="comment byuser comment-author-admin bypostauthor even thread-even depth-1 commItem">
								<div class="comment_author_avatar avatar">
									<img alt='' src='images/team/70x70.png' srcset='images/team/70x70.png' class='avatar avatar-70 photo' />
								</div>
								<div class="wrap_comment">
									<h5 class="comment_title">
									<a href="#">Admin</a>
									</h5>
									<div class="posted">
										<span class="comment_date">March 31, 2015</span>
									</div>
									<div class="authorInfo">
										<div class="comment_content">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
										</div>
									</div>
									<div class="replyWrap">
										<a rel="nofollow" class="comment-reply-login" href="#">Log in to Reply</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
					<div class="formValid">
						<h3>Leave a Reply</h3>
						<div class="commForm commentsForm">
							<div id="respond" class="comment-respond">
								<h3 id="reply-title" class="comment-reply-title">
								<small>
								<a rel="nofollow" id="cancel-comment-reply-link" href="#respond" style="display:none;">Cancel reply</a>
								</small>
								</h3>
								<p class="must-log-in">You must be <a href="#">logged in</a> to post a comment.</p>
							</div>
							<div class="nav_comments"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</x-app-layout>