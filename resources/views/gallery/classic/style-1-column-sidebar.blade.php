<x-app-layout>
    <x-slot name="title">
        Gallery
    </x-slot>

	<section class="light_section with_sidebar">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-12">
					<div class="masonryWrap no_padding_mansory">
						<div class="isotopeFiltr"></div>
						<div class="masonry masonry-colums-1 isotope" data-columns="1">
							<article class="isotopeElement flt_64">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Working With A Realtor" src="{{ asset('assets/images/isotope/1170x647.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Working With A Realtor</a>
										</h4>
										<div class="post_format_wrap postStandard">
										A Realtor is a broker or agent who is a member of the Board of Realtors, an organization that follows a code of ethics beyond state license laws. It is realtors who sponsor the Multiple Listing...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_65">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="What&#8217;s FHA Home Loans" src="{{ asset('assets/images/isotope/1170x647.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">What&#8217;s FHA Home Loans</a>
										</h4>
										<div class="post_format_wrap postStandard">
										An FHA insured loan is a US Federal Housing Administration mortgage insurance backed mortgage loan which is provided by a FHA-approved lender. FHA insured loans are a type of federal assistance...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_46 flt_64">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Top 3 Mortgage Mistakes" src="{{ asset('assets/images/isotope/1170x647.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Top 3 Mortgage Mistakes</a>
										</h4>
										<div class="post_format_wrap postStandard">
										During the 2007-2009 financial crisis, the United States economy crumbled because of a problem with mortgage foreclosures. Borrowers all over the nation had trouble paying their mortgages. At the...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_11 flt_8">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Tips For Homeowners" src="{{ asset('assets/images/isotope/1170x647.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Tips For Homeowners</a>
										</h4>
										<div class="post_format_wrap postStandard">
										Homeowners&#8217; insurance isn&#8217;t a luxury, it&#8217;s a necessity. In fact, most mortgage companies won&#8217;t make a loan or finance a residential real estate transaction unless the buyer...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_65 flt_10">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Tips For Buying a House" src="{{ asset('assets/images/isotope/1170x647.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Tips For Buying a House</a>
										</h4>
										<div class="post_format_wrap postStandard">
											Buying a house can be a daunting task, even for someone who has owned several homes. Here aresome helpful hints for the first time property buyers
										</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_65">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="The Home Inspection" src="{{ asset('assets/images/isotope/1170x647.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">The Home Inspection</a>
										</h4>
										<div class="post_format_wrap postStandard">
										A home inspection is a limited, non-invasive examination of the condition of a home, often in connection with the sale of that home. Home inspections are usually conducted by a home inspector who...</div>
									</div>
								</div>
							</article>
						</div>
						<div id="viewmore" class="squareButton pagination_viewmore">
							<a href="#" id="viewmore_link" class="theme_button view_more_button">
								<span class="icon-spin3 viewmore_loading">
								</span>
								<span class="viewmore_text_1">View more</span>
								<span class="viewmore_text_2">Loading ...</span>
							</a>
						</div>
					</div>
				</div>

				<!-- Sidebar here -->
				<x-sidebar />
				
			</div>
		</div>
	</section>
</x-app-layout>