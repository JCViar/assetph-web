<x-app-layout>
    <x-slot name="title">
        Gallery
    </x-slot>
	<section class="light_section without_sidebar">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="masonryWrap no_padding_mansory">
						<div class="isotopeFiltr"></div>
						<div class="masonry masonry-colums-4 isotope" data-columns="4">
							<article class="isotopeElement flt_64">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Moving House" src="{{ asset('assets/images/isotope/350x280.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Moving House</a>
										</h4>
										<div class="post_format_wrap postStandard">
										How do you move everything you own into your new home...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_65">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Renting to Own" src="{{ asset('assets/images/isotope/350x280.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Renting to Own</a>
										</h4>
										<div class="post_format_wrap postStandard">
										Rent-to-own, also known as rental-purchase, is a type of...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_46 flt_64">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Buy or Sell First?" src="{{ asset('assets/images/isotope/350x280.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Buy or Sell First?</a>
										</h4>
										<div class="post_format_wrap postStandard">
										Most people sell their current home at the same time as...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_11 flt_8">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Cash Vs. Mortgage" src="{{ asset('assets/images/isotope/350x280.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Cash vs Mortgage</a>
										</h4>
										<div class="post_format_wrap postStandard">
										Paying cash for a home eliminates the need to pay...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_8">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="The Home Inspection" src="{{ asset('assets/images/isotope/350x280.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">First Property Search</a>
										</h4>
										<div class="post_format_wrap postStandard">
										Moving to your own place? If you’re renting for the...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_11">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="The Home Inspection" src="{{ asset('assets/images/isotope/350x280.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">The Home Inspection</a>
										</h4>
										<div class="post_format_wrap postStandard">
										A home inspection is a limited, non-invasive examination...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_46">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Buying Hints & Insights" src="{{ asset('assets/images/isotope/350x280.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Buying Hints & Insights</a>
										</h4>
										<div class="post_format_wrap postStandard">
										Buying a house can be a daunting task, even for...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_64">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Working With A Realtor" src="{{ asset('assets/images/isotope/350x280.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Working With A Realtor</a>
										</h4>
										<div class="post_format_wrap postStandard">
										A Realtor is a broker or agent who is a member of the...</div>
									</div>
								</div>
							</article>
						</div>
						<div id="viewmore" class="squareButton pagination_viewmore">
							<a href="#" id="viewmore_link" class="theme_button view_more_button">
								<span class="icon-spin3 viewmore_loading">
								</span>
								<span class="viewmore_text_1">View more</span>
								<span class="viewmore_text_2">Loading ...</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</x-app-layout>