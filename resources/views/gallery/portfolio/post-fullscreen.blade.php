<x-app-layout>
    <x-slot name="title">
        Gallery
    </x-slot>
	<section class="no_padding_container">
		<div class="container-fluid">
			<div class="row">
				<div id="mobile_tap_hover" class="itemPageFull post_format_standard">
					<div class="thumb imgNav taphover">
						<img src="{{ asset('assets/images/isotope/1900x1262.png') }}" alt="">
						<a class="itemPrev taphover" href="#">
							<span class="itInf">
								<span class="titleItem">Previous item</span>
								The Art of House Hunting
							</span>
						</a>
						<a class="itemNext taphover" href="#">
							<span class="itInf">
								<span class="titleItem">Next item</span>
								5 Steps To Buy A Home
							</span>
						</a>
					</div>
					<div class="itemDescriptionWrap">
						<div class="container">
							<a href="#" class="toggleButton"></a>
							<a href="#" class="backClose"></a>
							<h1 class="post_title entry-title">The Perfect Apartment</h1>
							<div class="post_text_area toggleDescription">
								<p>It&#8217;s so easy these days to find listings and visit apartments. But don&#8217;t let your eagerness get the better of you. Be prepared by taking a moment to determine exactly what you&#8217;re looking for in an apartment. Once you know what you want in an apartment, you&#8217;ve got to take into consideration what you can</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="light_section no_padding_container">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 margin_top_big">
					<div class="portfolBlock">
						<h5>More Information</h5>
						<ul>
							<li>
								<span>Customer:</span>
								Admin
							</li>
							<li>
								<span>Categories:</span>
								<a class="cat_link" href="#">Projects</a> 
							</li>
							<li>
								<span>Tags:</span>
								<a class="tag_link" href="#">Driveway</a> 
							</li>
							<li>
								<span>Date post:</span>
								December 22, 2014
							</li>
						</ul>
					</div>
					<div class="post_text_area hrShadow withMargin">
						<p>It&#8217;s so easy these days to find listings and visit apartments. But don&#8217;t let your eagerness get the better of you. Be prepared by taking a moment to determine exactly what you&#8217;re looking for in an apartment. Once you know what you want in an apartment, you&#8217;ve got to take into consideration what you can actually afford. This will give you the confidence to know when you&#8217;ve found the right apartment, and it will also help you avoid the disappointment that comes with finding attractive apartments that are out of your range. For many apartment hunters, the actual apartment hunt is the fun part. Armed with information about what you want and what you can afford, your job now is to go out there and find the right apartment &#8212; as quickly and as efficiently as you can. Unfortunately, some people encounter discrimination just about every time they look for apartments. Others who are used to smooth sailing are shocked to discover that they&#8217;ve become the victim of illegal discrimination. Fair housing laws protect renters and apartment hunters alike from certain types of discrimination. It&#8217;s smart to know your rights and keep them in mind as you look for your new home.</p>
						<p>Commercial mortgages are structured to meet the needs of the borrower and the lender. Key terms include the loan amount (sometimes referred to as “loan proceeds”), interest rate, term (sometimes referred to as the “maturity”), amortization schedule, and prepayment flexibility. Commercial mortgages are generally subject to extensive underwriting and due diligence prior to closing. The lender’s underwriting process may include a financial review of the property and the property owner (or “sponsor”), as well as commissioning and review of various third-party reports, such as an appraisal.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="light_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h3>Recommended Posts</h3>
					<div class="relatedPostWrap">
						<div class="columnsWrap indent_style">
							<article class="col-sm-4">
								<div class="wrap thumb">
									<div class="thumb">
										<img alt="Who Should Buy &#8216;Rent To Own&#8217;" src="{{ asset('assets/images/blog/400x280.png') }}">
									</div>
									<div class="relatedInfo">
									<h5>
										<a href="#">Who Should Buy &#8216;Rent To Own&#8217;</a>
									</h5>
									<span class="infoTags">
										<a class="tag_link" href="#">Driveway,</a>
										<a class="tag_link" href="#">Living room</a> 
									</span>
									</div>
								</div>
							</article>
							<article class="col-sm-4">
								<div class="wrap thumb">
									<div class="thumb">
										<img alt="Commercial Real Estate Loans" src="{{ asset('assets/images/blog/400x280.png') }}">
									</div>
									<div class="relatedInfo">
										<h5>
											<a href="#">Commercial Real Estate Loans</a>
										</h5>	
										<span class="infoTags">
											<a class="tag_link" href="#">Living room</a> 
										</span>
									</div>
								</div>
							</article>
							<article class="col-sm-4">
								<div class="wrap thumb">
									<div class="thumb">
										<img alt="Real Estate Traps To Avoid" src="{{ asset('assets/images/blog/400x280.png') }}">
									</div>
									<div class="relatedInfo">
										<h5>
											<a href="#">Real Estate Traps To Avoid</a>
										</h5>
										<span class="infoTags">
											<a class="tag_link" href="#">Bedroom</a>
										</span>
									</div>
								</div>
							</article>
					</div>
					</div>					
				</div>
			</div>
		</div>
	</section>
</x-app-layout>