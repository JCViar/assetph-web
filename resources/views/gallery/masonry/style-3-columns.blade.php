<x-app-layout>
    <x-slot name="title">
        Gallery
    </x-slot>
	<section class="light_section without_sidebar">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="masonryWrap no_padding_mansory">
						<div class="isotopeFiltr"></div>
						<div class="masonry masonry-colums-3 isotope" data-columns="3">
							<article class="isotopeElement flt_64">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Understanding FHA Home Loans" src="{{ asset('assets/images/isotope/714x896.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Understanding FHA Home Loans</a>
										</h4>
										<div class="post_format_wrap postStandard">
										Because of lower down payment requirements and less stringent lending standards, FHA loans are...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_65">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="To Rent Or Buy? Finance Issues" src="{{ asset('assets/images/isotope/714x265.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">To Rent Or Buy? Finance Issues</a>
										</h4>
										<div class="post_format_wrap postStandard">
										Owning your own home is a huge commitment, not just to the building you buy and the area you...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_46 flt_64">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Building Your Wish-list" src="{{ asset('assets/images/isotope/714x475.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Building Your Wish-list</a>
										</h4>
										<div class="post_format_wrap postStandard">
										There’s a difference between what you need and what you want. We all need a place to eat, sleep...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_11 flt_8">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Mortgage Rules to Follow" src="{{ asset('assets/images/isotope/714x474.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Mortgage Rules to Follow</a>
										</h4>
										<div class="post_format_wrap postStandard">
										Whether this is your first home or fourth, really understanding your mortgage and how it works...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_65 flt_10">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="The World of Real Estate Markets" src="{{ asset('assets/images/isotope/714x524.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">The World of Real Estate Markets</a>
										</h4>
										<div class="post_format_wrap postStandard">
										Real estate is &#8220;property consisting of land and the buildings on it, along with its...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_65">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="What&#8217;s a Housing Code?" src="{{ asset('assets/images/isotope/714x916.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">What&#8217;s a Housing Code?</a>
										</h4>
										<div class="post_format_wrap postStandard">
										A a housing code, or a building code, or building control, is a set of rules that specify the...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_11">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Fighting Real Estate Fraud" src="{{ asset('assets/images/isotope/714x476.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Fighting Real Estate Fraud</a>
										</h4>
										<div class="post_format_wrap postStandard">
										Fraud and investment scams abound at all levels of the real estate market – whether it be a...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_46">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="The Art of House Hunting" src="{{ asset('assets/images/isotope/714x464.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">The Art of House Hunting</a>
										</h4>
										<div class="post_format_wrap postStandard">
										There comes a time when having the freedom to up sticks and move around starts to lose its...</div>
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_46 flt_64">
								<div class="isotopePadding bg_post">
									<div class="thumb hoverIncreaseIn">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<span class="hoverLink">
												<a href="#">
												</a>
											</span>
										</div>
										<img alt="Title Insurance: Who Needs It?" src="{{ asset('assets/images/isotope/714x285.png') }}">
									</div>
									<div class="post_wrap">
										<h4>
										<a href="#">Title Insurance: Who Needs It?</a>
										</h4>
										<div class="post_format_wrap postStandard">
										Who needs title insurance? Anyone who owns a house! Title insurance is a policy of insurance...</div>
									</div>
								</div>
							</article>
						</div>
						<div id="pagination" class="pagination">
							<ul>
								<li class="pager_current active squareButton light">
									<span>1</span>
								</li>
								<li class="squareButton light">
									<a href="#">2</a>
								</li>
								<li class="pager_next squareButton light">
									<a href="#">»</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</x-app-layout>