<x-app-layout>
    <x-slot name="title">
        Gallery
    </x-slot>
	<section class="light_section no_padding_container without_sidebar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<div class="portfolioWrap NOspacing">
						<div class="isotopeFiltr"></div>
						<div class="masonry grid portfolioNOspacing isotope">
							<article data-width="1" data-height="1" class="ellNOspacing isotopeElement flt_64">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
												<a href="#">Moving House</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date" itemprop="datePublished" content="2014-12-12">
													December 12, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="Moving House" href="{{ asset('assets/images/isotope/1900x1266.png') }}">
												</a>
											</span>
										</div>
										<img alt="Moving House" src="{{ asset('assets/images/isotope/420x420.png') }}">
									</div>
								</div>
							</article>
							<article data-width="1" data-height="1" class="ellNOspacing isotopeElement flt_65">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
												<a href="#">Renting to Own</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date" itemprop="datePublished" content="2014-12-12">
													December 12, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="Renting to Own" href="{{ asset('assets/images/isotope/1900x1262.png') }}">
												</a>
											</span>
										</div>
										<img alt="Renting to Own" src="{{ asset('assets/images/isotope/420x420.png') }}">
									</div>
								</div>
							</article>
							<article data-width="2" data-height="1" class="ellNOspacing isotopeElement flt_46 flt_64">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
												<a href="#">Buy or Sell First?</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date" itemprop="datePublished" content="2014-12-12">
													December 12, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="Buy or Sell First?" href="{{ asset('assets/images/isotope/1900x1266.png') }}">
												</a>
											</span>
										</div>
										<img alt="Buy or Sell First?" src="{{ asset('assets/images/isotope/840x420.png') }}">
									</div>
								</div>
							</article>
							<article data-width="2" data-height="1" class="ellNOspacing isotopeElement flt_11 flt_8">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
												<a href="#">Cash Vs. Mortgage</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date" itemprop="datePublished" content="2014-12-12">
													December 12, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="Cash Vs. Mortgage" href="{{ asset('assets/images/isotope/1900x1262.png') }}">
												</a>
											</span>
										</div>
										<img alt="Cash Vs. Mortgage" src="{{ asset('assets/images/isotope/840x420.png') }}">
									</div>
								</div>
							</article>
							<article data-width="2" data-height="2" class="ellNOspacing isotopeElement flt_65 flt_10">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
												<a href="#">First Property Search</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date" itemprop="datePublished" content="2014-12-12">
													December 12, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="First Property Search" href="{{ asset('assets/images/isotope/1900x1262.png') }}">
												</a>
											</span>
										</div>
										<img alt="First Property Search" src="{{ asset('assets/images/isotope/840x840.png') }}">
									</div>
								</div>
							</article>
							<article data-width="1" data-height="1" class="ellNOspacing isotopeElement flt_65">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
												<a href="#">The Home Inspection</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date" itemprop="datePublished" content="2014-12-12">
													December 12, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="The Home Inspection" href="{{ asset('assets/images/isotope/1900x1262.png') }}">
												</a>
											</span>
										</div>
										<img alt="The Home Inspection" src="{{ asset('assets/images/isotope/420x420.png') }}">
									</div>
								</div>
							</article>
							<article data-width="1" data-height="1" class="ellNOspacing isotopeElement flt_11">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
												<a href="#">Buying Hints &amp; Insights</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date" itemprop="datePublished" content="2014-12-12">
													December 12, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="Buying Hints &amp; Insights" href="{{ asset('assets/images/isotope/1900x1262.png') }}">
												</a>
											</span>
										</div>
										<img alt="Buying Hints &#038; Insights" src="{{ asset('assets/images/isotope/420x420.png') }}">
									</div>
								</div>
							</article>
							<article data-width="2" data-height="1" class="ellNOspacing isotopeElement flt_8">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
												<a href="#">Working With A Realtor</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date" itemprop="datePublished" content="2014-12-12">
													December 12, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="Working With A Realtor" href="{{ asset('assets/images/isotope/1900x1262.png') }}">
												</a>
											</span>
										</div>
										<img alt="Working With A Realtor" src="{{ asset('assets/images/isotope/840x420.png') }}">
									</div>
								</div>
							</article>
							<article data-width="2" data-height="1" class="ellNOspacing isotopeElement flt_10">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
												<a href="#">Home For The 1st Time</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date" itemprop="datePublished" content="2014-12-12">
													December 12, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="Home For The 1st Time" href="{{ asset('assets/images/isotope/1900x1262.png') }}">
												</a>
											</span>
										</div>
										<img alt="Home For The 1st Time" src="{{ asset('assets/images/isotope/840x420.png') }}">
									</div>
								</div>
							</article>
							<article data-width="1" data-height="1" class="ellNOspacing isotopeElement flt_64">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
												<a href="#">Step-by-Step Buying</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date" itemprop="datePublished" content="2014-12-12">
													December 12, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="Step-by-Step Buying" href="{{ asset('assets/images/isotope/1900x1262.png') }}">
												</a>
											</span>
										</div>
										<img alt="Step-by-Step Buying" src="{{ asset('assets/images/isotope/420x420.png') }}">
									</div>
								</div>
							</article>
							<article data-width="1" data-height="1" class="ellNOspacing isotopeElement flt_8">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow">
										</span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
												<a href="#">Tips For Buying a House</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date" itemprop="datePublished" content="2014-12-12">
													December 12, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="Tips For Buying a House" href="{{ asset('assets/images/isotope/1900x1262.png') }}">
												</a>
											</span>
										</div>
										<img alt="Tips For Buying a House" src="{{ asset('assets/images/isotope/420x420.png') }}">
									</div>
								</div>
							</article>
						</div>
						<div id="viewmore" class="squareButton pagination_viewmore">
							<a href="#" id="viewmore_link" class="theme_button view_more_button">
								<span class="icon-spin3 viewmore_loading"></span>
								<span class="viewmore_text_1">View more</span>
								<span class="viewmore_text_2">Loading ...</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</x-app-layout>