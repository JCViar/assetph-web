<x-app-layout>
    <x-slot name="title">
        Gallery
    </x-slot>
	<section class="light_section without_sidebar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<div class="portfolioWrap NOspacing no_padding">
						<div class="isotopeFiltr"></div>
						<div class="masonry portfolioNOspacing masonry-colums-2 isotope" data-columns="2">
							<article class="isotopeElement flt_64">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
													<a href="#">5 Steps To Buy A Home</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date">December 22, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="5 Steps To Buy A Home" href="{{ asset('assets/images/isotope/1900x1262.png')}}"></a>
											</span>
										</div>
										<img alt="5 Steps To Buy A Home" src="{{ asset('assets/images/isotope/714x540.png')}}">
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_65">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
													<a href="#">The Perfect Apartment</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date">December 22, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="The Perfect Apartment" href="{{ asset('assets/images/isotope/1900x1262.png')}}"></a>
											</span>
										</div>
										<img alt="The Perfect Apartment" src="{{ asset('assets/images/isotope/714x540.png')}}">
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_46 flt_64">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
													<a href="#">The Art of House Hunting</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date">December 22, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="The Art of House Hunting" href="{{ asset('assets/images/isotope/1900x1262.png')}}"></a>
											</span>
										</div>
										<img alt="The Art of House Hunting" src="{{ asset('assets/images/isotope/714x540.png')}}">
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_11 flt_8">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
													<a href="#">Freehold Vs. Condo</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date">December 22, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="Freehold Vs. Condo" href="{{ asset('assets/images/isotope/1900x1262.png')}}"></a>
											</span>
										</div>
										<img alt="Freehold Vs. Condo" src="{{ asset('assets/images/isotope/714x540.png')}}">
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_65 flt_10">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
													<a href="#">To Buy or Not To Buy</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date">December 22, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="To Buy or Not To Buy" href="{{ asset('assets/images/isotope/1900x1262.png')}}"></a>
											</span>
										</div>
										<img alt="To Buy or Not To Buy" src="{{ asset('assets/images/isotope/714x540.png')}}">
									</div>
								</div>
							</article>
							<article class="isotopeElement flt_46 flt_8 flt_64">
								<div class="isotopePadding">
									<div class="thumb hoverIncreaseOut">
										<span class="hoverShadow"></span>
										<div class="wrap_hover">
											<div class="portfolioInfo">
												<h4>
													<a href="#">Selling Your House?</a>
												</h4>
												<span class="datePost">
													<a href="#" class="post_date">December 22, 2014</a>
												</span>
											</div>
											<span class="hoverIcon">
												<a title="Selling Your House?" href="{{ asset('assets/images/isotope/1900x1266.png')}}"></a>
											</span>
										</div>
										<img alt="Selling Your House?" src="{{ asset('assets/images/isotope/714x540.png')}}">
									</div>
								</div>
							</article>
						</div>
						<div id="viewmore" class="squareButton pagination_viewmore">
							<a href="#" id="viewmore_link" class="theme_button view_more_button">
								<span class="icon-spin3 viewmore_loading"></span>
								<span class="viewmore_text_1">View more</span>
								<span class="viewmore_text_2">Loading ...</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</x-app-layout>