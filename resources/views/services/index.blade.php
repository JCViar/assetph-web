<x-app-layout>
    <x-slot name="title">
        Services
    </x-slot>
    <section class="dark_section image_bg_3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="container">
                        <h2 class="sc_title_align_center margin_top_small sc_title sc_title_regular color_1">High level of expertise in real estate</h2>
                        <div class="sc_content margin_bottom_small sc_aligncenter text_styling">
                            You are welcome to check out the property’s list of main features to make sure that this home<br />
                            is in prime condition which makes it a perfect living space. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="light_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">			
                    <h2 class="sc_title_align_center sc_title sc_title_underline  color_1">Property Features</h2>
                    <div class="margin_bottom_small sc_aligncenter text_styling">
                        Looking for a home with reduced running costs and increased comfort?<br />
                        Check out the list of the features of this great property.
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 margin_bottom_small">
                    <div class="sc_title_icon sc_title_left sc_size_large icon-house sc_title_bg sc_bg_circle" style=""></div>
                    <h5 class="sc_title_align_left sc_title sc_title_iconed  color_1">Modern Family Home</h5>
                    <div class="sc_section">
                        The property features many things that will suit any family: local community, schools and daycares. 
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 margin_bottom_small">
                    <div class="sc_title_icon sc_title_left sc_size_large icon-tree-decidious sc_title_bg sc_bg_circle" style=""></div>
                    <h5 class="sc_title_align_left sc_title sc_title_iconed  color_1">Well Stocked Gardens</h5>
                    <div class="sc_section"> 
                        At your disposal is a fantastic garden at the backyard of the house that will please you with fresh fuit &#038; veggies. 
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 margin_bottom_small">
                    <div class="sc_title_icon sc_title_left sc_size_large icon-bank sc_title_bg sc_bg_circle" style=""></div>
                    <h5 class="sc_title_align_left sc_title sc_title_iconed  color_1">Schools in Walking Distance</h5>
                    <div class="sc_section">
                        No worries about your kids! Schools are 5 minutes walk. As well, it’s extremely easy to drop them off and pick up!
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 margin_bottom_small">
                    <div class="sc_title_icon sc_title_left sc_size_large icon-photo-1 sc_title_bg sc_bg_circle" style=""></div>
                    <h5 class="sc_title_align_left sc_title sc_title_iconed  color_1">Fabulous Views</h5>
                    <div class="sc_section">
                        Get yourself surrounded by the stunning views opening around and enjoy relaxed atmosphere of the place.
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 margin_bottom_small">
                    <div class="sc_title_icon sc_title_left sc_size_large icon-key-2 sc_title_bg sc_bg_circle" style=""></div>
                    <h5 class="sc_title_align_left sc_title sc_title_iconed  color_1">Updated Kitchen</h5>
                    <div class="sc_section">
                        Kitchen is one of the most important spots in the house which is why it was made so modern and cozy.
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 margin_bottom_small">
                    <div class="sc_title_icon sc_title_left sc_size_large icon-target sc_title_bg sc_bg_circle" style=""></div>
                    <h5 class="sc_title_align_left sc_title sc_title_iconed  color_1">Large Play Center in Yard</h5>
                    <div class="sc_section">
                        Outdoor activities are too important for kids. You and your children will be happy having all these things around.
                    </div>
                </div>
            </div>
        </div>
    </section> 

    <section class="sc_section sc_aligncenter image_bg_2" >
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">		
                    <div class="sc_testimonials sc_testimonials_style sc_testimonials_controls_on">
                        <h2 class="sc_title sc_title_underline color_1 sc_testimonials_title">Testimonials</h2>
                        <div class="sc_slider sc_slider_swiper sc_slider_controls sc_slider_controls_on sc_slider_nopagination sc_slider_autoheight swiper-slider-container swiper_testimonials" data-interval="7000" id="swiper_testimonials">
                            <ul class="sc_testimonials_items slides swiper-wrapper">
                                <li class="sc_testimonials_item swiper-slide" data-autoheight="153">
                                    <div class="sc_testimonials_item_content">
                                        <div class="sc_testimonials_item_quote">
                                            <div class="sc_testimonials_item_text"> 
                                                If you are looking for a right place for your property to be taken care of you are right here.<br>
                                                Amazed by the professionalism and attitude to the client. Highly recommended. 
                                            </div>
                                        </div>
                                        <div class="sc_testimonials_item_author">
                                            <div class="sc_testimonials_item_avatar">
                                                <img alt="testimonials_1.jpg" src="images/testimonials/70x70.png">
                                            </div>
                                            <div class="sc_testimonials_item_name">Marcus Smith</div>
                                            <div class="sc_testimonials_item_position">Manager, New York</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="sc_testimonials_item swiper-slide" data-autoheight="153">
                                    <div class="sc_testimonials_item_content">
                                        <div class="sc_testimonials_item_quote">
                                            <div class="sc_testimonials_item_text"> 
                                                If you are looking for a cv template you are right here. Purchased and surprised about<br>
                                                handling and the quick support. highly recommended. 
                                            </div>
                                        </div>
                                        <div class="sc_testimonials_item_author">
                                            <div class="sc_testimonials_item_avatar">
                                                <img alt="testimonials_2.jpg" src="images/testimonials/70x70.png">
                                            </div>
                                            <div class="sc_testimonials_item_name">Amanda Sitam</div>
                                            <div class="sc_testimonials_item_position">Family residential, New York</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="sc_testimonials_item swiper-slide" data-autoheight="153">
                                    <div class="sc_testimonials_item_content">
                                        <div class="sc_testimonials_item_quote">
                                            <div class="sc_testimonials_item_text"> 
                                                This theme is top quality. Find an issue? Worry not, because the support that this team provides is amazing! I would definitely<br />recommend this theme for your next project or any other theme from ThemeREX for that matter. 
                                            </div>
                                        </div>
                                        <div class="sc_testimonials_item_author">
                                            <div class="sc_testimonials_item_avatar">
                                                <img alt="testimonials_3.jpg" src="images/testimonials/70x70.png">
                                            </div>
                                            <div class="sc_testimonials_item_name">Jeniffer King</div>
                                            <div class="sc_testimonials_item_position">Designer, New York</div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <ul class="flex-direction-nav">
                            <li>
                                <a class="flex-prev" href="#"></a>
                            </li>
                            <li>
                                <a class="flex-next" href="#"></a>
                            </li>
                        </ul>
                    </div> 
                </div>
            </div>
        </div>
    </section> 

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="sc_title_align_center sc_title sc_title_regular color_1">Selected Clients</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="sc_table sc_table_style_1 sc_table_size_no_indentation sc_table_align_center">
                        <table>
                            <thead>
                                <tr>
                                    <td>
                                        <a href="#" class="sc_image sc_image_hover sc_image_shape_square">
                                            <img src="{{ asset('assets/images/clients/292x164.png') }}" alt="" />
                                            <img class="img_hover" src="{{ asset('assets/images/clients/292x164.png') }}" alt="" />
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="sc_image sc_image_hover sc_image_shape_square">
                                            <img src="{{ asset('assets/images/clients/292x164.png') }}" alt="" />
                                            <img class="img_hover" src="{{ asset('assets/images/clients/292x164.png') }}" alt="" />
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="sc_image sc_image_hover sc_image_shape_square">
                                            <img src="{{ asset('assets/images/clients/292x164.png') }}" alt="" />
                                            <img class="img_hover" src="{{ asset('assets/images/clients/292x164.png') }}" alt="" />
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="sc_image sc_image_hover sc_image_shape_square">
                                            <img src="{{ asset('assets/images/clients/292x164.png') }}" alt="" />
                                            <img class="img_hover" src="{{ asset('assets/images/clients/292x164.png') }}" alt="" />
                                        </a>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <a href="#" class="sc_image sc_image_hover sc_image_shape_square">
                                            <img src="{{ asset('assets/images/clients/292x164.png') }}" alt="" />
                                            <img class="img_hover" src="{{ asset('assets/images/clients/292x164.png') }}" alt="" />
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="sc_image sc_image_hover sc_image_shape_square">
                                            <img src="{{ asset('assets/images/clients/292x164.png') }}" alt="" />
                                            <img class="img_hover" src="{{ asset('assets/images/clients/292x164.png') }}" alt="" />
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="sc_image sc_image_hover sc_image_shape_square">
                                            <img src="{{ asset('assets/images/clients/292x164.png') }}" alt="" />
                                            <img class="img_hover" src="{{ asset('assets/images/clients/292x164.png') }}" alt="" />
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="sc_image sc_image_hover sc_image_shape_square">
                                            <img src="{{ asset('assets/images/clients/292x164.png') }}" alt="" />
                                            <img class="img_hover" src="{{ asset('assets/images/clients/292x164.png') }}" alt="" />
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row aligncenter">
                <span class="sc_button sc_button_style_global sc_button_size_big squareButton global big">
                    <a href="#" class="">See our works</a>
                </span>
            </div>
        </div>
    </section>
</x-app-layout>