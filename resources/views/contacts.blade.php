<x-app-layout>
    <x-slot name="title">
        Contacts
    </x-slot>
    <section class="light_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">			
                    <!-- <div id="sc_googlemap_contacts" class="sc_googlemap" data-address="San Francisco, CA 94102, US" data-latlng="" data-zoom="14" data-style="default" data-point="{{ asset('assets/images/icon/60x80.png') }}"  data-title="San Francisco" data-description="San Francisco, CA 94102, US"></div> -->
                </div>
            </div>
        </div>
    </section> 

    <section class="light_section" >
        <div class="container">
            <div class="row">
                <div class="col-sm-12">		
                    <h2 class="sc_title_align_center sc_title sc_title_underline color_1">Get a Quote</h2>
                    <div class="sc_content sc_subtitle sc_aligncenter text_styling">
                        Request information about the property<br />
                        from certified specialist. Drop us a line! 
                    </div>
                </div>
            </div>
            <div>
                <div class="dark sc_contact_form sc_contact_form_contact_1">
                    <form class="contact_1" method="post" action="include/contact-form.php">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="required" for="sc_contact_form_username">Name</label>
                                <input type="text" name="name" id="sc_contact_form_username" placeholder="Name">
                            </div>
                            <div class="col-sm-6">
                                <label class="required" for="sc_contact_form_email">E-mail</label>
                                <input type="text" name="email" id="sc_contact_form_email" placeholder="Email">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="message">
                                    <div class="">
                                        <label class="required" for="sc_contact_form_message">Message</label>
                                        <textarea  id="sc_contact_form_message" class="textAreaSize" name="message" placeholder="Message"></textarea>
                                    </div>
                                </div>
                                <div class="sc_contact_form_button">
                                    <div class="squareButton sc_button_style_accent_2 sc_button_size_big global big">
                                        <button type="submit" name="contact_submit" class="sc_contact_form_submit">Send the message</button>
                                    </div>
                                </div>

                                <div class="result sc_infobox"></div>
                            </div>
                        </div>
                    </form>
                </div> 
            </div>
        </div>
    </section> 
</x-app-layout>