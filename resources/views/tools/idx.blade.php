<x-app-layout>
    <x-slot name="title">
        IDX
    </x-slot>
	<section class="fullwidth_section no_padding_container">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">

				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="dsidx" class="dsidx-results">

						<div class="dsidx-paging-control">Properties
							26 - 50 of 100
							|
							<a href="#">
							&#0171; First
							</a>
							|
							<a href="#">
							&lt; Previous
							</a>
							|
							<a href="#">
							Next &gt;
							</a>
							|
							<a href="#">
							Last &#0187;
							</a>
						</div>
						<div class="dsidx-sorting-control">Sorted by
							<form>
							<select >
								<option value="DateAdded|DESC" selected="selected">
									Time on market, newest first</option>
								<option value="DateAdded|ASC" >
									Time on market, oldest first</option>
								
								<option value="Price|DESC" >
									Price, highest first</option>
								<option value="Price|ASC" >
									Price, lowest first</option>
								
								<option value="OverallPriceDropPercent|DESC" >
									Price drop %, largest first</option>
								
								<option value="WalkScore|DESC" >
									Walk Score&#0174;, highest first</option>
								
								<option value="ImprovedSqFt|DESC" >
									Improved size, largest first</option>
								
								<option value="LotSqFt|DESC" >
									Lot size, largest first</option>
							</select>
							</form>
						</div>


						
						<div id="google_map_init" class="margin_bottom_mini">
							<div id="googlemap_hide">
								<div class="googlemap_button open">
									<img src="{{ asset('assets/images/real_estate/map_icon.png') }}">
								</div>
									<div id="sc_googlemap_2" class="sc_googlemap" data-zoom="12" data-style="default">
										<div id="sc_googlemap_2_1" class="sc_googlemap_marker" data-title="Costa Mesa" data-description="
										&lt;div class=&quot;sc_googlemap_desc&quot;&gt;
											&lt;img src=&quot;{{ asset('assets/images/real_estate/100x75.png') }}&quot; alt=&quot;&quot; class=&quot;alignnone sc_googlemap_img&quot;&gt;
											&lt;div class=&quot;sc_googlemap_textblock&quot;&gt;
												&lt;div class=&quot;idx_address&quot;&gt;75 Sunny St., Mission Viejo&lt;/div&gt;
												&lt;div class=&quot;idx_price&quot;&gt;$300,000&lt;/div&gt;
												&lt;div class=&quot;idx_descr&quot;&gt;5 beds, 5 full baths&lt;/div&gt;
												&lt;div class=&quot;idx_descr&quot;&gt;size: 5,000 sq ft&lt;/div&gt;
												&lt;div class=&quot;idx_descr&quot;&gt;Lot size: 43,560 sqft&lt;/div&gt;
											&lt;/div&gt;
										&lt;/div&gt;" data-address="Costa Mesa, w 19th st 9" data-latlng="" data-point="{{ asset('assets/images/real_estate/map_point.png') }}"></div>
										<div id="sc_googlemap_2_2" class="sc_googlemap_marker" data-title="Costa Mesa" data-description="
										&lt;div class=&quot;sc_googlemap_desc&quot;&gt;
											&lt;img src=&quot;{{ asset('assets/images/real_estate/100x75.png') }}&quot; alt=&quot;&quot; class=&quot;alignnone sc_googlemap_img&quot;&gt;
											&lt;div class=&quot;sc_googlemap_textblock&quot;&gt;
												&lt;div class=&quot;idx_address&quot;&gt;74 Sunny St., Mission Viejo&lt;/div&gt;
												&lt;div class=&quot;idx_price&quot;&gt;$340,000&lt;/div&gt;
												&lt;div class=&quot;idx_descr&quot;&gt;5 beds, 5 full baths&lt;/div&gt;
												&lt;div class=&quot;idx_descr&quot;&gt;size: 5,000 sq ft&lt;/div&gt;
												&lt;div class=&quot;idx_descr&quot;&gt;Lot size: 43,560 sqft&lt;/div&gt;
											&lt;/div&gt;
										&lt;/div&gt;" data-address="Costa Mesa, w 18th st 19" data-latlng="" data-point="{{ asset('assets/images/real_estate/map_point.png') }}"></div>
										<div id="sc_googlemap_2_3" class="sc_googlemap_marker" data-title="Costa Mesa" data-description="
										&lt;div class=&quot;sc_googlemap_desc&quot;&gt;
											&lt;img src=&quot;{{ asset('assets/images/real_estate/100x75.png') }}&quot; alt=&quot;&quot; class=&quot;alignnone sc_googlemap_img&quot;&gt;
											&lt;div class=&quot;sc_googlemap_textblock&quot;&gt;
												&lt;div class=&quot;idx_address&quot;&gt;73 Sunny St., Mission Viejo&lt;/div&gt;
												&lt;div class=&quot;idx_price&quot;&gt;$349,000&lt;/div&gt;
												&lt;div class=&quot;idx_descr&quot;&gt;5 beds, 4.5 baths&lt;/div&gt;
												&lt;div class=&quot;idx_descr&quot;&gt;size: 4,649 sq ft&lt;/div&gt;
												&lt;div class=&quot;idx_descr&quot;&gt;Lot size: 2.21 ac&lt;/div&gt;
											&lt;/div&gt;
										&lt;/div&gt;" data-address="Costa Mesa, Broadway 23" data-latlng="" data-point="{{ asset('assets/images/real_estate/map_point.png') }}"></div>
										<div id="sc_googlemap_2_4" class="sc_googlemap_marker" data-title="Costa Mesa" data-description="
										&lt;div class=&quot;sc_googlemap_desc&quot;&gt;
											&lt;img src=&quot;{{ asset('assets/images/real_estate/100x75.png') }}&quot; alt=&quot;&quot; class=&quot;alignnone sc_googlemap_img&quot;&gt;
											&lt;div class=&quot;sc_googlemap_textblock&quot;&gt;
												&lt;div class=&quot;idx_address&quot;&gt;72 Sunny St., Mission Viejo&lt;/div&gt;
												&lt;div class=&quot;idx_price&quot;&gt;$400,000&lt;/div&gt;
												&lt;div class=&quot;idx_descr&quot;&gt;2 beds, 1 full bath&lt;/div&gt;
												&lt;div class=&quot;idx_descr&quot;&gt;size: 700 sq ft&lt;/div&gt;
												&lt;div class=&quot;idx_descr&quot;&gt;Lot size: 6,750 sqft&lt;/div&gt;
											&lt;/div&gt;
										&lt;/div&gt;" data-address="Costa Mesa, cabrillo st 33" data-latlng="" data-point="{{ asset('assets/images/real_estate/map_point.png') }}"></div>
										<div id="sc_googlemap_2_5" class="sc_googlemap_marker" data-title="Costa Mesa" data-description="
										&lt;div class=&quot;sc_googlemap_desc&quot;&gt;
											&lt;img src=&quot;{{ asset('assets/images/real_estate/100x75.png') }}&quot; alt=&quot;&quot; class=&quot;alignnone sc_googlemap_img&quot;&gt;
											&lt;div class=&quot;sc_googlemap_textblock&quot;&gt;
												&lt;div class=&quot;idx_address&quot;&gt;71 Sunny St., Mission Viejo&lt;/div&gt;
												&lt;div class=&quot;idx_price&quot;&gt;$185,000&lt;/div&gt;
												&lt;div class=&quot;idx_descr&quot;&gt;3 beds, 2 full baths&lt;/div&gt;
												&lt;div class=&quot;idx_descr&quot;&gt;size: 1,700 sq ft&lt;/div&gt;
												&lt;div class=&quot;idx_descr&quot;&gt;Lot size: 1,263 sqft&lt;/div&gt;
											&lt;/div&gt;
										&lt;/div&gt;" data-address="Costa Mesa, dover dr 22" data-latlng="" data-point="{{ asset('assets/images/real_estate/map_point.png') }}"></div>
									</div>
							</div>
						</div>
						<ol id="dsidx-listings">
							<li class="dsidx-listing">
								<div class="dsidx-media">
									<div class="dsidx-photo"><a href="#">
										<img src="{{ asset('assets/images/real_estate/640x480.png') }}"
											alt="Photo of 75 Sunny St., Mission Viejo, CA 92692 (MLS # 20649489)" title="Photo of 75 Sunny St., Mission Viejo, CA 92692 (MLS # 20649489)" /></a>
									</div>
								</div>
								<div class="dsidx-primary-data">
									<div class="dsidx-address"><a href="#">
										75 Sunny St., Mission Viejo</a>
									</div>
									<div class="dsidx-price">
										$300,000
									</div>
								</div>
								<div class="dsidx-secondary-data">
										<div>5 beds, 5 full baths</div>
									<div>Home size: 5,000 sq ft</div>
									<div>Lot size: 43,560 sqft</div>
									<div>Year built: 2008</div>
									<div>Parking spots: 4</div>
									<div>Days on market: 925</div>
									<div>Walk Score&#0174;: 35</div>

								</div>
							</li>
							<li><hr /></li>
							<li class="dsidx-listing">
								<div class="dsidx-media">
									<div class="dsidx-photo"><a href="#">
										<img src="{{ asset('assets/images/real_estate/640x480.png') }}"
											alt="Photo of 74 Sunny St., Mission Viejo, CA 92692 (MLS # 20649488)" title="Photo of 74 Sunny St., Mission Viejo, CA 92692 (MLS # 20649488)" /></a>
									</div>
								</div>
								<div class="dsidx-primary-data">
									<div class="dsidx-address"><a href="#">
										74 Sunny St., Mission Viejo</a>
									</div>
									<div class="dsidx-price">
										$340,000
									</div>
								</div>
								<div class="dsidx-secondary-data">
										<div>5 beds, 5 full baths</div>
									<div>Home size: 5,000 sq ft</div>
									<div>Lot size: 43,560 sqft</div>
									<div>Year built: 2008</div>
									<div>Parking spots: 4</div>
									<div>Days on market: 925</div>
									<div>Walk Score&#0174;: 57</div>

								</div>
							</li>
							<li><hr /></li>
							<li class="dsidx-listing">
								<div class="dsidx-media">
									<div class="dsidx-photo"><a href="#">
										<img src="{{ asset('assets/images/real_estate/640x480.png') }}"
											alt="Photo of 73 Sunny St., Mission Viejo, CA 92691 (MLS # 20649458)" title="Photo of 73 Sunny St., Mission Viejo, CA 92691 (MLS # 20649458)" /></a>
									</div>
								</div>
								<div class="dsidx-primary-data">
									<div class="dsidx-address"><a href="#">
										73 Sunny St., Mission Viejo</a>
									</div>
									<div class="dsidx-price">
										$349,000
									</div>
								</div>
								<div class="dsidx-secondary-data">
										<div>5 beds, 4.5 baths</div>
									<div>Home size: 4,649 sq ft</div>
									<div>Lot size: 2.21 ac</div>
									<div>Year built: 1990</div>
									<div>Parking spots: 3</div>
									<div>Days on market: 925</div>
									<div>Walk Score&#0174;: 6</div>

								</div>
							</li>
							<li><hr /></li>
							<li class="dsidx-listing">
								<div class="dsidx-media">
									<div class="dsidx-photo"><a href="#">
										<img src="{{ asset('assets/images/real_estate/640x480.png') }}"
											alt="Photo of 72 Sunny St., Mission Viejo, CA 92691 (MLS # 20649457)" title="Photo of 72 Sunny St., Mission Viejo, CA 92691 (MLS # 20649457)" /></a>
									</div>
								</div>
								<div class="dsidx-primary-data">
									<div class="dsidx-address"><a href="#">
										72 Sunny St., Mission Viejo</a>
									</div>
									<div class="dsidx-price">
										$400,000
									</div>
								</div>
								<div class="dsidx-secondary-data">
										<div>2 beds, 1 full bath</div>
									<div>Home size: 700 sq ft</div>
									<div>Lot size: 6,750 sqft</div>
									<div>Year built: 1952</div>
									<div>Parking spots: 1</div>
									<div>Days on market: 929</div>
									<div>Walk Score&#0174;: 52</div>

								</div>
							</li>
							<li><hr /></li>
							<li class="dsidx-listing">
								<div class="dsidx-media">
									<div class="dsidx-photo"><a href="#">
										<img src="{{ asset('assets/images/real_estate/640x480.png') }}"
											alt="Photo of 71 Sunny St., Mission Viejo, CA 92691 (MLS # 20649456)" title="Photo of 71 Sunny St., Mission Viejo, CA 92691 (MLS # 20649456)" /></a>
									</div>
								</div>
								<div class="dsidx-primary-data">
									<div class="dsidx-address"><a href="#">
										71 Sunny St., Mission Viejo</a>
									</div>
									<div class="dsidx-price">
										$185,000
									</div>
								</div>
								<div class="dsidx-secondary-data">
										<div>3 beds, 2 full baths</div>
									<div>Home size: 1,700 sq ft</div>
									<div>Lot size: 1,263 sqft</div>
									<div>Year built: 2004</div>
									<div>Days on market: 930</div>
									<div>Walk Score&#0174;: 97</div>

								</div>
							</li>
						</ol>
						
						<div class="dsidx-paging-control">Properties
							26 - 50 of 100
							|
							<a href="#">
							&#0171; First
							</a>
							|
							<a href="#">
							&lt; Previous
							</a>
							|
							<a href="#">
							Next &gt;
							</a>
							|
							<a href="#">
							Last &#0187;
							</a>
						</div>
						<div id="dsidx-disclaimer">
							<p>Listing information deemed reliable but not guaranteed</p><p>This MLS IDX is (c) Diverse Solutions 2015.</p>
						</div>
						
					</div>						
				</div>
			</div>
		</div>
	</section>
</x-app-layout>