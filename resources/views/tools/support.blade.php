<x-app-layout>
    <x-slot name="title">
        Support
    </x-slot>
	<section class="light_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="sc_title_align_center sc_title sc_title_underline color_1">Support Policy</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<ul class="sc_list sc_list_style_ul">
						<li class="sc_list_item">To get a <b>pre-purchase advice</b>, you may post in the theme’s forum
						<br />
						<br />
						</li>
						<li class="sc_list_item">To get a response more promptly, you are recommended to use our <b>Ticket-system</b>
						<br />
						<br />
						</li>
						<li class="sc_list_item">We try to answer your questions within <b>1-10 hours</b>. However, if any delay occurs it doesn’t mean that we forgot about you. Some of the issues require testing and analyzing, so we could help you more profoundly.
						</li>
					</ul>
				</div>
				<div class="col-sm-6">
					<ul class="sc_list sc_list_style_ul">
						<li class="sc_list_item">
						<b>Please, note!</b> We do not support queries if you do not have a purchase code.
						<br />
						<br />
						</li>
						<li class="sc_list_item">
							Item support <b>does not include:</b>
						</li>
						<ul>
						<li>Customization and installation services</li>
						<li>Support for third party software and plug-ins</li>
						</ul>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section class="grey_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="sc_title_align_center sc_title sc_title_underline color_1">How to use our Support System</h2>
					<div class="sc_subtitle sc_content sc_aligncenter">
						ThemeRex technical support is always there to assist their users<br />
						with all their technical questions regarding ThemeRex products. 
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<figure class="sc_image  sc_image_shape_square">
						<img src="{{ asset('assets/images/557x374.png') }}" alt="" />
					</figure>
				</div>
				<div class="col-sm-6">
					<ol class="sc_list sc_list_style_ol">
						<li class="sc_list_item">
							In the <b>‘Item Details’</b> there is a clickable banner <b>“Support Sys”</b> using which you are getting into tickets.
							<br />
							<br />
						</li>
						<li class="sc_list_item">
							Or you may simply use this link <a style="color:#f55858;" href="http://themerex.ticksy.com">themerex.ticksy.com</a>
							<br />
							<br />
						</li>
						<li class="sc_list_item">
							Enter your <b>purchase key</b> and register in the system
							<br />
							<br />
						</li>
						<li class="sc_list_item">
							To avoid confusing, please use for Support System <b>your own ThemeForest login</b>
							<br />
							<br />
						</li>
						<li class="sc_list_item">
							Fill out the <b>ticket form</b>
						</li>
					</ol>
				</div>
			</div>
		</div>
	</section>

	<section class="light_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="sc_title_align_center sc_title sc_title_underline color_1">How to find a Purchase key</h2>
				</div>
				<div class="col-sm-6">
					<figure class="sc_image sc_image_shape_square text-center">
						<img src="{{ asset('assets/images/455x350.png') }}" alt="" />
					</figure>
				</div>
				<div class="col-sm-6">
					<ol class="sc_list sc_list_style_ol margin_top_small">
						<li class="sc_list_item">
							Open the <b>‘downloads’ tab</b> in your account	
							<br />
							<br />		
						</li>
						<li class="sc_list_item">
							<b>Find our theme</b> in the list of products
							<br />
							<br />
						</li>
						<li class="sc_list_item">
							Click <b>‘download’</b> button, and in the drop down menu select <b>“license sertificate &#038; purchase code”</b>
						</li>
					</ol>
				</div>
			</div>
			<div class="row support_button">
				<div class="col-md-4 col-sm-12 no_margin_top">
					<div class="sc_section">
					<span class="sc_button sc_button_style_accent_1 sc_button_size_banner alignleft squareButton fullSize accent_1 banner">
					<a href="http://themerex.ticksy.com" class="">Create Ticket</a>
					</span>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 no_margin_top">
					<div class="sc_section">
						<span class="sc_button sc_button_style_accent_1 sc_button_size_banner alignleft squareButton fullSize accent_1 banner">
							<a href="#" class="">Theme Forum</a>
						</span>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 no_margin_top">
					<div class="sc_section">
						<span class="sc_button sc_button_style_accent_1 sc_button_size_banner alignleft squareButton fullSize accent_1 banner">
							<a href="doc/index.html" class="">Theme Documentation</a>
						</span>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="grey_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="sc_title_align_center sc_title sc_title_underline color_1">Support Team</h2>
					<div class="sc_subtitle sc_content sc_aligncenter">
						We create our themes with a thought of our customers, therefore<br />
						our team works hard to provide you best technical support in the world. 
					</div>
				</div>
			</div>
			<div class="row">
			<div class="sc_team">
				<div class="sc_columns columnsWrap">
					<div class="col-sm-4 no_margin_top">
						<div class="sc_team_item sc_team_item_1 odd first">
							<div class="sc_team_item_avatar">
								<img alt="t2.jpg" src="{{ asset('assets/images/team/370x370.png') }}">
								<a class="hoverLink" href="#"></a>
							</div>
							<div class="sc_team_item_info">
								<h4 class="sc_team_item_title">Amanda Doe</h4>
								<div class="sc_team_item_position">Developer</div>
								<ul class="sc_team_item_socials">
									<li>
										<a href="https://twitter.com/Theme_REX" class="social_icons social_twitter icon-twitter" target="_blank"></a>
									</li>
									<li>
										<a href="https://plus.google.com/102189073109602153696" class="social_icons social_gplus icon-gplus" target="_blank"></a>
									</li>
									<li>
										<a href="https://www.facebook.com/themerex" class="social_icons social_facebook icon-facebook" target="_blank"></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-4 no_margin_top">
						<div class="sc_team_item sc_team_item_2 even">
							<div class="sc_team_item_avatar">
								<img alt="t4.jpg" src="{{ asset('assets/images/team/370x370.png') }}">
								<a class="hoverLink" href="#"></a>
							</div>
							<div class="sc_team_item_info">
								<h4 class="sc_team_item_title">Natalia Sitam</h4>
								<div class="sc_team_item_position">Designer</div>
								<ul class="sc_team_item_socials">
									<li>
										<a href="https://twitter.com/Theme_REX" class="social_icons social_twitter icon-twitter" target="_blank"></a>
									</li>
									<li>
										<a href="https://plus.google.com/102189073109602153696" class="social_icons social_gplus icon-gplus" target="_blank"></a>
									</li>
									<li>
										<a href="https://www.facebook.com/themerex" class="social_icons social_facebook icon-facebook" target="_blank"></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-4 no_margin_top">
						<div class="sc_team_item sc_team_item_3 odd">
							<div class="sc_team_item_avatar">
								<img alt="t1.jpg" src="{{ asset('assets/images/team/370x370.png') }}">
								<a class="hoverLink" href="#"></a>
							</div>
							<div class="sc_team_item_info">
								<h4 class="sc_team_item_title">John Smith</h4>
								<div class="sc_team_item_position">Programmer</div>
								<ul class="sc_team_item_socials">
									<li>
										<a href="https://twitter.com/Theme_REX" class="social_icons social_twitter icon-twitter" target="_blank"></a>
									</li>
									<li>
										<a href="https://plus.google.com/102189073109602153696" class="social_icons social_gplus icon-gplus" target="_blank"></a>
									</li>
									<li>
										<a href="https://www.facebook.com/themerex" class="social_icons social_facebook icon-facebook" target="_blank"></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</section>
	</x-app-layout>