<x-app-layout>
    <x-slot name="title">
        Shortcodes
    </x-slot>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-12">
					<article>
						<h2>Accordion</h2>
						<div class="sc_accordion sc_accordion_style_1" data-active="0">
							<div class="sc_accordion_item">
								<h5 class="sc_accordion_title">Accordion 1</h5>
								<div class="sc_accordion_content"> 
									Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
								</div>
							</div>
							<div class="sc_accordion_item">
								<h5 class="sc_accordion_title">Accordion 2</h5>
								<div class="sc_accordion_content">
									Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
								</div>
							</div>
							<div class="sc_accordion_item">
								<h5 class="sc_accordion_title">Accordion 3</h5>
								<div class="sc_accordion_content"> 
									Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
								</div>
							</div>
						</div>
						<div class="sc_accordion sc_accordion_style_2" data-active="0">
							<div class="sc_accordion_item">
								<h5 class="sc_accordion_title">Accordion 1</h5>
								<div class="sc_accordion_content">
									Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
								</div>
							</div>
							<div class="sc_accordion_item">
								<h5 class="sc_accordion_title">Accordion 2</h5>
								<div class="sc_accordion_content">
									Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
								</div>
							</div>
							<div class="sc_accordion_item">
								<h5 class="sc_accordion_title">Accordion 3</h5>
								<div class="sc_accordion_content">
									Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
								</div>
							</div>
						</div>
					</article>
					<div class="sc_line sc_line_style_solid margin_bottom_middle"></div>
					<article>
						<h2>Tabs</h2>
						<div class="   sc_tabs sc_tabs_style_1" data-active="0">
							<ul class="sc_tabs_titles">
								<li class="tab_names">
									<a href="#sc_tabs_style_1_1" class="theme_button" id="sc_tabs_style_1_1_tab">Tab title 1</a>
								</li>
								<li class="tab_names">
									<a href="#sc_tabs_style_1_2" class="theme_button" id="sc_tabs_style_1_2_tab">Tab title 2</a>
								</li>
								<li class="tab_names last">
									<a href="#sc_tabs_style_1_3" class="theme_button" id="sc_tabs_style_1_3_tab">Tab title 3</a>
								</li>
							</ul>
							<div id="sc_tabs_style_1_1" class="sc_tabs_content"> 
								Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. 
							</div>
							<div id="sc_tabs_style_1_2" class="sc_tabs_content"> 
								Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. 
							</div>
							<div id="sc_tabs_style_1_3" class="sc_tabs_content"> 
								Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. 
							</div>
						</div>
						<div class="sc_line sc_line_style_dashed margin_bottom_middle"></div>
						<div class="   sc_tabs sc_tabs_style_2" data-active="0">
							<ul class="sc_tabs_titles">
								<li class="tab_names">
									<a href="#sc_tabs_style_2_1" class="theme_button" id="sc_tabs_style_2_1_tab">Tab title 1</a>
								</li>
								<li class="tab_names">
									<a href="#sc_tabs_style_2_2" class="theme_button" id="sc_tabs_style_2_2_tab">Tab title 2</a>
								</li>
								<li class="tab_names last">
									<a href="#sc_tabs_style_2_3" class="theme_button" id="sc_tabs_style_2_3_tab">Tab title 3</a>
								</li>
							</ul>
							<div class="sc_tabs_wrap">
							<div id="sc_tabs_style_2_1" class="sc_tabs_content"> 
								Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
							</div>
							<div id="sc_tabs_style_2_2" class="sc_tabs_content"> 
								Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
							</div>
							<div id="sc_tabs_style_2_3" class="sc_tabs_content"> 
								Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
							</div>
							</div>
						</div>
					</article>
					<div class="sc_line sc_line_style_solid margin_bottom_middle"></div>
					<article>
						<h2>Table</h2>
						<div class="sc_table sc_table_style_1 sc_table_size_medium sc_table_align_center">
							<table>
								<thead>
									<tr>
										<th>#</th>
										<th>Column 1</th>
										<th>Column 2</th>
										<th>Column 3</th>
										<th>Column 4</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>Raw 1 cell 1</td>
										<td>Raw 1 cell 2</td>
										<td>Raw 1 cell 3</td>
										<td>Raw 1 cell 4</td>
									</tr>
									<tr>
										<td>2</td>
										<td>Raw 2 cell 1</td>
										<td>Raw 2 cell 2</td>
										<td>Raw 2 cell 3</td>
										<td>Raw 2 cell 4</td>
									</tr>
									<tr>
										<td>3</td>
										<td>Raw 3 cell 1</td>
										<td>Raw 3 cell 2</td>
										<td>Raw 3 cell 3</td>
										<td>Raw 3 cell 4</td>
									</tr>
									<tr>
										<td>4</td>
										<td>Raw 4 cell 1</td>
										<td>Raw 4 cell 2</td>
										<td>Raw 4 cell 3</td>
										<td>Raw 4 cell 4</td>
									</tr>
								</tbody>
							</table>
						</div>
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Audio Player</h2>
						<div class="audio_container with_info">
							<span class="audio_info">Lily hunter</span>
							<h4 class="audio_info">Insert Audio Title Here</h4>
							<div>
								<audio src="sounds/laura.mp3" class="sc_audio" controls="controls" data-title="Insert Audio Title Here" data-author="Lily hunter"></audio>
							</div>
						</div>
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Video Player</h2>
						<div class="sc_video_player" data-width="100%" data-height="430">
							<div class="sc_video_frame sc_video_play_button" data-video="&lt;iframe class=&quot;video_frame&quot; src=&quot;https://youtube.com/embed/YcX3IdXzZWs?autoplay=1&quot; width=&quot;100%&quot; height=&quot;430&quot; frameborder=&quot;0&quot; webkitAllowFullScreen=&quot;webkitAllowFullScreen&quot; mozallowfullscreen=&quot;mozallowfullscreen&quot; allowFullScreen=&quot;allowFullScreen&quot;&gt;&lt;/iframe&gt;">
							<img alt="" src="{{ asset('assets/images/video/1280x720.png') }}">
							</div>
						</div>
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Slider</h2>
						<div id="sc_slider_shortcodes" class="sc_slider sc_slider_autoheight sc_slider_swiper sc_slider_controls sc_slider_pagination swiper-slider-container" data-interval="5000">
							<ul class="slides swiper-wrapper">
								<li class="swiper-slide">
									<img src="{{ asset('assets/images/slider/1900x1266.png') }}" alt="">
								</li>
								<li class="swiper-slide">
									<img src="{{ asset('assets/images/slider/1900x1262.png') }}" alt="">
								</li>
								<li class="swiper-slide">
									<img src="{{ asset('assets/images/slider/1900x1266.png') }}" alt="">
								</li>
							</ul>
							<ul class="flex-direction-nav">
								<li>
									<a class="flex-prev" href="#"></a>
								</li>
								<li>
									<a class="flex-next" href="#"></a>
								</li>
							</ul>
							<div class="flex-control-nav"></div>
						</div>
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2 id="go_buttons">Buttons</h2>
						<div class="sc_section  margin_bottom_mini">
							<span class="sc_button sc_button_style_accent_1 sc_button_size_big squareButton accent_1 big">
								<a href="#" class="">Example</a>
							</span>
							<span class="sc_button sc_button_style_global sc_button_size_big squareButton global big">
								<a href="#" class="">Example</a>
							</span>
							<span class="sc_button sc_button_style_accent_2 sc_button_size_big squareButton accent_2 big">
								<a href="#" class="">Example</a>
							</span>
							<span class="sc_button sc_button_style_gray sc_button_size_big squareButton gray big">
								<a href="#" class="">Example</a>
							</span>
						</div>
						<div class="sc_section">
							<span class="sc_button sc_button_style_accent_1 sc_button_size_medium squareButton accent_1 medium">
								<a href="#" class="">Example</a>
							</span>
							<span class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
								<a href="#" class="">Example</a>
							</span>
							<span class="sc_button sc_button_style_accent_2 sc_button_size_medium squareButton accent_2 medium">
								<a href="#" class="">Example</a>
							</span>
							<span class="sc_button sc_button_style_gray sc_button_size_medium squareButton gray medium">
								<a href="#" class="">Example</a>
							</span>
						</div>
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Standart Inline Elements</h2>
						<p><span class="sc_highlight sc_highlight_style_3">Bold text</span> dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <span class="sc_highlight sc_highlight_style_4">incididunt</span> labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex <a href="#">dafault link color</a>. Duis aute irure dolor in <span class="sc_highlight">
						<del class="sc_highlight sc_highlight_style_4">reprehenderit</del>
						</span> in voluptate <span class="sc_highlight sc_highlight_style_1">velit esse cillum</span> dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae  vitae dict <span class="sc_highlight sc_highlight_style_5">vitae dicta sunt</span> explicabo. Nemo voluptatem quia <span class="sc_tooltip_parent">voluptas sit aspernatur<span class="sc_tooltip">Tooltip Title</span>
						</span> aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Dropcaps</h2>
						<div class="columnsWrap sc_columns sc_columns_count_2 row">
							<div class="col-sm-6 sc_column_item">
								<div class="sc_dropcaps sc_dropcaps_style_1">
									<span class="sc_dropcap">D</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad miamco laboris nisi utmodo consequat.
								</div>
							</div>
							<div class="col-sm-6 sc_column_item">
								<div class="sc_dropcaps sc_dropcaps_style_2">
									<span class="sc_dropcap">D</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad miamco laboris nisi utmodo consequat.
								</div>
							</div>
						</div>
						<div class="columnsWrap sc_columns sc_columns_count_2 row">
							<div class="col-sm-6 sc_column_item">
								<div class="sc_dropcaps sc_dropcaps_style_3">
									<span class="sc_dropcap">D</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad miamco laboris nisi utmodo consequat.
								</div>
							</div>
							<div class="col-sm-6 sc_column_item">
								<div class="sc_dropcaps sc_dropcaps_style_4">
									<span class="sc_dropcap">D</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad miamco laboris nisi utmodo consequat.
								</div>
							</div>
						</div>
						<div class="columnsWrap sc_columns sc_columns_count_2 row">
							<div class="col-sm-6 sc_column_item">
								<div class="sc_dropcaps sc_dropcaps_style_5">
									<span class="sc_dropcap">D</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad miamco laboris nisi utmodo consequat.
								</div>
							</div>
							<div class="col-sm-6 sc_column_item">
								<div class="sc_dropcaps sc_dropcaps_style_6">
									<span class="sc_dropcap">D</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad miamco laboris nisi utmodo consequat.
								</div>
							</div>
						</div>
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Blockquotes</h2>
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						<blockquote cite="#" class="sc_quote sc_quote_style_1 margin_bottom_mini">
							<p>Always bear in mind that your own resolution to succeed is more important than any other.</p>
							<p class="sc_quote_title">
								<a href="#">Albert Einstein</a>
							</p>
						</blockquote> 
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
						<blockquote cite="#" class="sc_quote sc_quote_style_2 margin_bottom_mini">
							<p>Always bear in mind that your own resolution to succeed is more important than any other.</p>
							<p class="sc_quote_title">
							<a href="#">Albert Einstein</a>
							</p>
						</blockquote> 
						Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Google Map</h2>
						{{-- <div id="sc_googlemap_shortcodes" class="sc_googlemap" data-address="San Francisco, CA 94102, US" data-latlng="" data-zoom="14" data-style="style1" data-point="{{ asset('assets/images/icon/60x80.png') }}"  data-title="San Francisco" data-description="San Francisco, CA 94102, US"></div>							 --}}
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Allert Message</h2>
						<div class="sc_infobox sc_infobox_style_regular sc_infobox_closeable">
							<b>General message goes here</b>
						</div>
						<div class="sc_infobox sc_infobox_style_success sc_infobox_closeable">
							<b>success message goes here</b>
						</div>
						<div class="sc_infobox sc_infobox_style_warning sc_infobox_closeable">
							<b>warning message goes here</b>
						</div>
						<div class="sc_infobox sc_infobox_style_info sc_infobox_closeable">
							<b>information message goes here</b>
						</div>
						<div class="sc_infobox sc_infobox_style_error sc_infobox_closeable">
							<b>error message goes here</b>
						</div>
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Progress Bars</h2>
						<div class="row">
							<div class="col-sm-6 sc_column_item no_margin_top">
								<div class="sc_skills sc_skills_bar sc_skills_horizontal" data-type="bar" data-subtitle="Skills" data-dir="horizontal">
									<div class="sc_skills_item sc_skills_style_1 sc_skills_style_bar_1">
										<div class="sc_skills_total" data-start="0" data-stop="85" data-step="1" data-max="100" data-speed="12" data-duration="1020" data-ed="%">0%</div>
										<div class="sc_skills_info">Graphic design</div>
										<div class="sc_skills_wrap">
											<div class="sc_skills_count" style="background-color:#1f9ad6;"></div>
										</div>
									</div>
									<div class="sc_skills_item sc_skills_style_1 sc_skills_style_bar_1">
										<div class="sc_skills_total" data-start="0" data-stop="50" data-step="1" data-max="100" data-speed="11" data-duration="550" data-ed="%">0%</div>
										<div class="sc_skills_info">HTML5 &#038; Css3</div>
										<div class="sc_skills_wrap">
											<div class="sc_skills_count" style="background-color:#1f9ad6;"></div>
										</div>
									</div>
									<div class="sc_skills_item sc_skills_style_1 sc_skills_style_bar_1">
										<div class="sc_skills_total" data-start="0" data-stop="45" data-step="1" data-max="100" data-speed="30" data-duration="1350" data-ed="%">0%</div>
										<div class="sc_skills_info">Web design</div>
										<div class="sc_skills_wrap">
											<div class="sc_skills_count" style="background-color:#1f9ad6;"></div>
										</div>
									</div>
									<div class="sc_skills_item sc_skills_style_1 sc_skills_style_bar_1">
										<div class="sc_skills_total" data-start="0" data-stop="70" data-step="1" data-max="100" data-speed="15" data-duration="1050" data-ed="%">0%</div>
										<div class="sc_skills_info">Wordpress</div>
										<div class="sc_skills_wrap">
											<div class="sc_skills_count" style="background-color:#1f9ad6;"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6 sc_column_item">
								<div class="sc_skills sc_skills_bar sc_skills_horizontal" data-type="bar" data-subtitle="Skills" data-dir="horizontal">
									<div class="sc_skills_item sc_skills_style_1 sc_skills_style_bar_2">
										<div class="sc_skills_total" data-start="0" data-stop="85" data-step="1" data-max="100" data-speed="21" data-duration="1785" data-ed="%">0%</div>
										<div class="sc_skills_info">Graphic design</div>
										<div class="sc_skills_wrap">
											<div class="sc_skills_count" style="background-color:#1f9ad6;"></div>
										</div>
									</div>
									<div class="sc_skills_item sc_skills_style_1 sc_skills_style_bar_2">
										<div class="sc_skills_total" data-start="0" data-stop="50" data-step="1" data-max="100" data-speed="28" data-duration="1400" data-ed="%">0%</div>
										<div class="sc_skills_info">HTML5 &#038; Css3</div>
										<div class="sc_skills_wrap">
											<div class="sc_skills_count" style="background-color:#1f9ad6;"></div>
										</div>
									</div>
									<div class="sc_skills_item sc_skills_style_1 sc_skills_style_bar_2">
										<div class="sc_skills_total" data-start="0" data-stop="45" data-step="1" data-max="100" data-speed="19" data-duration="855" data-ed="%">0%</div>
										<div class="sc_skills_info">Web design</div>
										<div class="sc_skills_wrap">
											<div class="sc_skills_count" style="background-color:#1f9ad6;"></div>
										</div>
									</div>
									<div class="sc_skills_item sc_skills_style_1 sc_skills_style_bar_2">
										<div class="sc_skills_total" data-start="0" data-stop="70" data-step="1" data-max="100" data-speed="17" data-duration="1190" data-ed="%">0%</div>
										<div class="sc_skills_info">Wordpress</div>
										<div class="sc_skills_wrap">
											<div class="sc_skills_count" style="background-color:#1f9ad6;"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Separators</h2>
						<div class="sc_line sc_line_style_solid margin_bottom_none"></div>
						Solid gray 
						<div class="sc_line sc_line_style_dashed margin_top_mini margin_bottom_none"></div>
						Dashed gray 
						<div class="sc_line sc_line_style_solid_dark margin_top_mini margin_bottom_none"></div>
						Solid dark 
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Circle Progress</h2>
						<div class="sc_skills sc_skills_pie" data-type="pie" data-subtitle="Skills">
							<div class="columnsWrap sc_skills_columns">
								<div class="sc_skills_column col-md-4 col-sm-4">
									<div class="sc_skills_item sc_skills_style_1">
										<canvas id="sc_skills_canvas_591631475"></canvas>
										<div class="sc_skills_total" data-start="0" data-stop="80" data-step="1" data-steps="100" data-max="100" data-speed="38" data-duration="3040" data-color="#1f9ad6" data-easing="easeOutCirc" data-ed="%">0%</div>
									</div>
									<div class="sc_skills_info">Graphic design</div>
								</div>
								<div class="sc_skills_column col-md-4 col-sm-4">
									<div class="sc_skills_item sc_skills_style_1">
										<canvas id="sc_skills_canvas_389831371"></canvas>
										<div class="sc_skills_total" data-start="0" data-stop="75" data-step="1" data-steps="100" data-max="100" data-speed="12" data-duration="900" data-color="#1f9ad6" data-easing="easeOutCirc" data-ed="%">0%</div>
									</div>
									<div class="sc_skills_info">HTML5 &#038; Css3</div>
								</div>
								<div class="sc_skills_column col-md-4 col-sm-4">
									<div class="sc_skills_item sc_skills_style_1">
										<canvas id="sc_skills_canvas_138400841"></canvas>
										<div class="sc_skills_total" data-start="0" data-stop="95" data-step="1" data-steps="100" data-max="100" data-speed="33" data-duration="3135" data-color="#1f9ad6" data-easing="easeOutCirc" data-ed="%">0%</div>
									</div>
									<div class="sc_skills_info">Web design</div>
								</div>
							</div>
						</div> 
					</article>
				</div>
				<div class="col-md-4 col-sm-12">
					<div id="sidebar_main" class="widget_area sidebar_main sidebar sidebarStyleDark" role="complementary">
						<aside class=" widgetWrap widget widget_categories">
							<h5 class="title">Categories</h5>
							<ul>
								<li class="cat-item current-cat-parent dropMenu">
									<a href="blog-classic-style-1-columns.html">Classic Style</a> (14)
									<ul class="children">
										<li class="cat-item">
											<a href="blog-classic-style-1-columns.html">1 Column</a> (9)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-2-columns.html">2 Columns</a> (11)
										</li>
										<li class="cat-item current-cat">
											<a href="blog-classic-style-2-columns-sidebar.html">2 Columns + sidebar</a> (11)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-3-columns.html">3 Columns</a> (10)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-3-columns-sidebar.html">3 Columns + sidebar</a> (11)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-4-columns.html">4 Columns</a> (10)
										</li>
									</ul>
								</li>
								<li class="cat-item">
									<a href="blog-classic-style-large.html">Classic Style Large</a> (13)
								</li>
								<li class="cat-item">
									<a href="blog-classic-style-small.html">Classic Style Small</a> (12)
								</li>
								<li class="cat-item">
									<a href="#">Contemporary</a> (15)
								</li>
								<li class="cat-item">
									<a href="#">Cottage</a> (5)
								</li>
								<li class="cat-item">
									<a href="gallery-grid-alternative.html">Grid Alternative</a> (22)
								</li>
								<li class="cat-item dropMenu">
									<a href="blog-masonry-style-2-columns.html">Masonry demo</a> (18)
									<ul class="children">
										<li class="cat-item">
											<a href="blog-masonry-style-2-columns.html">2 Columns</a> (17)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-2-columns-sidebar.html">2 Columns + sidebar</a> (17)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-3-columns.html">3 Columns</a> (17)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-3-columns-sidebar.html">3 Columns + sidebar</a> (13)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-4-columns.html">4 Columns</a> (16)
										</li>
									</ul>
								</li>
								<li class="cat-item dropMenu">
									<a href="gallery-classic-style-1-column.html">Portfolio Classic</a> (22)
									<ul class="children">
										<li class="cat-item">
											<a href="gallery-classic-style-1-column.html">1 Column</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-1-column-sidebar.html">1 Column + sidebar</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-2-columns.html">2 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-2-columns-sidebar.html">2 Columns + sidebar</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-3-columns.html">3 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-3-columns-sidebar.html">3 Columns + sidebar</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-4-columns.html">4 Columns</a> (22)
										</li>
									</ul>
								</li>
								<li class="cat-item dropMenu">
									<a href="gallery-grid-style-2-columns.html">Portfolio Grid</a> (22)
									<ul class="children">
										<li class="cat-item">
											<a href="gallery-grid-style-2-columns.html">2 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-2-columns-fullscreen.html">2 Columns fullscreen</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-3-columns.html">3 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-3-columns-fullscreen.html">3 Columns fullscreen</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-4-columns.html">4 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-4-columns-fullscreen.html">4 Columns fullscreen</a> (22)
										</li>
									</ul>
								</li>
								<li class="cat-item dropMenu">
									<a href="gallery-masonry-style-2-columns.html">Portfolio Masonry</a> (12)
									<ul class="children">
										<li class="cat-item">
											<a href="gallery-masonry-style-2-columns.html">2 Columns</a> (11)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-2-columns-sidebar.html">2 Columns + sidebar</a> (12)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-3-columns.html">3 Columns</a> (12)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-3-columns-sidebar.html">3 Columns + sidebar</a> (12)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-4-columns.html">4 Columns</a> (12)
										</li>
									</ul>
								</li>
								<li class="cat-item">
									<a href="gallery-grid-alternative.html">Projects</a> (9)
								</li>
							</ul>
						</aside>
						<aside class="widgetWrap widget widget_recent_comments">
							<h5 class="title">Recent Comments</h5>
							<ul>
								<li class="recentcomments">
									<span class="comment-author-link">Admin</span> 
									on 
									<a href="#">Is Condo Life For You?</a>
								</li>
								<li class="recentcomments">
									<span class="comment-author-link">Admin</span> 
									on 
									<a href="#">New Post With Image</a>
								</li>
								<li class="recentcomments">
									<span class="comment-author-link">Admin</span> 
									on 
									<a href="#">House Market Indicators</a>
								</li>
							</ul>
						</aside>
						<aside class="widgetWrap widget widget_archive">
							<h5 class="title">Archives</h5>
							<ul>
								<li>
									<a href='#'>January 2015</a>&nbsp;(3)
								</li>
								<li>
									<a href='#'>December 2014</a>&nbsp;(59)
								</li>
								<li>
									<a href='#'>November 2014</a>&nbsp;(3)
								</li>
								<li>
									<a href='#'>September 2014</a>&nbsp;(1)
								</li>
								<li>
									<a href='#'>August 2014</a>&nbsp;(1)
								</li>
								<li>
									<a href='#'>July 2014</a>&nbsp;(2)
								</li>
								<li>
									<a href='#'>May 2014</a>&nbsp;(5)
								</li>
								<li>
									<a href='#'>February 2014</a>&nbsp;(1)
								</li>
							</ul>
						</aside>
						<aside class="widgetWrap widget widget_search">
							<form role="search" method="get" class="search-form" action="index.html">
								<input type="text" class="search-field" placeholder="Search" value="" name="s" title="Search for:" />
								<span class="search-button light ico">
									<a class="search-field icon-search" href="#"></a>
								</span>
							</form>
						</aside>
						<aside class="widgetWrap widget widget_calendar">
							<div id="calendar_wrap">
								<table class="calendar">
									<thead>
										<tr>
											<th class="curMonth" colspan="7">
												<a href="#" title="View posts for August 2015">August</a>
											</th>
										</tr>
										<tr>
											<th scope="col" title="Monday">Mon</th>
											<th scope="col" title="Tuesday">Tue</th>
											<th scope="col" title="Wednesday">Wed</th>
											<th scope="col" title="Thursday">Thu</th>
											<th scope="col" title="Friday">Fri</th>
											<th scope="col" title="Saturday">Sat</th>
											<th scope="col" title="Sunday">Sun</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td colspan="1" class="pad">&nbsp;</td>
											<td>
												<span>1</span>
											</td>
											<td>
												<span>2</span>
											</td>
											<td>
												<a href="#" title="Post Formats – Chat">3</a>
											</td>
											<td>
												<span>4</span>
											</td>
											<td>
												<a href="#" title="Vimeo Video Post">5</a>
											</td>
											<td>
												<span>6</span>
											</td>
										</tr>
										<tr>
											<td>
												<span>7</span>
											</td>
											<td>
												<a href="#" title="What Is Housing Сooperative">8</a>
											</td>
											<td>
												<span>9</span>
											</td>
											<td>
												<a href="#" title="Post format – Aside">10</a>
											</td>
											<td>
												<span>11</span>
											</td>
											<td>
												<span>12</span>
											</td>
											<td>
												<span>13</span>
											</td>
										</tr>
										<tr>
											<td>
												<a href="#" title="Post formats – Link">14</a>
											</td>
											<td>
												<a href="#" title="Audio Post">15</a>
											</td>
											<td>
												<span>16</span>
											</td>
											<td>
												<span>17</span>
											</td>
											<td>
												<a href="#" title="Gallery Post Format">18</a>
											</td>
											<td>
												<a href="#" title="Post With Image">19</a>
											</td>
											<td>
												<span>20</span>
											</td>
										</tr>
										<tr>
											<td>
												<span>21</span>
											</td>
											<td>
												<span>22</span>
											</td>
											<td>
												<a href="#" title="Post Without Image">23</a>
											</td>
											<td>
												<span>24</span>
											</td>
											<td>
												<span>25</span>
											</td>
											<td>
												<a href="#" title="Status">26</a>
											</td>
											<td>
												<span>27</span>
											</td>
										</tr>
										<tr>
											<td>
												<span>28</span>
											</td>
											<td class="today">
												<span>29</span>
											</td>
											<td>
												<span>30</span>
											</td>
											<td class="pad" colspan="4">&nbsp;</td>
										</tr>
									</tbody>

									<tfoot>
										<tr>
											<th colspan="4" class="prevMonth">
												<div class="left">
													<a href="#" data-type="post" data-year="2014" data-month="7" title="View posts for July 2015">Jul</a>
												</div>
											</th>
											<th colspan="3" class="nextMonth">
												<div class="right">
													<a href="#" data-type="post" data-year="2015" data-month="9" title="View posts for September 2015">Sep</a>
												</div>
											</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</aside>
						<aside class="widgetWrap widget widget_tag_cloud">
							<h5 class="title">Tags</h5>
							<div class="tagcloud">
								<a href='#' class='tag-link-12' title='12 topics'>Attic</a>
								<a href='#' class='tag-link-46' title='15 topics'>Basement</a>
								<a href='#' class='tag-link-11' title='11 topics'>Bedroom</a>
								<a href='#' class='tag-link-65' title='8 topics'>Driveway</a>
								<a href='#' class='tag-link-8' title='6 topics'>Garage</a>
								<a href='#' class='tag-link-10' title='9 topics'>Kitchen</a>
								<a href='#' class='tag-link-64' title='9 topics'>Living room</a>
								<a href='#' class='tag-link-9' title='23 topics'>Popular</a>
							</div>
						</aside>
						<aside class="widgetWrap widget null-instagram-feed">
							<h5 class="title">Instagram</h5>
							<ul class="instagram-pics">
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
							</ul>
							<p class="clear">
								<a href="#" rel="me" target="_blank">Follow Us</a>
							</p>
						</aside>
					</div>						
				</div>
			</div>
		</div>
	</section>
</x-app-layout>