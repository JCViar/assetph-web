<x-app-layout>
    <x-slot name="title">
        Typography
    </x-slot>
	<section class="post">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-12">
					<article>
						<h1>H1 Heading</h1>
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br />
						&nbsp;</p>
						<h2>H2 Heading</h2>
						<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.<br />
						&nbsp;</p>
						<h3>H3 Heading</h3>
						<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.<br />
						&nbsp;</p>
						<h4>H4 Heading</h4>
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br />
						&nbsp;</p>
						<h5>H5 Heading</h5>
						<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.<br />
						&nbsp;</p>
						<h6>H6 Heading</h6>
						<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Standart Inline Elements</h2>
						<span class="sc_highlight sc_highlight_style_3">Bold text</span> dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <span class="sc_highlight sc_highlight_style_4">incididunt</span> labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex <a href="#">dafault link color</a>. Duis aute irure dolor in <span class="sc_highlight">
						<del class="sc_highlight sc_highlight_style_4"	>reprehenderit</del>
						</span> in voluptate <span class="sc_highlight sc_highlight_style_1">velit esse cillum</span> dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae  vitae dict <span class="sc_highlight sc_highlight_style_5">vitae dicta sunt</span> explicabo. Nemo voluptatem quia <span class="sc_tooltip_parent">voluptas sit aspernatur<span class="sc_tooltip">Tooltip Title</span>
						</span> aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Dropcaps</h2>
						<div class="columnsWrap sc_columns sc_columns_count_2 row">
							<div class="col-sm-6 sc_column_item sc_column_item_1">
								<div class="sc_dropcaps sc_dropcaps_style_1">
									<span class="sc_dropcap">D</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad miamco laboris nisi utmodo consequat.
								</div>
							</div>
							<div class="col-sm-6 sc_column_item sc_column_item_2">
								<div class="sc_dropcaps sc_dropcaps_style_2">
									<span class="sc_dropcap">D</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad miamco laboris nisi utmodo consequat.
								</div>
							</div>
						</div>
						<div class="columnsWrap sc_columns sc_columns_count_2 row">
							<div class="col-sm-6 sc_column_item sc_column_item_1">
								<div class="sc_dropcaps sc_dropcaps_style_3">
									<span class="sc_dropcap">D</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad miamco laboris nisi utmodo consequat.
								</div>
							</div>
							<div class="col-sm-6 sc_column_item sc_column_item_2">
								<div class="sc_dropcaps sc_dropcaps_style_4">
									<span class="sc_dropcap">D</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad miamco laboris nisi utmodo consequat.
								</div>
							</div>
						</div>
						<div class="columnsWrap sc_columns sc_columns_count_2 row">
							<div class="col-sm-6 sc_column_item sc_column_item_1">
								<div class="sc_dropcaps sc_dropcaps_style_5">
									<span class="sc_dropcap">D</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad miamco laboris nisi utmodo consequat.
								</div>
							</div>
							<div class="col-sm-6 sc_column_item sc_column_item_2">
								<div class="sc_dropcaps sc_dropcaps_style_6">
									<span class="sc_dropcap">D</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad miamco laboris nisi utmodo consequat.
								</div>
							</div>
						</div>
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Blockquotes</h2>
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						<blockquote cite="#" class="sc_quote sc_quote_style_1 margin_bottom_mini">
							<p>Always bear in mind that your own resolution to succeed is more important than any other.</p>
							<p class="sc_quote_title">
								<a href="#">Albert Einstein</a>
							</p>
						</blockquote> 
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
						<blockquote cite="#" class="sc_quote sc_quote_style_2 margin_bottom_mini">
							<p>Always bear in mind that your own resolution to succeed is more important than any other.</p>
							<p class="sc_quote_title">
							<a href="#">Albert Einstein</a>
							</p>
						</blockquote> 
						Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Image Alignment</h2>
						<p>
							Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque lbo. 
							<div class="sc_section  margin_bottom_mini sc_alignleft col-sm-6">
								<figure class="sc_image  sc_image_shape_square">
									<img src="{{ asset('assets/images/isotope/1900x1262.png') }}" alt="" />
									<figcaption>
										<span>This image aligned left</span>
									</figcaption>
								</figure>
							</div> 
							Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit.
						</p>
						<p>
							Voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
							<div class="sc_section sc_fullwidth margin_bottom_mini">
								Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. 
							</div>
							<div class="sc_section  margin_bottom_mini sc_alignright col-sm-6">
								<figure data-image="{{ asset('assets/images/isotope/1900x1262.png') }}" data-title="" class="thumb hoverIncrease sc_image  sc_image_shape_square">
									<img src="{{ asset('assets/images/isotope/1900x1262.png') }}" alt="" />
								</figure>
							</div> 
							Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit.
						</p>
						<p>	
							Voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
						</p>
							<div class="sc_section sc_fullwidth"> 
								Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequunatione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. 
							</div> 
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Lists</h2>
						<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequunatione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p> 
						<div class="col-md-3 col-sm-6 sc_column_item sc_column_item_1">
							<ul class="sc_list sc_list_style_ul">
								<li class="sc_list_item">Lorem ipsum</li>
								<li class="sc_list_item">Dolor sit amet</li>
								<li class="sc_list_item">Consectetur</li>
								<li class="sc_list_item">Adipisicing elit</li>
								<li class="sc_list_item">Sed do eiusmod</li>
							</ul>
						</div>
						<div class="col-md-3 col-sm-6 sc_column_item sc_column_item_2">
							<ul class="sc_list sc_list_style_disk">
								<li class="sc_list_item">Tempor incididunt</li>
								<li class="sc_list_item">Dolore magna</li>
								<li class="sc_list_item">Ut enim ad minim</li>
								<li class="sc_list_item">Nostrud exercion</li>
								<li class="sc_list_item">Ullamco laboris</li>
							</ul>
						</div>
						<div class="col-md-3 col-sm-6 sc_column_item sc_column_item_3">
							<ul class="sc_list sc_list_style_arrows">
								<li class="sc_list_item icon-right-open-mini">Lorem ipsum</li>
								<li class="sc_list_item icon-right-open-mini">Dolor sit amet</li>
								<li class="sc_list_item icon-right-open-mini">Consectetur</li>
								<li class="sc_list_item icon-right-open-mini">Adipisicing elit</li>
								<li class="sc_list_item icon-right-open-mini">Sed do eiusmod</li>
							</ul>
						</div>
						<div class="col-md-3 col-sm-6 sc_column_item sc_column_item_4">
							<ol class="sc_list sc_list_style_ol">
								<li class="sc_list_item">Tempor incididunt</li>
								<li class="sc_list_item">Dolore magna</li>
								<li class="sc_list_item">Ut enim ad minim</li>
								<li class="sc_list_item">Nostrud exercion</li>
								<li class="sc_list_item">Ullamco laboris</li>
							</ol>
						</div>
					</article>
					<div class="sc_line sc_line_style_solid margin_top_middle margin_bottom_middle"></div>
					<article>
						<h2>Columns</h2>
						<div class="row">
							<div class="col-sm-6 sc_column_item sc_column_item_1">
								<h5>1/2</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>
							<div class="col-sm-6 sc_column_item sc_column_item_2">
								<h5>1/2</h5>
								<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#8216;Content here. </p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4 sc_column_item sc_column_item_1">
								<h5>1/3</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
							</div>
							<div class="col-sm-4 sc_column_item sc_column_item_2">
								<h5>1/3</h5>
								<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
							</div>
							<div class="col-sm-4 sc_column_item sc_column_item_3">
								<h5>1/3</h5>
								<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-8 sc_column_item sc_column_item_1">
								<h5>2/3</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>
							</div>
							<div class="col-sm-4 sc_column_item sc_column_item_3">
								<h5>1/3</h5>
								<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC. 
							</div>
						</div>
						<div class="row">
							<div class="col-md-3 col-sm-6 sc_column_item sc_column_item_1">
								<h5>1/4</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							</div>
							<div class="col-md-3 col-sm-6 sc_column_item sc_column_item_2">
								<h5>1/4</h5>
								<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its aliqua layout.</p>
							</div>
							<div class="col-md-3 col-sm-6 sc_column_item sc_column_item_3">
								<h5>1/4</h5>
								<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature aliqua at its.</p>
							</div>
							<div class="col-md-3 col-sm-6 sc_column_item sc_column_item_4">
								<h5>1/4</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							</div>
						</div> 
					</article>
				</div>
				<div class="col-md-4 col-sm-12">
					<div id="sidebar_main" class="widget_area sidebar_main sidebar sidebarStyleDark" role="complementary">
						<aside class=" widgetWrap widget widget_categories">
							<h5 class="title">Categories</h5>
							<ul>
								<li class="cat-item current-cat-parent dropMenu">
									<a href="blog-classic-style-1-columns.html">Classic Style</a> (14)
									<ul class="children">
										<li class="cat-item">
											<a href="blog-classic-style-1-columns.html">1 Column</a> (9)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-2-columns.html">2 Columns</a> (11)
										</li>
										<li class="cat-item current-cat">
											<a href="blog-classic-style-2-columns-sidebar.html">2 Columns + sidebar</a> (11)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-3-columns.html">3 Columns</a> (10)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-3-columns-sidebar.html">3 Columns + sidebar</a> (11)
										</li>
										<li class="cat-item">
											<a href="blog-classic-style-4-columns.html">4 Columns</a> (10)
										</li>
									</ul>
								</li>
								<li class="cat-item">
									<a href="blog-classic-style-large.html">Classic Style Large</a> (13)
								</li>
								<li class="cat-item">
									<a href="blog-classic-style-small.html">Classic Style Small</a> (12)
								</li>
								<li class="cat-item">
									<a href="#">Contemporary</a> (15)
								</li>
								<li class="cat-item">
									<a href="#">Cottage</a> (5)
								</li>
								<li class="cat-item">
									<a href="gallery-grid-alternative.html">Grid Alternative</a> (22)
								</li>
								<li class="cat-item dropMenu">
									<a href="blog-masonry-style-2-columns.html">Masonry demo</a> (18)
									<ul class="children">
										<li class="cat-item">
											<a href="blog-masonry-style-2-columns.html">2 Columns</a> (17)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-2-columns-sidebar.html">2 Columns + sidebar</a> (17)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-3-columns.html">3 Columns</a> (17)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-3-columns-sidebar.html">3 Columns + sidebar</a> (13)
										</li>
										<li class="cat-item">
											<a href="blog-masonry-style-4-columns.html">4 Columns</a> (16)
										</li>
									</ul>
								</li>
								<li class="cat-item dropMenu">
									<a href="gallery-classic-style-1-column.html">Portfolio Classic</a> (22)
									<ul class="children">
										<li class="cat-item">
											<a href="gallery-classic-style-1-column.html">1 Column</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-1-column-sidebar.html">1 Column + sidebar</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-2-columns.html">2 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-2-columns-sidebar.html">2 Columns + sidebar</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-3-columns.html">3 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-3-columns-sidebar.html">3 Columns + sidebar</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-classic-style-4-columns.html">4 Columns</a> (22)
										</li>
									</ul>
								</li>
								<li class="cat-item dropMenu">
									<a href="gallery-grid-style-2-columns.html">Portfolio Grid</a> (22)
									<ul class="children">
										<li class="cat-item">
											<a href="gallery-grid-style-2-columns.html">2 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-2-columns-fullscreen.html">2 Columns fullscreen</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-3-columns.html">3 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-3-columns-fullscreen.html">3 Columns fullscreen</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-4-columns.html">4 Columns</a> (22)
										</li>
										<li class="cat-item">
											<a href="gallery-grid-style-4-columns-fullscreen.html">4 Columns fullscreen</a> (22)
										</li>
									</ul>
								</li>
								<li class="cat-item dropMenu">
									<a href="gallery-masonry-style-2-columns.html">Portfolio Masonry</a> (12)
									<ul class="children">
										<li class="cat-item">
											<a href="gallery-masonry-style-2-columns.html">2 Columns</a> (11)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-2-columns-sidebar.html">2 Columns + sidebar</a> (12)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-3-columns.html">3 Columns</a> (12)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-3-columns-sidebar.html">3 Columns + sidebar</a> (12)
										</li>
										<li class="cat-item">
											<a href="gallery-masonry-style-4-columns.html">4 Columns</a> (12)
										</li>
									</ul>
								</li>
								<li class="cat-item">
									<a href="gallery-grid-alternative.html">Projects</a> (9)
								</li>
							</ul>
						</aside>
						<aside class="widgetWrap widget widget_recent_comments">
							<h5 class="title">Recent Comments</h5>
							<ul>
								<li class="recentcomments">
									<span class="comment-author-link">Admin</span> 
									on 
									<a href="#">Is Condo Life For You?</a>
								</li>
								<li class="recentcomments">
									<span class="comment-author-link">Admin</span> 
									on 
									<a href="#">New Post With Image</a>
								</li>
								<li class="recentcomments">
									<span class="comment-author-link">Admin</span> 
									on 
									<a href="#">House Market Indicators</a>
								</li>
							</ul>
						</aside>
						<aside class="widgetWrap widget widget_archive">
							<h5 class="title">Archives</h5>
							<ul>
								<li>
									<a href='#'>January 2015</a>&nbsp;(3)
								</li>
								<li>
									<a href='#'>December 2014</a>&nbsp;(59)
								</li>
								<li>
									<a href='#'>November 2014</a>&nbsp;(3)
								</li>
								<li>
									<a href='#'>September 2014</a>&nbsp;(1)
								</li>
								<li>
									<a href='#'>August 2014</a>&nbsp;(1)
								</li>
								<li>
									<a href='#'>July 2014</a>&nbsp;(2)
								</li>
								<li>
									<a href='#'>May 2014</a>&nbsp;(5)
								</li>
								<li>
									<a href='#'>February 2014</a>&nbsp;(1)
								</li>
							</ul>
						</aside>
						<aside class="widgetWrap widget widget_search">
							<form role="search" method="get" class="search-form" action="index.html">
								<input type="text" class="search-field" placeholder="Search" value="" name="s" title="Search for:" />
								<span class="search-button light ico">
									<a class="search-field icon-search" href="#"></a>
								</span>
							</form>
						</aside>
						<aside class="widgetWrap widget widget_calendar">
							<div id="calendar_wrap">
								<table class="calendar">
									<thead>
										<tr>
											<th class="curMonth" colspan="7">
												<a href="#" title="View posts for August 2015">August</a>
											</th>
										</tr>
										<tr>
											<th scope="col" title="Monday">Mon</th>
											<th scope="col" title="Tuesday">Tue</th>
											<th scope="col" title="Wednesday">Wed</th>
											<th scope="col" title="Thursday">Thu</th>
											<th scope="col" title="Friday">Fri</th>
											<th scope="col" title="Saturday">Sat</th>
											<th scope="col" title="Sunday">Sun</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td colspan="1" class="pad">&nbsp;</td>
											<td>
												<span>1</span>
											</td>
											<td>
												<span>2</span>
											</td>
											<td>
												<a href="#" title="Post Formats – Chat">3</a>
											</td>
											<td>
												<span>4</span>
											</td>
											<td>
												<a href="#" title="Vimeo Video Post">5</a>
											</td>
											<td>
												<span>6</span>
											</td>
										</tr>
										<tr>
											<td>
												<span>7</span>
											</td>
											<td>
												<a href="#" title="What Is Housing Сooperative">8</a>
											</td>
											<td>
												<span>9</span>
											</td>
											<td>
												<a href="#" title="Post format – Aside">10</a>
											</td>
											<td>
												<span>11</span>
											</td>
											<td>
												<span>12</span>
											</td>
											<td>
												<span>13</span>
											</td>
										</tr>
										<tr>
											<td>
												<a href="#" title="Post formats – Link">14</a>
											</td>
											<td>
												<a href="#" title="Audio Post">15</a>
											</td>
											<td>
												<span>16</span>
											</td>
											<td>
												<span>17</span>
											</td>
											<td>
												<a href="#" title="Gallery Post Format">18</a>
											</td>
											<td>
												<a href="#" title="Post With Image">19</a>
											</td>
											<td>
												<span>20</span>
											</td>
										</tr>
										<tr>
											<td>
												<span>21</span>
											</td>
											<td>
												<span>22</span>
											</td>
											<td>
												<a href="#" title="Post Without Image">23</a>
											</td>
											<td>
												<span>24</span>
											</td>
											<td>
												<span>25</span>
											</td>
											<td>
												<a href="#" title="Status">26</a>
											</td>
											<td>
												<span>27</span>
											</td>
										</tr>
										<tr>
											<td>
												<span>28</span>
											</td>
											<td class="today">
												<span>29</span>
											</td>
											<td>
												<span>30</span>
											</td>
											<td class="pad" colspan="4">&nbsp;</td>
										</tr>
									</tbody>

									<tfoot>
										<tr>
											<th colspan="4" class="prevMonth">
												<div class="left">
													<a href="#" data-type="post" data-year="2014" data-month="7" title="View posts for July 2015">Jul</a>
												</div>
											</th>
											<th colspan="3" class="nextMonth">
												<div class="right">
													<a href="#" data-type="post" data-year="2015" data-month="9" title="View posts for September 2015">Sep</a>
												</div>
											</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</aside>
						<aside class="widgetWrap widget widget_tag_cloud">
							<h5 class="title">Tags</h5>
							<div class="tagcloud">
								<a href='#' class='tag-link-12' title='12 topics'>Attic</a>
								<a href='#' class='tag-link-46' title='15 topics'>Basement</a>
								<a href='#' class='tag-link-11' title='11 topics'>Bedroom</a>
								<a href='#' class='tag-link-65' title='8 topics'>Driveway</a>
								<a href='#' class='tag-link-8' title='6 topics'>Garage</a>
								<a href='#' class='tag-link-10' title='9 topics'>Kitchen</a>
								<a href='#' class='tag-link-64' title='9 topics'>Living room</a>
								<a href='#' class='tag-link-9' title='23 topics'>Popular</a>
							</div>
						</aside>
						<aside class="widgetWrap widget null-instagram-feed">
							<h5 class="title">Instagram</h5>
							<ul class="instagram-pics">
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
								<li class="">
									<a href="#" target="_blank"  class="">
										<img src="{{ asset('assets/images/instagram/640x640.png') }}"  alt="Instagram Image" title="Instagram Image"  class=""/>
									</a>
								</li>
							</ul>
							<p class="clear">
								<a href="#" rel="me" target="_blank">Follow Us</a>
							</p>
						</aside>
					</div>						
				</div>
			</div>
		</div>
	</section>
</x-app-layout>