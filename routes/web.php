<?php

use App\Http\Controllers\AboutUsController;
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\TourController;
use App\Http\Controllers\FaqController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\ToolController;
use App\Http\Controllers\OtherController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.index');
});

// Home routes
Route::get('home', [HomeController::class, 'index'])->name('home');
Route::get('home-fullpage-slider', [HomeController::class, 'home_fullpage_slider'])->name('homeFullpageSlider');
Route::get('home-menu-center', [HomeController::class, 'home_menu_center'])->name('homeMenuCenter');
Route::get('home-portfolio-style', [HomeController::class, 'home_portfolio_style'])->name('homePortfolio');
Route::get('home-presentation-page', [HomeController::class, 'home_presentation_page'])->name('homePresentation');
Route::get('home-video-tour', [HomeController::class, 'home_video_tour'])->name('homeVideoTour');

// Other routes
Route::get('about-us', [AboutUsController::class, 'index'])->name('aboutUs');
Route::get('appointments', [AppointmentController::class, 'index'])->name('appointments');
Route::get('services', [ServiceController::class, 'index'])->name('services');
Route::get('agents-team', [TeamController::class, 'index'])->name('team');
Route::get('contacts', [ContactController::class, 'index'])->name('contacts');
Route::get('faqs', [FaqController::class, 'index'])->name('faqs');

// Tour routes
Route::get('tour/the-perfect-apartment', [TourController::class, 'index'])->name('tour');
Route::get('tour/projects', [TourController::class, 'projects'])->name('projects');

// Gallery Classic routes
Route::get('gallery/classic/style-1-column-sidebar', [GalleryController::class, 'style_1_column_sidebar'])->name('classic1Sidebar');
Route::get('gallery/classic/style-2-column-sidebar', [GalleryController::class, 'style_2_columns_sidebar'])->name('classic2Sidebar');
Route::get('gallery/classic/style-3-column-sidebar', [GalleryController::class, 'style_3_columns_sidebar'])->name('classic3Sidebar');
Route::get('gallery/classic/style-1-column', [GalleryController::class, 'style_1_column'])->name('classic1');
Route::get('gallery/classic/style-2-columns', [GalleryController::class, 'style_2_columns'])->name('classic2');
Route::get('gallery/classic/style-3-columns', [GalleryController::class, 'style_3_columns'])->name('classic3');
Route::get('gallery/classic/style-4-columns', [GalleryController::class, 'style_4_columns'])->name('classic4');

// Gallery Grid routes
Route::get('gallery/grid/style-2-columns-fullscreen', [GalleryController::class, 'style_2_columns_fullscreen'])->name('grid2fs');
Route::get('gallery/grid/style-3-columns-fullscreen', [GalleryController::class, 'style_3_columns_fullscreen'])->name('grid3fs');
Route::get('gallery/grid/style-4-columns-fullscreen', [GalleryController::class, 'style_4_columns_fullscreen'])->name('grid4fs');
Route::get('gallery/grid/style-2-columns', [GalleryController::class, 'g_style_2_columns'])->name('grid2');
Route::get('gallery/grid/style-3-columns', [GalleryController::class, 'g_style_3_columns'])->name('grid3');
Route::get('gallery/grid/style-4-columns', [GalleryController::class, 'g_style_4_columns'])->name('grid4');

Route::get('gallery/grid/alternative', [GalleryController::class, 'g_style_alternatives'])->name('gridAlternative');

// Gallery Masonry routes
Route::get('gallery/masonry/style-2-columns', [GalleryController::class, 'm_style_2_columns'])->name('masonry2');
Route::get('gallery/masonry/style-3-columns', [GalleryController::class, 'm_style_3_columns'])->name('masonry3');
Route::get('gallery/masonry/style-4-columns', [GalleryController::class, 'm_style_4_columns'])->name('masonry4');
Route::get('gallery/masonry/style-2-column-sidebar', [GalleryController::class, 'm_style_2_columns_sidebar'])->name('masonry2Sidebar');
Route::get('gallery/masonry/style-3-column-sidebar', [GalleryController::class, 'm_style_3_columns_sidebar'])->name('masonry3Sidebar');

//  Gallery Portfolio
Route::get('gallery/portfolio/post-fullscreen', [GalleryController::class, 'portfolio_fullscreen'])->name('portfolioFullscreen');
Route::get('gallery/portfolio/post-standard', [GalleryController::class, 'post_standard'])->name('portfolioStandard');

// Blog Post route
Route::get('blog/classic/style-1-columns', [BlogController::class, 'style_1_columns'])->name('blogClassic1');
Route::get('blog/classic/style-2-columns', [BlogController::class, 'style_2_columns'])->name('blogClassic2');
Route::get('blog/classic/style-3-columns', [BlogController::class, 'style_3_columns'])->name('blogClassic3');
Route::get('blog/classic/style-4-columns', [BlogController::class, 'style_4_columns'])->name('blogClassic4');
Route::get('blog/classic/style-2-columns-sidebar', [BlogController::class, 'style_2_columns_sidebar'])->name('blogClassic2S');
Route::get('blog/classic/style-3-columns-sidebar', [BlogController::class, 'style_3_columns_sidebar'])->name('blogClassic3S');
Route::get('blog/classic/style-large', [BlogController::class, 'style_large'])->name('blogClassicLarge');
Route::get('blog/classic/style-small', [BlogController::class, 'style_small'])->name('blogClassicSmall');

Route::get('blog/masonry/style-2-columns', [BlogController::class, 'm_style_2_columns'])->name('blogMasonry2');
Route::get('blog/masonry/style-3-columns', [BlogController::class, 'm_style_3_columns'])->name('blogMasonry3');
Route::get('blog/masonry/style-4-columns', [BlogController::class, 'm_style_4_columns'])->name('blogMasonry4');
Route::get('blog/masonry/style-2-columns-sidebar', [BlogController::class, 'm_style_2_columns_sidebar'])->name('blogMasonry2S');
Route::get('blog/masonry/style-3-columns-sidebar', [BlogController::class, 'm_style_3_columns_sidebar'])->name('blogMasonry3S');

Route::get('blog/post/standard-sidebar', [BlogController::class, 'p_standard_sidebar'])->name('postStandardS');
Route::get('blog/post/standard', [BlogController::class, 'p_standard'])->name('postStandard');

// Tools
Route::get('tools/documentation', [ToolController::class, 'documentation'])->name('documentation');
Route::get('tools/idx/real-estate-matching-your-search', [ToolController::class, 'idx'])->name('idx');
Route::get('tools/shortcodes', [ToolController::class, 'shortcode'])->name('shortcodes');
Route::get('tools/support', [ToolController::class, 'support'])->name('support');
Route::get('tools/typography', [ToolController::class, 'typography'])->name('typography');

// Others
Route::get('others/navigation-side-menu', [OtherController::class, 'navigation_side_menu'])->name('sidemenu');
Route::get('others/subpages-blog', [OtherController::class, 'subpages_blog'])->name('postblock');
Route::get('others/subpages-price-table', [OtherController::class, 'subpages_price_table'])->name('pricetable');



Route::get('/dashboard', function () {
    return view('home.presentation-page');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
